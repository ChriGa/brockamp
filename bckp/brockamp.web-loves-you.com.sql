-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: d0264c4f
-- ------------------------------------------------------
-- Server version	5.6.33-nmm1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `w089_assets`
--

DROP TABLE IF EXISTS `w089_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_assets`
--

LOCK TABLES `w089_assets` WRITE;
/*!40000 ALTER TABLE `w089_assets` DISABLE KEYS */;
INSERT INTO `w089_assets` VALUES (1,0,0,119,0,'root.1','Root Asset','{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),(2,1,1,2,1,'com_admin','com_admin','{}'),(3,1,3,6,1,'com_banners','com_banners','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(4,1,7,8,1,'com_cache','com_cache','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(5,1,9,10,1,'com_checkin','com_checkin','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(6,1,11,12,1,'com_config','com_config','{}'),(7,1,13,16,1,'com_contact','com_contact','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(8,1,17,26,1,'com_content','com_content','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),(9,1,27,28,1,'com_cpanel','com_cpanel','{}'),(10,1,29,30,1,'com_installer','com_installer','{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),(11,1,31,32,1,'com_languages','com_languages','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(12,1,33,34,1,'com_login','com_login','{}'),(13,1,35,36,1,'com_mailto','com_mailto','{}'),(14,1,37,38,1,'com_massmail','com_massmail','{}'),(15,1,39,40,1,'com_media','com_media','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),(16,1,41,42,1,'com_menus','com_menus','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(17,1,43,44,1,'com_messages','com_messages','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(18,1,45,80,1,'com_modules','com_modules','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(19,1,81,84,1,'com_newsfeeds','com_newsfeeds','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(20,1,85,86,1,'com_plugins','com_plugins','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(21,1,87,88,1,'com_redirect','com_redirect','{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),(22,1,89,90,1,'com_search','com_search','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),(23,1,91,92,1,'com_templates','com_templates','{\"core.admin\":{\"7\":1},\"core.options\":[],\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(24,1,93,96,1,'com_users','com_users','{\"core.admin\":{\"7\":1},\"core.options\":[],\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(26,1,97,98,1,'com_wrapper','com_wrapper','{}'),(27,8,18,25,2,'com_content.category.2','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(28,3,4,5,2,'com_banners.category.3','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(29,7,14,15,2,'com_contact.category.4','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(30,19,82,83,2,'com_newsfeeds.category.5','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(32,24,94,95,1,'com_users.category.7','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(33,1,99,100,1,'com_finder','com_finder','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),(34,1,101,102,1,'com_joomlaupdate','com_joomlaupdate','{\"core.admin\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),(35,1,103,104,1,'com_tags','com_tags','{\"core.admin\":[],\"core.manage\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),(36,1,105,106,1,'com_contenthistory','com_contenthistory','{}'),(37,1,107,108,1,'com_ajax','com_ajax','{}'),(38,1,109,110,1,'com_postinstall','com_postinstall','{}'),(39,18,46,47,2,'com_modules.module.1','Main Menu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),(40,18,48,49,2,'com_modules.module.2','Login','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(41,18,50,51,2,'com_modules.module.3','Popular Articles','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(42,18,52,53,2,'com_modules.module.4','Recently Added Articles','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(43,18,54,55,2,'com_modules.module.8','Toolbar','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(44,18,56,57,2,'com_modules.module.9','Quick Icons','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(45,18,58,59,2,'com_modules.module.10','Logged-in Users','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(46,18,60,61,2,'com_modules.module.12','Admin Menu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(47,18,62,63,2,'com_modules.module.13','Admin Submenu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(48,18,64,65,2,'com_modules.module.14','User Status','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(49,18,66,67,2,'com_modules.module.15','Title','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(50,18,68,69,2,'com_modules.module.16','Login Form','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(51,18,70,71,2,'com_modules.module.17','Breadcrumbs','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),(52,18,72,73,2,'com_modules.module.79','Multilanguage status','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(53,18,74,75,2,'com_modules.module.86','Joomla Version','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(54,1,111,112,1,'com_jce','JCE','{}'),(55,18,76,77,2,'com_modules.module.87','AP Ajax Quick Contact',''),(58,1,113,114,1,'com_xmap','COM_XMAP','{}'),(59,18,78,79,2,'com_modules.module.89','testmodul','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"module.edit.frontend\":[]}'),(60,27,19,20,3,'com_content.article.1','Demo Beitrag','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(61,27,21,22,3,'com_content.article.2','Beitrag konnte leider nicht gefunden werden','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(62,27,23,24,3,'com_content.article.3','Beitrag mit Slideshow','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(63,1,115,116,1,'com_fields','com_fields','{}'),(64,1,117,118,1,'com_associations','com_associations','{}');
/*!40000 ALTER TABLE `w089_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_associations`
--

DROP TABLE IF EXISTS `w089_associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_associations`
--

LOCK TABLES `w089_associations` WRITE;
/*!40000 ALTER TABLE `w089_associations` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_associations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_banner_clients`
--

DROP TABLE IF EXISTS `w089_banner_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_banner_clients`
--

LOCK TABLES `w089_banner_clients` WRITE;
/*!40000 ALTER TABLE `w089_banner_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_banner_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_banner_tracks`
--

DROP TABLE IF EXISTS `w089_banner_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_banner_tracks`
--

LOCK TABLES `w089_banner_tracks` WRITE;
/*!40000 ALTER TABLE `w089_banner_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_banner_tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_banners`
--

DROP TABLE IF EXISTS `w089_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_metakey_prefix` (`metakey_prefix`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_banners`
--

LOCK TABLES `w089_banners` WRITE;
/*!40000 ALTER TABLE `w089_banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_categories`
--

DROP TABLE IF EXISTS `w089_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_language` (`language`),
  KEY `idx_path` (`path`(100)),
  KEY `idx_alias` (`alias`(100))
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_categories`
--

LOCK TABLES `w089_categories` WRITE;
/*!40000 ALTER TABLE `w089_categories` DISABLE KEYS */;
INSERT INTO `w089_categories` VALUES (1,0,0,0,11,0,'','system','ROOT','root','','',1,0,'0000-00-00 00:00:00',1,'{}','','','{}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(2,27,1,1,2,1,'uncategorised','com_content','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(3,28,1,3,4,1,'uncategorised','com_banners','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(4,29,1,5,6,1,'uncategorised','com_contact','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(5,30,1,7,8,1,'uncategorised','com_newsfeeds','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(7,32,1,9,10,1,'uncategorised','com_users','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1);
/*!40000 ALTER TABLE `w089_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_contact_details`
--

DROP TABLE IF EXISTS `w089_contact_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_contact_details`
--

LOCK TABLES `w089_contact_details` WRITE;
/*!40000 ALTER TABLE `w089_contact_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_contact_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_content`
--

DROP TABLE IF EXISTS `w089_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_content`
--

LOCK TABLES `w089_content` WRITE;
/*!40000 ALTER TABLE `w089_content` DISABLE KEYS */;
INSERT INTO `w089_content` VALUES (1,60,'Demo Beitrag','demo-beitrag','<h1>Lorem ipsum dolor sit amet</h1>\r\n<h2>consetetur sadipscing elitr, sed diam nonumy eirmod tempor</h2>\r\n<h3>invidunt ut labore et dolore magna aliquyam erat</h3>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>sed diam voluptua.</li>\r\n<li>At vero eos et accusam et</li>\r\n<li>justo duo&nbsp;<a href=\"#\">dolores</a> et ea rebum.</li>\r\n<li>Stet clita kasd gubergren</li>\r\n<li>no sea<a href=\"#\">t akimata</a> sanctus est Lorem ipsum</li>\r\n</ul>\r\n<p>&nbsp;</p>','',1,2,'2015-09-09 12:28:58',332,'','2015-09-09 12:29:54',332,0,'0000-00-00 00:00:00','2015-09-09 12:28:58','0000-00-00 00:00:00','{\"image_intro\":\"images\\/content\\/logo\\/lorem_logo_S.png\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/content\\/logo\\/lorem_logo_M.png\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',2,2,'','',1,6,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*',''),(2,61,'Beitrag konnte leider nicht gefunden werden','404-artikel','<style scoped=\"scoped\" type=\"text/css\"><!--\r\n@import url(http://fonts.googleapis.com/css?family=Gilda+Display);\r\n.static {\r\n  width: 100%;\r\n  height: 100%;\r\n  position: relative;\r\n  margin: 0;\r\n  padding: 0;\r\n  top: -100px;\r\n  opacity: 0.05;\r\n  z-index: 230;\r\n  user-select: none;\r\n  user-drag: none;\r\n}\r\n\r\n.error {\r\n  text-align: center;\r\n  font-family: \'Gilda Display\', serif;\r\n  font-size: 95px;\r\n  font-style: italic;\r\n  text-align: center;\r\n  width: 100px;\r\n  height: 60px;\r\n  line-height: 60px;\r\n  margin: 30px auto;\r\n  position: relative;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: -60px;\r\n  right: 0;\r\n  animation: noise 2s linear infinite;\r\n  overflow: default;\r\n}\r\n\r\n.error:after {\r\n  content: \'404\';\r\n  font-family: \'Gilda Display\', serif;\r\n  font-size: 100px;\r\n  font-style: italic;\r\n  text-align: center;\r\n  width: 150px;\r\n  height: 60px;\r\n  line-height: 60px;\r\n  margin: auto;\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  opacity: 0;\r\n  color: blue;\r\n  animation: noise-1 .2s linear infinite;\r\n}\r\n\r\n.info {\r\n  text-align: center;\r\n  font-family: \'Gilda Display\', serif;\r\n  font-size: 15px;\r\n  font-style: italic;\r\n  text-align: center;\r\n  width: 200px;\r\n  height: 60px;\r\n  line-height: 60px;\r\n  margin: -40px auto;\r\n  position: absolute;\r\n  left: 0;\r\n  right: 0;\r\n  animation: noise-3 1s linear infinite;\r\n}\r\n\r\n.error:before {\r\n  content: \'404\';\r\n  font-family: \'Gilda Display\', serif;\r\n  font-size: 100px;\r\n  font-style: italic;\r\n  text-align: center;\r\n  width: 100px;\r\n  height: 60px;\r\n  line-height: 60px;\r\n  margin: auto;\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  opacity: 0;\r\n  color: red;\r\n  animation: noise-2 .2s linear infinite;\r\n}\r\n\r\n@keyframes noise-1 {\r\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\r\n  10% {opacity: .1;}\r\n  50% {opacity: .5; left: -6px;}\r\n  80% {opacity: .3;}\r\n  100% {opacity: .6; left: 2px;}\r\n}\r\n\r\n@keyframes noise-2 {\r\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\r\n  10% {opacity: .1;}\r\n  50% {opacity: .5; left: 6px;}\r\n  80% {opacity: .3;}\r\n  100% {opacity: .6; left: -2px;}\r\n}\r\n\r\n@keyframes noise {\r\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \r\n  4.3% {opacity: 1; transform: scaleY(1.7);}\r\n  43% {opacity: 1; transform: scaleX(1.5);}\r\n}\r\n\r\n@keyframes noise-3 {\r\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\r\n  4.3% {opacity: 1; transform: scaleY(4);}\r\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\r\n}\r\n--></style>\r\n<div class=\"error\">404</div>\r\n<p><br /><br /> <span class=\"info\">Beitrag nicht gefunden</span>&nbsp;</p>','',1,2,'2015-10-25 17:40:28',332,'','2015-10-25 18:37:16',332,0,'0000-00-00 00:00:00','2015-10-25 17:40:28','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_icons\":\"\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_vote\":\"\",\"show_hits\":\"0\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',32,1,'','',1,72,'{\"robots\":\"noindex, nofollow\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*',''),(3,62,'Beitrag mit Slideshow','beitrag-mit-slideshow','\r\n<div class=\"row-fluid\">\r\n<div class=\"span8\">\r\n<div id=\"myCarousel\" class=\"carousel-fade carousel slide\"><ol class=\"carousel-indicators\">\r\n<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>\r\n<li data-target=\"#myCarousel\" data-slide-to=\"1\"></li>\r\n<li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\r\n</ol><!-- Carousel items -->\r\n<div class=\"carousel-inner\">\r\n<div class=\"active item\"><img src=\"http://lorempixel.com/g/650/400/nature\" alt=\"dummy slide\" /></div>\r\n<div class=\"item\"><img src=\"http://lorempixel.com/g/650/400/sports\" alt=\"dummy slide\" /></div>\r\n<div class=\"item\"><img src=\"http://lorempixel.com/g/650/400/fashion\" alt=\"dummy slide\" /></div>\r\n</div>\r\n<!-- Carousel nav --><a class=\"carousel-control left\" href=\"#myCarousel\" data-slide=\"prev\">&lsaquo;</a> <a class=\"carousel-control right\" href=\"#myCarousel\" data-slide=\"next\">&rsaquo;</a></div>\r\n</div>\r\n</div>\r\n\r\n<script type=\"text/javascript\">jQuery(document).ready(function() {\r\n		jQuery(\'.carousel\').carousel({\r\n			interval: 4000\r\n		})\r\n	});</script>','',1,2,'2015-11-01 18:08:57',332,'','2016-09-28 21:30:36',332,0,'0000-00-00 00:00:00','2015-11-01 18:08:57','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',36,0,'','',1,0,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*','');
/*!40000 ALTER TABLE `w089_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_content_frontpage`
--

DROP TABLE IF EXISTS `w089_content_frontpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_content_frontpage`
--

LOCK TABLES `w089_content_frontpage` WRITE;
/*!40000 ALTER TABLE `w089_content_frontpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_content_frontpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_content_rating`
--

DROP TABLE IF EXISTS `w089_content_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_content_rating`
--

LOCK TABLES `w089_content_rating` WRITE;
/*!40000 ALTER TABLE `w089_content_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_content_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_content_types`
--

DROP TABLE IF EXISTS `w089_content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`(100))
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_content_types`
--

LOCK TABLES `w089_content_types` WRITE;
/*!40000 ALTER TABLE `w089_content_types` DISABLE KEYS */;
INSERT INTO `w089_content_types` VALUES (1,'Article','com_content.article','{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}','ContentHelperRoute::getArticleRoute','{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(2,'Contact','com_contact.contact','{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}','ContactHelperRoute::getContactRoute','{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),(3,'Newsfeed','com_newsfeeds.newsfeed','{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}','NewsfeedsHelperRoute::getNewsfeedRoute','{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(4,'User','com_users.user','{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}','UsersHelperRoute::getUserRoute',''),(5,'Article Category','com_content.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','ContentHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(6,'Contact Category','com_contact.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','ContactHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(7,'Newsfeeds Category','com_newsfeeds.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','NewsfeedsHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(8,'Tag','com_tags.tag','{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}','TagsHelperRoute::getTagRoute','{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),(9,'Banner','com_banners.banner','{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}','','{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(10,'Banners Category','com_banners.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(11,'Banner Client','com_banners.client','{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}','','','','{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),(12,'User Notes','com_users.note','{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}','','','','{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),(13,'User Notes Category','com_users.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');
/*!40000 ALTER TABLE `w089_content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_contentitem_tag_map`
--

DROP TABLE IF EXISTS `w089_contentitem_tag_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_contentitem_tag_map`
--

LOCK TABLES `w089_contentitem_tag_map` WRITE;
/*!40000 ALTER TABLE `w089_contentitem_tag_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_contentitem_tag_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_core_log_searches`
--

DROP TABLE IF EXISTS `w089_core_log_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_core_log_searches`
--

LOCK TABLES `w089_core_log_searches` WRITE;
/*!40000 ALTER TABLE `w089_core_log_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_core_log_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_extensions`
--

DROP TABLE IF EXISTS `w089_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10019 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_extensions`
--

LOCK TABLES `w089_extensions` WRITE;
/*!40000 ALTER TABLE `w089_extensions` DISABLE KEYS */;
INSERT INTO `w089_extensions` VALUES (1,0,'com_mailto','component','com_mailto','',0,1,1,1,'{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}','','','',0,'0000-00-00 00:00:00',0,0),(2,0,'com_wrapper','component','com_wrapper','',0,1,1,1,'{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}','','','',0,'0000-00-00 00:00:00',0,0),(3,0,'com_admin','component','com_admin','',1,1,1,1,'{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(4,0,'com_banners','component','com_banners','',1,1,1,0,'{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}','{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}','','',0,'0000-00-00 00:00:00',0,0),(5,0,'com_cache','component','com_cache','',1,1,1,1,'{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(6,0,'com_categories','component','com_categories','',1,1,1,1,'{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(7,0,'com_checkin','component','com_checkin','',1,1,1,1,'{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(8,0,'com_contact','component','com_contact','',1,1,1,0,'{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}','{\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"allow_vcard_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_category_crumb\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(9,0,'com_cpanel','component','com_cpanel','',1,1,1,1,'{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(10,0,'com_installer','component','com_installer','',1,1,1,1,'{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_jed_info\":\"0\",\"cachetimeout\":\"6\",\"minimum_stability\":\"4\"}','','',0,'0000-00-00 00:00:00',0,0),(11,0,'com_languages','component','com_languages','',1,1,1,1,'{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}','{\"administrator\":\"de-DE\",\"site\":\"de-DE\"}','','',0,'0000-00-00 00:00:00',0,0),(12,0,'com_login','component','com_login','',1,1,1,1,'{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(13,0,'com_media','component','com_media','',1,1,0,1,'{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}','{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}','','',0,'0000-00-00 00:00:00',0,0),(14,0,'com_menus','component','com_menus','',1,1,1,1,'{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(15,0,'com_messages','component','com_messages','',1,1,1,1,'{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(16,0,'com_modules','component','com_modules','',1,1,1,1,'{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(17,0,'com_newsfeeds','component','com_newsfeeds','',1,1,1,0,'{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(18,0,'com_plugins','component','com_plugins','',1,1,1,1,'{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(19,0,'com_search','component','com_search','',1,1,1,0,'{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}','{\"enabled\":\"0\",\"show_date\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(20,0,'com_templates','component','com_templates','',1,1,1,1,'{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}','{\"template_positions_display\":\"1\",\"upload_limit\":\"2\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}','','',0,'0000-00-00 00:00:00',0,0),(22,0,'com_content','component','com_content','',1,1,0,1,'{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"1\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(23,0,'com_config','component','com_config','',1,1,0,1,'{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}','{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"9\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}','','',0,'0000-00-00 00:00:00',0,0),(24,0,'com_redirect','component','com_redirect','',1,1,0,1,'{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(25,0,'com_users','component','com_users','',1,1,0,1,'{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}','{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"1\",\"useractivation\":\"2\",\"mail_to_admin\":\"1\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(27,0,'com_finder','component','com_finder','',1,1,0,0,'{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}','{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}','','',0,'0000-00-00 00:00:00',0,0),(28,0,'com_joomlaupdate','component','com_joomlaupdate','',1,1,0,1,'{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(29,0,'com_tags','component','com_tags','',1,1,1,1,'{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"show_tag_num_items\":\"0\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(30,0,'com_contenthistory','component','com_contenthistory','',1,1,1,0,'{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}','','','',0,'0000-00-00 00:00:00',0,0),(31,0,'com_ajax','component','com_ajax','',1,1,1,1,'{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}','','','',0,'0000-00-00 00:00:00',0,0),(32,0,'com_postinstall','component','com_postinstall','',1,1,1,1,'{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(33,0,'com_fields','component','com_fields','',1,1,1,0,'{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}','','','',0,'0000-00-00 00:00:00',0,0),(34,0,'com_associations','component','com_associations','',1,1,1,0,'{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"Januar 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(102,0,'LIB_PHPUTF8','library','phputf8','',0,1,1,1,'{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}','','','',0,'0000-00-00 00:00:00',0,0),(103,0,'LIB_JOOMLA','library','joomla','',0,1,1,1,'{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','{\"mediaversion\":\"e5fad21796641c8c296ec81ff988f88d\"}','','',0,'0000-00-00 00:00:00',0,0),(104,0,'LIB_IDNA','library','idna_convert','',0,1,1,1,'{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}','','','',0,'0000-00-00 00:00:00',0,0),(105,0,'FOF','library','fof','',0,1,1,1,'{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}','','','',0,'0000-00-00 00:00:00',0,0),(106,0,'LIB_PHPASS','library','phpass','',0,1,1,1,'{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}','','','',0,'0000-00-00 00:00:00',0,0),(200,0,'mod_articles_archive','module','mod_articles_archive','',0,1,1,0,'{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}','','','',0,'0000-00-00 00:00:00',0,0),(201,0,'mod_articles_latest','module','mod_articles_latest','',0,1,1,0,'{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(202,0,'mod_articles_popular','module','mod_articles_popular','',0,1,1,0,'{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}','','','',0,'0000-00-00 00:00:00',0,0),(203,0,'mod_banners','module','mod_banners','',0,1,1,0,'{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}','','','',0,'0000-00-00 00:00:00',0,0),(204,0,'mod_breadcrumbs','module','mod_breadcrumbs','',0,1,1,1,'{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}','','','',0,'0000-00-00 00:00:00',0,0),(205,0,'mod_custom','module','mod_custom','',0,1,1,1,'{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}','','','',0,'0000-00-00 00:00:00',0,0),(206,0,'mod_feed','module','mod_feed','',0,1,1,0,'{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}','','','',0,'0000-00-00 00:00:00',0,0),(207,0,'mod_footer','module','mod_footer','',0,1,1,0,'{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}','','','',0,'0000-00-00 00:00:00',0,0),(208,0,'mod_login','module','mod_login','',0,1,1,1,'{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}','','','',0,'0000-00-00 00:00:00',0,0),(209,0,'mod_menu','module','mod_menu','',0,1,1,1,'{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}','','','',0,'0000-00-00 00:00:00',0,0),(210,0,'mod_articles_news','module','mod_articles_news','',0,1,1,0,'{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}','','','',0,'0000-00-00 00:00:00',0,0),(211,0,'mod_random_image','module','mod_random_image','',0,1,1,0,'{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}','','','',0,'0000-00-00 00:00:00',0,0),(212,0,'mod_related_items','module','mod_related_items','',0,1,1,0,'{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}','','','',0,'0000-00-00 00:00:00',0,0),(213,0,'mod_search','module','mod_search','',0,1,1,0,'{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}','','','',0,'0000-00-00 00:00:00',0,0),(214,0,'mod_stats','module','mod_stats','',0,1,1,0,'{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}','','','',0,'0000-00-00 00:00:00',0,0),(215,0,'mod_syndicate','module','mod_syndicate','',0,1,1,1,'{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}','','','',0,'0000-00-00 00:00:00',0,0),(216,0,'mod_users_latest','module','mod_users_latest','',0,1,1,0,'{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(218,0,'mod_whosonline','module','mod_whosonline','',0,1,1,0,'{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}','','','',0,'0000-00-00 00:00:00',0,0),(219,0,'mod_wrapper','module','mod_wrapper','',0,1,1,0,'{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}','','','',0,'0000-00-00 00:00:00',0,0),(220,0,'mod_articles_category','module','mod_articles_category','',0,1,1,0,'{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}','','','',0,'0000-00-00 00:00:00',0,0),(221,0,'mod_articles_categories','module','mod_articles_categories','',0,1,1,0,'{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}','','','',0,'0000-00-00 00:00:00',0,0),(222,0,'mod_languages','module','mod_languages','',0,1,1,1,'{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}','','','',0,'0000-00-00 00:00:00',0,0),(223,0,'mod_finder','module','mod_finder','',0,1,0,0,'{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}','','','',0,'0000-00-00 00:00:00',0,0),(300,0,'mod_custom','module','mod_custom','',1,1,1,1,'{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}','','','',0,'0000-00-00 00:00:00',0,0),(301,0,'mod_feed','module','mod_feed','',1,1,1,0,'{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}','','','',0,'0000-00-00 00:00:00',0,0),(302,0,'mod_latest','module','mod_latest','',1,1,1,0,'{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(303,0,'mod_logged','module','mod_logged','',1,1,1,0,'{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}','','','',0,'0000-00-00 00:00:00',0,0),(304,0,'mod_login','module','mod_login','',1,1,1,1,'{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}','','','',0,'0000-00-00 00:00:00',0,0),(305,0,'mod_menu','module','mod_menu','',1,1,1,0,'{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}','','','',0,'0000-00-00 00:00:00',0,0),(307,0,'mod_popular','module','mod_popular','',1,1,1,0,'{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}','','','',0,'0000-00-00 00:00:00',0,0),(308,0,'mod_quickicon','module','mod_quickicon','',1,1,1,1,'{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}','','','',0,'0000-00-00 00:00:00',0,0),(309,0,'mod_status','module','mod_status','',1,1,1,0,'{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}','','','',0,'0000-00-00 00:00:00',0,0),(310,0,'mod_submenu','module','mod_submenu','',1,1,1,0,'{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}','','','',0,'0000-00-00 00:00:00',0,0),(311,0,'mod_title','module','mod_title','',1,1,1,0,'{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}','','','',0,'0000-00-00 00:00:00',0,0),(312,0,'mod_toolbar','module','mod_toolbar','',1,1,1,1,'{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}','','','',0,'0000-00-00 00:00:00',0,0),(313,0,'mod_multilangstatus','module','mod_multilangstatus','',1,1,1,0,'{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}','{\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(314,0,'mod_version','module','mod_version','',1,1,1,0,'{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}','{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(315,0,'mod_stats_admin','module','mod_stats_admin','',1,1,1,0,'{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}','{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}','','',0,'0000-00-00 00:00:00',0,0),(316,0,'mod_tags_popular','module','mod_tags_popular','',0,1,1,0,'{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}','{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(317,0,'mod_tags_similar','module','mod_tags_similar','',0,1,1,0,'{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}','{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(400,0,'plg_authentication_gmail','plugin','gmail','authentication',0,0,1,0,'{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}','{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}','','',0,'0000-00-00 00:00:00',1,0),(401,0,'plg_authentication_joomla','plugin','joomla','authentication',0,1,1,1,'{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',0,0),(402,0,'plg_authentication_ldap','plugin','ldap','authentication',0,0,1,0,'{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}','{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}','','',0,'0000-00-00 00:00:00',3,0),(403,0,'plg_content_contact','plugin','contact','content',0,1,1,0,'{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}','','','',0,'0000-00-00 00:00:00',1,0),(404,0,'plg_content_emailcloak','plugin','emailcloak','content',0,1,1,0,'{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}','{\"mode\":\"1\"}','','',0,'0000-00-00 00:00:00',1,0),(406,0,'plg_content_loadmodule','plugin','loadmodule','content',0,1,1,0,'{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}','{\"style\":\"xhtml\"}','','',0,'2011-09-18 15:22:50',0,0),(407,0,'plg_content_pagebreak','plugin','pagebreak','content',0,1,1,0,'{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}','{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}','','',0,'0000-00-00 00:00:00',4,0),(408,0,'plg_content_pagenavigation','plugin','pagenavigation','content',0,1,1,0,'{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}','{\"position\":\"1\"}','','',0,'0000-00-00 00:00:00',5,0),(409,0,'plg_content_vote','plugin','vote','content',0,1,1,0,'{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}','','','',0,'0000-00-00 00:00:00',6,0),(410,0,'plg_editors_codemirror','plugin','codemirror','editors',0,1,1,1,'{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.23.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}','{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}','','',0,'0000-00-00 00:00:00',1,0),(411,0,'plg_editors_none','plugin','none','editors',0,1,1,1,'{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}','','','',0,'0000-00-00 00:00:00',2,0),(412,0,'plg_editors_tinymce','plugin','tinymce','editors',0,1,1,0,'{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2017\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.5.6\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}','{\"mode\":\"1\",\"skin\":\"0\",\"mobile\":\"0\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\"\",\"html_height\":\"550\",\"html_width\":\"750\",\"resizing\":\"1\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"hr\":\"1\",\"link\":\"1\",\"media\":\"1\",\"print\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"alignment\":\"1\",\"visualchars\":\"1\",\"visualblocks\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}','','',0,'0000-00-00 00:00:00',3,0),(413,0,'plg_editors-xtd_article','plugin','article','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}','','','',0,'0000-00-00 00:00:00',1,0),(414,0,'plg_editors-xtd_image','plugin','image','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}','','','',0,'0000-00-00 00:00:00',2,0),(415,0,'plg_editors-xtd_pagebreak','plugin','pagebreak','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}','','','',0,'0000-00-00 00:00:00',3,0),(416,0,'plg_editors-xtd_readmore','plugin','readmore','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}','','','',0,'0000-00-00 00:00:00',4,0),(417,0,'plg_search_categories','plugin','categories','search',0,1,1,0,'{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(418,0,'plg_search_contacts','plugin','contacts','search',0,1,1,0,'{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(419,0,'plg_search_content','plugin','content','search',0,1,1,0,'{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(420,0,'plg_search_newsfeeds','plugin','newsfeeds','search',0,1,1,0,'{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(422,0,'plg_system_languagefilter','plugin','languagefilter','system',0,0,1,1,'{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}','','','',0,'0000-00-00 00:00:00',1,0),(423,0,'plg_system_p3p','plugin','p3p','system',0,0,1,0,'{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}','{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}','','',0,'0000-00-00 00:00:00',2,0),(424,0,'plg_system_cache','plugin','cache','system',0,0,1,1,'{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}','{\"browsercache\":\"0\",\"cachetime\":\"15\"}','','',0,'0000-00-00 00:00:00',9,0),(425,0,'plg_system_debug','plugin','debug','system',0,1,1,0,'{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}','{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}','','',0,'0000-00-00 00:00:00',4,0),(426,0,'plg_system_log','plugin','log','system',0,1,1,1,'{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}','','','',0,'0000-00-00 00:00:00',5,0),(427,0,'plg_system_redirect','plugin','redirect','system',0,0,1,1,'{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}','','','',0,'0000-00-00 00:00:00',6,0),(428,0,'plg_system_remember','plugin','remember','system',0,1,1,1,'{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}','','','',0,'0000-00-00 00:00:00',7,0),(429,0,'plg_system_sef','plugin','sef','system',0,1,1,0,'{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}','','','',0,'0000-00-00 00:00:00',8,0),(430,0,'plg_system_logout','plugin','logout','system',0,1,1,1,'{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}','','','',0,'0000-00-00 00:00:00',3,0),(431,0,'plg_user_contactcreator','plugin','contactcreator','user',0,0,1,0,'{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}','{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}','','',0,'0000-00-00 00:00:00',1,0),(432,0,'plg_user_joomla','plugin','joomla','user',0,1,1,0,'{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}','','',0,'0000-00-00 00:00:00',2,0),(433,0,'plg_user_profile','plugin','profile','user',0,0,1,0,'{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}','{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(434,0,'plg_extension_joomla','plugin','joomla','extension',0,1,1,1,'{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',1,0),(435,0,'plg_content_joomla','plugin','joomla','content',0,1,1,0,'{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',0,0),(436,0,'plg_system_languagecode','plugin','languagecode','system',0,0,1,0,'{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}','','','',0,'0000-00-00 00:00:00',10,0),(437,0,'plg_quickicon_joomlaupdate','plugin','joomlaupdate','quickicon',0,1,1,1,'{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}','','','',0,'0000-00-00 00:00:00',0,0),(438,0,'plg_quickicon_extensionupdate','plugin','extensionupdate','quickicon',0,1,1,1,'{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}','','','',0,'0000-00-00 00:00:00',0,0),(439,0,'plg_captcha_recaptcha','plugin','recaptcha','captcha',0,0,1,0,'{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}','{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}','','',0,'0000-00-00 00:00:00',0,0),(440,0,'plg_system_highlight','plugin','highlight','system',0,1,1,0,'{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}','','','',0,'0000-00-00 00:00:00',7,0),(441,0,'plg_content_finder','plugin','finder','content',0,0,1,0,'{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}','','','',0,'0000-00-00 00:00:00',0,0),(442,0,'plg_finder_categories','plugin','categories','finder',0,1,1,0,'{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}','','','',0,'0000-00-00 00:00:00',1,0),(443,0,'plg_finder_contacts','plugin','contacts','finder',0,1,1,0,'{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}','','','',0,'0000-00-00 00:00:00',2,0),(444,0,'plg_finder_content','plugin','content','finder',0,1,1,0,'{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','','','',0,'0000-00-00 00:00:00',3,0),(445,0,'plg_finder_newsfeeds','plugin','newsfeeds','finder',0,1,1,0,'{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','','','',0,'0000-00-00 00:00:00',4,0),(447,0,'plg_finder_tags','plugin','tags','finder',0,1,1,0,'{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','','','',0,'0000-00-00 00:00:00',0,0),(448,0,'plg_twofactorauth_totp','plugin','totp','twofactorauth',0,0,1,0,'{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}','','','',0,'0000-00-00 00:00:00',0,0),(449,0,'plg_authentication_cookie','plugin','cookie','authentication',0,1,1,0,'{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}','','','',0,'0000-00-00 00:00:00',0,0),(450,0,'plg_twofactorauth_yubikey','plugin','yubikey','twofactorauth',0,0,1,0,'{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}','','','',0,'0000-00-00 00:00:00',0,0),(451,0,'plg_search_tags','plugin','tags','search',0,1,1,0,'{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(452,0,'plg_system_updatenotification','plugin','updatenotification','system',0,1,1,0,'{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}','{\"lastrun\":1494265834}','','',0,'0000-00-00 00:00:00',0,0),(453,0,'plg_editors-xtd_module','plugin','module','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}','','','',0,'0000-00-00 00:00:00',0,0),(454,0,'plg_system_stats','plugin','stats','system',0,1,1,0,'{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}','{\"mode\":3,\"lastrun\":\"\",\"unique_id\":\"b8624dd23737e444e7f6f4737ff7f7423e5a93da\",\"interval\":12}','','',0,'0000-00-00 00:00:00',0,0),(455,0,'plg_installer_packageinstaller','plugin','packageinstaller','installer',0,1,1,1,'{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}','','','',0,'0000-00-00 00:00:00',1,0),(456,0,'PLG_INSTALLER_FOLDERINSTALLER','plugin','folderinstaller','installer',0,1,1,1,'{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}','','','',0,'0000-00-00 00:00:00',2,0),(457,0,'PLG_INSTALLER_URLINSTALLER','plugin','urlinstaller','installer',0,1,1,1,'{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}','','','',0,'0000-00-00 00:00:00',3,0),(458,0,'plg_quickicon_phpversioncheck','plugin','phpversioncheck','quickicon',0,1,1,1,'{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}','','','',0,'0000-00-00 00:00:00',0,0),(459,0,'plg_editors-xtd_menu','plugin','menu','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}','','','',0,'0000-00-00 00:00:00',0,0),(460,0,'plg_editors-xtd_contact','plugin','contact','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}','','','',0,'0000-00-00 00:00:00',0,0),(461,0,'plg_system_fields','plugin','fields','system',0,1,1,0,'{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}','','','',0,'0000-00-00 00:00:00',0,0),(462,0,'plg_fields_calendar','plugin','calendar','fields',0,1,1,0,'{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}','','','',0,'0000-00-00 00:00:00',0,0),(463,0,'plg_fields_checkboxes','plugin','checkboxes','fields',0,1,1,0,'{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}','','','',0,'0000-00-00 00:00:00',0,0),(464,0,'plg_fields_color','plugin','color','fields',0,1,1,0,'{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}','','','',0,'0000-00-00 00:00:00',0,0),(465,0,'plg_fields_editor','plugin','editor','fields',0,1,1,0,'{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}','','','',0,'0000-00-00 00:00:00',0,0),(466,0,'plg_fields_imagelist','plugin','imagelist','fields',0,1,1,0,'{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}','','','',0,'0000-00-00 00:00:00',0,0),(467,0,'plg_fields_integer','plugin','integer','fields',0,1,1,0,'{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}','{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(468,0,'plg_fields_list','plugin','list','fields',0,1,1,0,'{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}','','','',0,'0000-00-00 00:00:00',0,0),(469,0,'plg_fields_media','plugin','media','fields',0,1,1,0,'{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}','','','',0,'0000-00-00 00:00:00',0,0),(470,0,'plg_fields_radio','plugin','radio','fields',0,1,1,0,'{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}','','','',0,'0000-00-00 00:00:00',0,0),(471,0,'plg_fields_sql','plugin','sql','fields',0,1,1,0,'{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}','','','',0,'0000-00-00 00:00:00',0,0),(472,0,'plg_fields_text','plugin','text','fields',0,1,1,0,'{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}','','','',0,'0000-00-00 00:00:00',0,0),(473,0,'plg_fields_textarea','plugin','textarea','fields',0,1,1,0,'{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}','','','',0,'0000-00-00 00:00:00',0,0),(474,0,'plg_fields_url','plugin','url','fields',0,1,1,0,'{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}','','','',0,'0000-00-00 00:00:00',0,0),(475,0,'plg_fields_user','plugin','user','fields',0,1,1,0,'{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}','','','',0,'0000-00-00 00:00:00',0,0),(476,0,'plg_fields_usergrouplist','plugin','usergrouplist','fields',0,1,1,0,'{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}','','','',0,'0000-00-00 00:00:00',0,0),(477,0,'plg_content_fields','plugin','fields','content',0,1,1,0,'{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}','','','',0,'0000-00-00 00:00:00',0,0),(478,0,'plg_editors-xtd_fields','plugin','fields','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}','','','',0,'0000-00-00 00:00:00',0,0),(507,0,'isis','template','isis','',1,1,1,0,'{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"templateColor\":\"\",\"logoFile\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(600,802,'English (en-GB)','language','en-GB','',0,1,1,1,'{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"en-GB site language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(601,802,'English (en-GB)','language','en-GB','',1,1,1,1,'{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"en-GB administrator language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(700,0,'files_joomla','file','joomla','',0,1,1,1,'{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(802,0,'English (en-GB) Language Pack','package','pkg_en-GB','',0,1,1,1,'{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}','','','',0,'0000-00-00 00:00:00',0,0),(10000,10002,'GermanDE','language','de-DE','',0,1,0,0,'{\"name\":\"German (DE)\",\"type\":\"language\",\"creationDate\":\"26.04.2017\",\"author\":\"J!German\",\"copyright\":\"Copyright (C) 2008 - 2017 J!German. All rights reserved.\",\"authorEmail\":\"team@jgerman.de\",\"authorUrl\":\"http:\\/\\/www.jgerman.de\",\"version\":\"3.7.0.1\",\"description\":\"\\n\\t  <img style=\\\"margin: 5px; vertical-align: middle;\\\" alt=\\\"German (Deutsch)\\\" src=\\\"data:;base64,R0lGODlhEgAMAJEAAP\\/OAAAAAN0AAAAAACH5BAAAAAAALAAAAAASAAwAAAIXjI+py+2vhJy02ouz3hb4D4biSJbmKRYAOw==\\\" height=\\\"12\\\" width=\\\"18\\\" \\/>Deutsche Frontend (Website)-\\u00dcbersetzung f\\u00fcr Joomla! 3.7.0\\n\\t  <br \\/>\\n\\t  <img style=\\\"margin: 5px; vertical-align: middle;\\\" alt=\\\"English (Englisch)\\\" src=\\\"data:image\\/gif;base64,R0lGODlhEgAMAPcAANQYLe+nr+iPmgElcdQuQtQtQtq\\/zc8UK88TKu2Sm+A4SOucpn2RvxIseCBLmRIpdtIWLAEkctAUK\\/\\/f3g4tguqXodozRcwDHNa8y8fT5h9GlP\\/7+R82fcwIIPOCiRg2fwc0fP\\/6+AEohAwqgffV2QYuhfaTmQApgi1VngAZd9Y0SOmTnaysytIjOPixtbZlgOxVYehUYPbP09FqfWByq\\/XL0BIndO2Fju6AieZ8iQAaed9gcOnm7t28wgEpdImUt2B\\/uOtWYsAPHP\\/o5t5SYdzs98pwd\\/KXn\\/\\/v7tjo9WRyqXBtkgEdbPbu8c0MJHdomvB4gHBglMwGH7Nphm6Zy9Pq6uufqfjh5NUwRM8SKhIqd9c5TNc4TNUxRRIjcxAvg9FpfPCmpiBOjv\\/r6cYgKhIfb\\/\\/i4fSTmdm+zClSnOiMl+dXY1RioK5kgxg5hPOZoNMpPmh\\/tTxalmqFut\\/G0tchLdni765RcOiOmQAgfcHZ7v77+3J4o+6UnfTKz\\/\\/\\/\\/OurtYScx66wzThepMy7vwAeeiJLmumQmv\\/m5QAceN00RmOBqgEnc9zr9+lWY+qWoNY0Rw80eudUYWZ1qytZk+unsAYxiup5g+iSnX6Ww7Vif9EQH\\/Df5dbc6hIqdt3w+\\/\\/q6MwFHfOLkuj6\\/+ylrgAVde+aotPQ3+yMls8VLNbc69+lo+6nr9tHWAAPcLTI480GHssAGf\\/\\/\\/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAASAAwAAAjoAH9wKPOggZYPPepsCiPHRgNPXtzwGVKEwZdShUoYAAArAIpEKSwp0RTDERREjRiMyIOGYwAHIia9kORhApspRC6NsZOJDgRYlQK1WYODxKc5gjJcYeUnxB8ZCKRYQeKihqw9p1goUNRlC6QCBOAcyNICCxcVBApYUBCrrdtYFw6k6vDW7RsBAlYsqJAgBwInO\\/ocwvNoAaYjQPTIkmXKBA9OEkIBGiVrg5oEqqi8aoIqyIwoGjBwJDWIRiczN1rdOQMDzBNDOk5s7JjGFYU4SUCJMrJETIQBPkAQIiNkFaUBjJhEWlQlIAA7\\\" height=\\\"12\\\" width=\\\"18\\\" \\/>German Frontend (Website) translation for Joomla! 3.7.0\\n\\t\",\"group\":\"\",\"filename\":\"install\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10001,10002,'GermanDE','language','de-DE','',1,1,0,0,'{\"name\":\"German (DE)\",\"type\":\"language\",\"creationDate\":\"26.04.2017\",\"author\":\"J!German\",\"copyright\":\"Copyright (C) 2008 - 2017 J!German. All rights reserved.\",\"authorEmail\":\"team@jgerman.de\",\"authorUrl\":\"http:\\/\\/www.jgerman.de\",\"version\":\"3.7.0.1\",\"description\":\"\\n      <img style=\\\"margin: 5px; vertical-align: middle;\\\" alt=\\\"German (Deutsch)\\\" src=\\\"data:;base64,R0lGODlhEgAMAJEAAP\\/OAAAAAN0AAAAAACH5BAAAAAAALAAAAAASAAwAAAIXjI+py+2vhJy02ouz3hb4D4biSJbmKRYAOw==\\\" height=\\\"12\\\" width=\\\"18\\\" \\/>Deutsche Backend (Administrator)-\\u00dcbersetzung f\\u00fcr Joomla! 3.7.0\\n      <br \\/>\\n      <img style=\\\"margin: 5px; vertical-align: middle;\\\" alt=\\\"English (Englisch)\\\" src=\\\"data:image\\/gif;base64,R0lGODlhEgAMAPcAANQYLe+nr+iPmgElcdQuQtQtQtq\\/zc8UK88TKu2Sm+A4SOucpn2RvxIseCBLmRIpdtIWLAEkctAUK\\/\\/f3g4tguqXodozRcwDHNa8y8fT5h9GlP\\/7+R82fcwIIPOCiRg2fwc0fP\\/6+AEohAwqgffV2QYuhfaTmQApgi1VngAZd9Y0SOmTnaysytIjOPixtbZlgOxVYehUYPbP09FqfWByq\\/XL0BIndO2Fju6AieZ8iQAaed9gcOnm7t28wgEpdImUt2B\\/uOtWYsAPHP\\/o5t5SYdzs98pwd\\/KXn\\/\\/v7tjo9WRyqXBtkgEdbPbu8c0MJHdomvB4gHBglMwGH7Nphm6Zy9Pq6uufqfjh5NUwRM8SKhIqd9c5TNc4TNUxRRIjcxAvg9FpfPCmpiBOjv\\/r6cYgKhIfb\\/\\/i4fSTmdm+zClSnOiMl+dXY1RioK5kgxg5hPOZoNMpPmh\\/tTxalmqFut\\/G0tchLdni765RcOiOmQAgfcHZ7v77+3J4o+6UnfTKz\\/\\/\\/\\/OurtYScx66wzThepMy7vwAeeiJLmumQmv\\/m5QAceN00RmOBqgEnc9zr9+lWY+qWoNY0Rw80eudUYWZ1qytZk+unsAYxiup5g+iSnX6Ww7Vif9EQH\\/Df5dbc6hIqdt3w+\\/\\/q6MwFHfOLkuj6\\/+ylrgAVde+aotPQ3+yMls8VLNbc69+lo+6nr9tHWAAPcLTI480GHssAGf\\/\\/\\/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAASAAwAAAjoAH9wKPOggZYPPepsCiPHRgNPXtzwGVKEwZdShUoYAAArAIpEKSwp0RTDERREjRiMyIOGYwAHIia9kORhApspRC6NsZOJDgRYlQK1WYODxKc5gjJcYeUnxB8ZCKRYQeKihqw9p1goUNRlC6QCBOAcyNICCxcVBApYUBCrrdtYFw6k6vDW7RsBAlYsqJAgBwInO\\/ocwvNoAaYjQPTIkmXKBA9OEkIBGiVrg5oEqqi8aoIqyIwoGjBwJDWIRiczN1rdOQMDzBNDOk5s7JjGFYU4SUCJMrJETIQBPkAQIiNkFaUBjJhEWlQlIAA7\\\" height=\\\"12\\\" width=\\\"18\\\" \\/>German Backend (Administrator) translation for Joomla! 3.7.0\\n    \",\"group\":\"\",\"filename\":\"install\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10002,0,'German (Germany) Language Pack','package','pkg_de-DE','',0,1,1,0,'{\"name\":\"German (Germany) Language Pack\",\"type\":\"package\",\"creationDate\":\"26.04.2017\",\"author\":\"J!German\",\"copyright\":\"\",\"authorEmail\":\"team@jgerman.de\",\"authorUrl\":\"http:\\/\\/www.jgerman.de\",\"version\":\"3.7.0.1\",\"description\":\"\\n    <div style=\\\"text-align: center;\\\">\\n      <h2>Deutsches \\u201eFull\\u201c-Sprachpaket f\\u00fcr Joomla! 3.7.0 von <a title=\\\"J!German\\\" href=\\\"http:\\/\\/www.jgerman.de\\\" target=\\\"_blank\\\">J!German<\\/a><\\/h2>\\n      <h3><span style=\\\"color: #008000;\\\">\\u00dcbersetzungsversion: 3.7.0v1<\\/span><\\/h3>\\n      <hr \\/>\\n      <table rules=\\\"all\\\" frame=\\\"border\\\" style=\\\"width: 90%; border-color: #000000; border-width: 1px; border-style: solid;\\\" align=\\\"center\\\" border=\\\"1\\\">\\n      <colgroup> <col width=\\\"30%\\\" \\/> <col width=\\\"60\\\" \\/> <\\/colgroup>\\n      <tbody>\\n        <tr>\\n          <td>\\n            <ul>\\n              <li>Frontend (Website)-\\u00dcbersetzung<\\/li>\\n            <\\/ul>\\n          <\\/td>\\n          <td rowspan=\\\"2\\\">\\n            <ul>\\n              <li>\\n                <span style=\\\"text-decoration: underline;\\\">Neuinstallation:<\\/span>\\n                <br \\/>\\n                Legen Sie die deutsche Sprache unter <a title=\\\"Language(s)\\\" href=\\\"index.php?option=com_languages\\\" target=\\\"_blank\\\">\\u201eExtensions\\u201c \\u2192 \\u201eLanguage(s)\\u201c<\\/a> als Standardsprache (\\u201eDefault\\u201c), sowohl f\\u00fcr die Website (\\u201eInstalled - Site\\u201c) als auch f\\u00fcr die Administration (\\u201eInstalled - Administrator\\u201c), fest.\\n              <\\/li>\\n              <br \\/>\\n              <li>\\n                <span style=\\\"text-decoration: underline;\\\">Aktualisierung:<\\/span>\\n                <br \\/>\\n                Es sind keine weiteren Schritte erforderlich.\\n              <\\/li>\\n            <\\/ul>\\n          <\\/td>\\n        <\\/tr>\\n        <tr>\\n          <td>\\n            <ul>\\n              <li>Backend (Administrator)-\\u00dcbersetzung<\\/li>\\n            <\\/ul>\\n          <\\/td>\\n        <\\/tr>\\n      <\\/tbody>\\n      <\\/table>\\n      <br \\/>\\n      <span style=\\\"text-decoration: underline;\\\">Hinweis:<\\/span> Dieses Paket unterst\\u00fctzt die Joomla! eigene <a title=\\\"Joomla!-Aktualisierungsfunktion\\\" href=\\\"index.php?option=com_installer&amp;view=update\\\" target=\\\"_blank\\\">Aktualisierungsfunktion<\\/a>!\\n    <\\/div>\\n    \",\"group\":\"\",\"filename\":\"pkg_de-DE\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10003,10018,'COM_JCE','component','com_jce','',1,1,0,0,'{\"name\":\"COM_JCE\",\"type\":\"component\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"COM_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10004,10018,'plg_editors_jce','plugin','jce','editors',0,1,1,0,'{\"name\":\"plg_editors_jce\",\"type\":\"plugin\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"WF_EDITOR_PLUGIN_DESC\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10005,10018,'plg_system_jce','plugin','jce','system',0,1,1,0,'{\"name\":\"plg_system_jce\",\"type\":\"plugin\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"PLG_SYSTEM_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10007,0,'AP Ajax Quick Contact','module','mod_ap_ajax_quick_contact','',0,1,0,0,'{\"name\":\"AP Ajax Quick Contact\",\"type\":\"module\",\"creationDate\":\"April 2014\",\"author\":\"Aplikko\",\"copyright\":\"Copyright @ 2014 Aplikko.com. All rights reserved.\",\"authorEmail\":\"contact@aplikko.com\",\"authorUrl\":\"http:\\/\\/www.aplikko.com\",\"version\":\"3.3\",\"description\":\"\",\"group\":\"\",\"filename\":\"mod_ap_ajax_quick_contact\"}','{\"form_width\":\"50\",\"name\":\"Name:\",\"email\":\"Email:\",\"message\":\"Message:\",\"captcha_label\":\"1\",\"captcha\":\"Captcha:\",\"submit\":\"Send\",\"subject\":\"Quick Contact from the Website\",\"ap_error_field\":\"Please, enter your\",\"ap_error_email\":\"You have entered incorrect email. Please fix it!\",\"ap_captchaError\":\"Please enter correct Captcha!\",\"ap_send_message\":\"<div style=\\\"text-align:center;\\\"><h3>Your Message has been sent!<\\/h3><p><i style=\\\"font-size:150%;color:#468847;vertical-align:middle;margin-top:1px;margin-right:5px;\\\" class=\\\"fa fa-check-square-o\\\"><\\/i><span style=\\\"margin-right:5px;\\\" class=\\\"label label-success\\\"><strong>Thank you!<\\/strong><\\/span> We will get to you as soon as possible.<\\/p><\\/div>\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}','','',0,'0000-00-00 00:00:00',0,0),(10010,0,'COM_XMAP','component','com_xmap','',1,1,0,0,'{\"name\":\"COM_XMAP\",\"type\":\"component\",\"creationDate\":\"2015-03-01\",\"author\":\"Branko Wilhelm\",\"copyright\":\"(c) 2005 - 2009 Joomla! Vargas. All rights reserved; (c) 2015 Branko Wilhelm. All rights reserved.\",\"authorEmail\":\"branko.wilhelm@gmail.com\",\"authorUrl\":\"www.z-index.net\",\"version\":\"3.0 rev 302\",\"description\":\"mapX - Sitemap Generator for Joomla!\",\"group\":\"\",\"filename\":\"xmap\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10011,0,'PLG_XMAP_COM_CONTENT','plugin','com_content','xmap',0,1,1,0,'{\"name\":\"PLG_XMAP_COM_CONTENT\",\"type\":\"plugin\",\"creationDate\":\"01\\/26\\/2011\",\"author\":\"Guillermo Vargas\",\"copyright\":\"(c) 2005 - 2009 Joomla! Vargas. All rights reserved; (c) 2015 Branko Wilhelm. All rights reserved.\",\"authorEmail\":\"guille@vargas.co.cr\",\"authorUrl\":\"joomla.vargas.co.cr\",\"version\":\"3.0 rev 302\",\"description\":\"PLG_XMAP_COM_CONTENT_DESC\",\"group\":\"\",\"filename\":\"com_content\"}','{\"expand_categories\":\"1\",\"expand_featured\":\"1\",\"include_archived\":\"2\",\"show_unauth\":\"0\",\"add_pagebreaks\":\"1\",\"max_art\":\"0\",\"max_art_age\":\"0\",\"add_images\":\"1\",\"cat_priority\":\"-1\",\"cat_changefreq\":\"-1\",\"art_priority\":\"-1\",\"art_changefreq\":\"-1\",\"keywords\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(10012,0,'PLG_XMAP_COM_TAGS','plugin','com_tags','xmap',0,1,1,0,'{\"name\":\"PLG_XMAP_COM_TAGS\",\"type\":\"plugin\",\"creationDate\":\"Sep 2013\",\"author\":\"Branko Wilhelm\",\"copyright\":\"Copyright (c) 2013 - 2015 Branko Wilhelm\",\"authorEmail\":\"branko.wilhelm@gmail.com\",\"authorUrl\":\"www.z-index.net\",\"version\":\"rev 302\",\"description\":\"PLG_XMAP_COM_TAGS_DESC\",\"group\":\"\",\"filename\":\"com_tags\"}','{\"include_tags\":\"1\",\"show_unauth\":\"0\",\"tag_priority\":\"-1\",\"tag_changefreq\":\"-1\"}','','',0,'0000-00-00 00:00:00',0,0),(10013,0,'089-Standard','template','089-standard','',0,1,1,0,'{\"name\":\"089-Standard\",\"type\":\"template\",\"creationDate\":\"29\\/08\\/2015\",\"author\":\"089-Webdesign\",\"copyright\":\"Copyright (C) 2015 089webdesign.de. All rights reserved.\",\"authorEmail\":\"info@089webdesign.de\",\"authorUrl\":\"http:\\/\\/089webdesign.de\\/\",\"version\":\"1.0\",\"description\":\"\\n\\t\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"logoFile\":\"\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"googleFont\":\"0\",\"googleFontName\":\"Open+Sans\"}','','',0,'0000-00-00 00:00:00',0,0),(10014,10018,'plg_content_jce','plugin','jce','content',0,1,1,0,'{\"name\":\"plg_content_jce\",\"type\":\"plugin\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"PLG_CONTENT_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10015,10018,'plg_installer_jce','plugin','jce','installer',0,1,1,0,'{\"name\":\"plg_installer_jce\",\"type\":\"plugin\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"PLG_INSTALLER_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10016,10018,'plg_extension_jce','plugin','jce','extension',0,1,1,0,'{\"name\":\"plg_extension_jce\",\"type\":\"plugin\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2017 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.6.12\",\"description\":\"PLG_EXTENSION_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10017,10018,'plg_quickicon_jce','plugin','jce','quickicon',0,1,1,0,'{\"name\":\"plg_quickicon_jce\",\"type\":\"plugin\",\"creationDate\":\"31-08-2016\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2016 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"www.joomalcontenteditor.net\",\"version\":\"2.6.0-pro-beta3\",\"description\":\"PLG_QUICKICON_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10018,0,'PKG_JCE','package','pkg_jce','',0,1,1,0,'{\"name\":\"PKG_JCE\",\"type\":\"package\",\"creationDate\":\"27-04-2017\",\"author\":\"Ryan Demmer\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"2.6.12\",\"description\":\"PKG_JCE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pkg_jce\"}','{}','','',0,'0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `w089_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_fields`
--

DROP TABLE IF EXISTS `w089_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_created_user_id` (`created_user_id`),
  KEY `idx_access` (`access`),
  KEY `idx_context` (`context`(191)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_fields`
--

LOCK TABLES `w089_fields` WRITE;
/*!40000 ALTER TABLE `w089_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_fields_categories`
--

DROP TABLE IF EXISTS `w089_fields_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_fields_categories`
--

LOCK TABLES `w089_fields_categories` WRITE;
/*!40000 ALTER TABLE `w089_fields_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_fields_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_fields_groups`
--

DROP TABLE IF EXISTS `w089_fields_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_fields_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_created_by` (`created_by`),
  KEY `idx_access` (`access`),
  KEY `idx_context` (`context`(191)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_fields_groups`
--

LOCK TABLES `w089_fields_groups` WRITE;
/*!40000 ALTER TABLE `w089_fields_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_fields_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_fields_values`
--

DROP TABLE IF EXISTS `w089_fields_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_fields_values` (
  `field_id` int(10) unsigned NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `idx_field_id` (`field_id`),
  KEY `idx_item_id` (`item_id`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_fields_values`
--

LOCK TABLES `w089_fields_values` WRITE;
/*!40000 ALTER TABLE `w089_fields_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_fields_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_filters`
--

DROP TABLE IF EXISTS `w089_finder_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` mediumtext NOT NULL,
  `params` longtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_filters`
--

LOCK TABLES `w089_finder_filters` WRITE;
/*!40000 ALTER TABLE `w089_finder_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links`
--

DROP TABLE IF EXISTS `w089_finder_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`),
  KEY `idx_title` (`title`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links`
--

LOCK TABLES `w089_finder_links` WRITE;
/*!40000 ALTER TABLE `w089_finder_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms0`
--

DROP TABLE IF EXISTS `w089_finder_links_terms0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms0`
--

LOCK TABLES `w089_finder_links_terms0` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms0` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms1`
--

DROP TABLE IF EXISTS `w089_finder_links_terms1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms1`
--

LOCK TABLES `w089_finder_links_terms1` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms1` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms2`
--

DROP TABLE IF EXISTS `w089_finder_links_terms2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms2`
--

LOCK TABLES `w089_finder_links_terms2` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms2` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms3`
--

DROP TABLE IF EXISTS `w089_finder_links_terms3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms3`
--

LOCK TABLES `w089_finder_links_terms3` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms3` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms4`
--

DROP TABLE IF EXISTS `w089_finder_links_terms4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms4`
--

LOCK TABLES `w089_finder_links_terms4` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms4` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms5`
--

DROP TABLE IF EXISTS `w089_finder_links_terms5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms5`
--

LOCK TABLES `w089_finder_links_terms5` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms5` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms6`
--

DROP TABLE IF EXISTS `w089_finder_links_terms6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms6`
--

LOCK TABLES `w089_finder_links_terms6` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms6` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms7`
--

DROP TABLE IF EXISTS `w089_finder_links_terms7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms7`
--

LOCK TABLES `w089_finder_links_terms7` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms7` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms8`
--

DROP TABLE IF EXISTS `w089_finder_links_terms8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms8`
--

LOCK TABLES `w089_finder_links_terms8` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms8` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_terms9`
--

DROP TABLE IF EXISTS `w089_finder_links_terms9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_terms9`
--

LOCK TABLES `w089_finder_links_terms9` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_terms9` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_terms9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termsa`
--

DROP TABLE IF EXISTS `w089_finder_links_termsa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termsa`
--

LOCK TABLES `w089_finder_links_termsa` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termsa` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termsa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termsb`
--

DROP TABLE IF EXISTS `w089_finder_links_termsb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termsb`
--

LOCK TABLES `w089_finder_links_termsb` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termsb` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termsb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termsc`
--

DROP TABLE IF EXISTS `w089_finder_links_termsc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termsc`
--

LOCK TABLES `w089_finder_links_termsc` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termsc` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termsc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termsd`
--

DROP TABLE IF EXISTS `w089_finder_links_termsd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termsd`
--

LOCK TABLES `w089_finder_links_termsd` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termsd` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termsd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termse`
--

DROP TABLE IF EXISTS `w089_finder_links_termse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termse`
--

LOCK TABLES `w089_finder_links_termse` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termse` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_links_termsf`
--

DROP TABLE IF EXISTS `w089_finder_links_termsf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_links_termsf`
--

LOCK TABLES `w089_finder_links_termsf` WRITE;
/*!40000 ALTER TABLE `w089_finder_links_termsf` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_links_termsf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_taxonomy`
--

DROP TABLE IF EXISTS `w089_finder_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_taxonomy`
--

LOCK TABLES `w089_finder_taxonomy` WRITE;
/*!40000 ALTER TABLE `w089_finder_taxonomy` DISABLE KEYS */;
INSERT INTO `w089_finder_taxonomy` VALUES (1,0,'ROOT',0,0,0);
/*!40000 ALTER TABLE `w089_finder_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_taxonomy_map`
--

DROP TABLE IF EXISTS `w089_finder_taxonomy_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_taxonomy_map`
--

LOCK TABLES `w089_finder_taxonomy_map` WRITE;
/*!40000 ALTER TABLE `w089_finder_taxonomy_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_taxonomy_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_terms`
--

DROP TABLE IF EXISTS `w089_finder_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_terms`
--

LOCK TABLES `w089_finder_terms` WRITE;
/*!40000 ALTER TABLE `w089_finder_terms` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_terms_common`
--

DROP TABLE IF EXISTS `w089_finder_terms_common`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_terms_common`
--

LOCK TABLES `w089_finder_terms_common` WRITE;
/*!40000 ALTER TABLE `w089_finder_terms_common` DISABLE KEYS */;
INSERT INTO `w089_finder_terms_common` VALUES ('a','en'),('about','en'),('after','en'),('ago','en'),('all','en'),('am','en'),('an','en'),('and','en'),('ani','en'),('any','en'),('are','en'),('aren\'t','en'),('as','en'),('at','en'),('be','en'),('but','en'),('by','en'),('for','en'),('from','en'),('get','en'),('go','en'),('how','en'),('if','en'),('in','en'),('into','en'),('is','en'),('isn\'t','en'),('it','en'),('its','en'),('me','en'),('more','en'),('most','en'),('must','en'),('my','en'),('new','en'),('no','en'),('none','en'),('not','en'),('noth','en'),('nothing','en'),('of','en'),('off','en'),('often','en'),('old','en'),('on','en'),('onc','en'),('once','en'),('onli','en'),('only','en'),('or','en'),('other','en'),('our','en'),('ours','en'),('out','en'),('over','en'),('page','en'),('she','en'),('should','en'),('small','en'),('so','en'),('some','en'),('than','en'),('thank','en'),('that','en'),('the','en'),('their','en'),('theirs','en'),('them','en'),('then','en'),('there','en'),('these','en'),('they','en'),('this','en'),('those','en'),('thus','en'),('time','en'),('times','en'),('to','en'),('too','en'),('true','en'),('under','en'),('until','en'),('up','en'),('upon','en'),('use','en'),('user','en'),('users','en'),('veri','en'),('version','en'),('very','en'),('via','en'),('want','en'),('was','en'),('way','en'),('were','en'),('what','en'),('when','en'),('where','en'),('whi','en'),('which','en'),('who','en'),('whom','en'),('whose','en'),('why','en'),('wide','en'),('will','en'),('with','en'),('within','en'),('without','en'),('would','en'),('yes','en'),('yet','en'),('you','en'),('your','en'),('yours','en');
/*!40000 ALTER TABLE `w089_finder_terms_common` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_tokens`
--

DROP TABLE IF EXISTS `w089_finder_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_tokens`
--

LOCK TABLES `w089_finder_tokens` WRITE;
/*!40000 ALTER TABLE `w089_finder_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_tokens_aggregate`
--

DROP TABLE IF EXISTS `w089_finder_tokens_aggregate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_tokens_aggregate`
--

LOCK TABLES `w089_finder_tokens_aggregate` WRITE;
/*!40000 ALTER TABLE `w089_finder_tokens_aggregate` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_tokens_aggregate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_finder_types`
--

DROP TABLE IF EXISTS `w089_finder_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_finder_types`
--

LOCK TABLES `w089_finder_types` WRITE;
/*!40000 ALTER TABLE `w089_finder_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_finder_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_languages`
--

DROP TABLE IF EXISTS `w089_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_languages`
--

LOCK TABLES `w089_languages` WRITE;
/*!40000 ALTER TABLE `w089_languages` DISABLE KEYS */;
INSERT INTO `w089_languages` VALUES (1,0,'en-GB','English (UK)','English (UK)','en','en','','','','',1,1,1);
/*!40000 ALTER TABLE `w089_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_menu`
--

DROP TABLE IF EXISTS `w089_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_language` (`language`),
  KEY `idx_alias` (`alias`(100)),
  KEY `idx_path` (`path`(100))
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_menu`
--

LOCK TABLES `w089_menu` WRITE;
/*!40000 ALTER TABLE `w089_menu` DISABLE KEYS */;
INSERT INTO `w089_menu` VALUES (1,'','Menu_Item_Root','root','','','','',1,0,0,0,0,'0000-00-00 00:00:00',0,0,'',0,'',0,61,0,'*',0),(2,'main','com_banners','Banners','','Banners','index.php?option=com_banners','component',1,1,1,4,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',1,10,0,'*',1),(3,'main','com_banners','Banners','','Banners/Banners','index.php?option=com_banners','component',1,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',2,3,0,'*',1),(4,'main','com_banners_categories','Categories','','Banners/Categories','index.php?option=com_categories&extension=com_banners','component',1,2,2,6,0,'0000-00-00 00:00:00',0,0,'class:banners-cat',0,'',4,5,0,'*',1),(5,'main','com_banners_clients','Clients','','Banners/Clients','index.php?option=com_banners&view=clients','component',1,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners-clients',0,'',6,7,0,'*',1),(6,'main','com_banners_tracks','Tracks','','Banners/Tracks','index.php?option=com_banners&view=tracks','component',1,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners-tracks',0,'',8,9,0,'*',1),(7,'main','com_contact','Contacts','','Contacts','index.php?option=com_contact','component',1,1,1,8,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',19,24,0,'*',1),(8,'main','com_contact_contacts','Contacts','','Contacts/Contacts','index.php?option=com_contact','component',1,7,2,8,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',20,21,0,'*',1),(9,'main','com_contact_categories','Categories','','Contacts/Categories','index.php?option=com_categories&extension=com_contact','component',1,7,2,6,0,'0000-00-00 00:00:00',0,0,'class:contact-cat',0,'',22,23,0,'*',1),(10,'main','com_messages','Messaging','','Messaging','index.php?option=com_messages','component',1,1,1,15,0,'0000-00-00 00:00:00',0,0,'class:messages',0,'',25,28,0,'*',1),(11,'main','com_messages_add','New Private Message','','Messaging/New Private Message','index.php?option=com_messages&task=message.add','component',1,10,2,15,0,'0000-00-00 00:00:00',0,0,'class:messages-add',0,'',26,27,0,'*',1),(13,'main','com_newsfeeds','News Feeds','','News Feeds','index.php?option=com_newsfeeds','component',1,1,1,17,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',29,34,0,'*',1),(14,'main','com_newsfeeds_feeds','Feeds','','News Feeds/Feeds','index.php?option=com_newsfeeds','component',1,13,2,17,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',30,31,0,'*',1),(15,'main','com_newsfeeds_categories','Categories','','News Feeds/Categories','index.php?option=com_categories&extension=com_newsfeeds','component',1,13,2,6,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds-cat',0,'',32,33,0,'*',1),(16,'main','com_redirect','Redirect','','Redirect','index.php?option=com_redirect','component',1,1,1,24,0,'0000-00-00 00:00:00',0,0,'class:redirect',0,'',35,36,0,'*',1),(17,'main','com_search','Basic Search','','Basic Search','index.php?option=com_search','component',1,1,1,19,0,'0000-00-00 00:00:00',0,0,'class:search',0,'',37,38,0,'*',1),(18,'main','com_finder','Smart Search','','Smart Search','index.php?option=com_finder','component',1,1,1,27,0,'0000-00-00 00:00:00',0,0,'class:finder',0,'',39,40,0,'*',1),(20,'main','com_tags','Tags','','Tags','index.php?option=com_tags','component',1,1,1,29,0,'0000-00-00 00:00:00',0,1,'class:tags',0,'',41,42,0,'',1),(21,'main','com_postinstall','Post-installation messages','','Post-installation messages','index.php?option=com_postinstall','component',1,1,1,32,0,'0000-00-00 00:00:00',0,1,'class:postinstall',0,'',43,44,0,'*',1),(111,'mainmenu','Home','home','','home','index.php?option=com_content&view=article&id=1','component',1,1,1,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"0\",\"link_titles\":\"0\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',11,12,1,'*',0),(112,'mainmenu','Menue 1','menue-1','','menue-1','index.php?option=com_content&view=article&id=1','component',1,1,1,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',13,18,0,'*',0),(113,'mainmenu','Menue-Unterpunkt 1','menue-unterpunkt-1','','menue-1/menue-unterpunkt-1','index.php?option=com_content&view=article&id=1','component',1,112,2,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',14,15,0,'*',0),(119,'hidden-menu','404-Seite','404','','404','index.php?option=com_content&view=article&id=2','component',0,1,1,22,0,'0000-00-00 00:00:00',0,1,' ',0,'{\"show_title\":\"\",\"link_titles\":\"0\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',45,46,0,'*',0),(120,'mainmenu','Bootstrap Slideshow','bootstrap-slideshow','','menue-1/bootstrap-slideshow','index.php?option=com_content&view=article&id=3','component',1,112,2,22,0,'0000-00-00 00:00:00',0,1,' ',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_icons\":\"\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":\"1\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',16,17,0,'*',0),(136,'main','com_joomlaupdate','com-joomlaupdate','','com-joomlaupdate','index.php?option=com_joomlaupdate','component',1,1,1,28,0,'0000-00-00 00:00:00',0,1,'class:joomlaupdate',0,'{}',47,48,0,'',1),(142,'main','COM_XMAP_TITLE','com-xmap-title','','com-xmap-title','index.php?option=com_xmap','component',1,1,1,10010,0,'0000-00-00 00:00:00',0,1,'class:component',0,'{}',49,50,0,'',1),(152,'main','com_associations','multilingual-associations','','multilingual-associations','index.php?option=com_associations','component',1,1,1,34,0,'0000-00-00 00:00:00',0,1,'class:associations',0,'{}',51,52,0,'*',1),(153,'main','COM_JCE','com-jce','','com-jce','index.php?option=com_jce','component',1,1,1,10003,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/logo.png',0,'{}',53,60,0,'',1),(154,'main','COM_JCE_MENU_CPANEL','com-jce-menu-cpanel','','com-jce/com-jce-menu-cpanel','index.php?option=com_jce','component',1,153,2,10003,0,'0000-00-00 00:00:00',0,1,'class:component',0,'{}',54,55,0,'',1),(155,'main','COM_JCE_MENU_CONFIG','com-jce-menu-config','','com-jce/com-jce-menu-config','index.php?option=com_jce&view=config','component',1,153,2,10003,0,'0000-00-00 00:00:00',0,1,'class:component',0,'{}',56,57,0,'',1),(156,'main','COM_JCE_MENU_PROFILES','com-jce-menu-profiles','','com-jce/com-jce-menu-profiles','index.php?option=com_jce&view=profiles','component',1,153,2,10003,0,'0000-00-00 00:00:00',0,1,'class:component',0,'{}',58,59,0,'',1);
/*!40000 ALTER TABLE `w089_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_menu_types`
--

DROP TABLE IF EXISTS `w089_menu_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_menu_types`
--

LOCK TABLES `w089_menu_types` WRITE;
/*!40000 ALTER TABLE `w089_menu_types` DISABLE KEYS */;
INSERT INTO `w089_menu_types` VALUES (1,0,'mainmenu','Main Menu','The main menu for the site',0),(2,0,'footer-menu','Footer-Menu','footer-menu',0),(3,0,'hidden-menu','Hidden-Menu','Hidden-Menu',0);
/*!40000 ALTER TABLE `w089_menu_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_messages`
--

DROP TABLE IF EXISTS `w089_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_messages`
--

LOCK TABLES `w089_messages` WRITE;
/*!40000 ALTER TABLE `w089_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_messages_cfg`
--

DROP TABLE IF EXISTS `w089_messages_cfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_messages_cfg`
--

LOCK TABLES `w089_messages_cfg` WRITE;
/*!40000 ALTER TABLE `w089_messages_cfg` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_messages_cfg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_modules`
--

DROP TABLE IF EXISTS `w089_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_modules`
--

LOCK TABLES `w089_modules` WRITE;
/*!40000 ALTER TABLE `w089_modules` DISABLE KEYS */;
INSERT INTO `w089_modules` VALUES (1,39,'Main Menu','','',1,'menu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',1,0,'{\"menutype\":\"mainmenu\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"nav-pills\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"089-standard-custom\"}',0,'*'),(2,40,'Login','','',1,'login',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_login',1,1,'',1,'*'),(3,41,'Popular Articles','','',3,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_popular',3,1,'{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(4,42,'Recently Added Articles','','',4,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_latest',3,1,'{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(8,43,'Toolbar','','',1,'toolbar',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_toolbar',3,1,'',1,'*'),(9,44,'Quick Icons','','',1,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_quickicon',3,1,'',1,'*'),(10,45,'Logged-in Users','','',2,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_logged',3,1,'{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(12,46,'Admin Menu','','',1,'menu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',3,1,'{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}',1,'*'),(13,47,'Admin Submenu','','',1,'submenu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_submenu',3,1,'',1,'*'),(14,48,'User Status','','',2,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_status',3,1,'',1,'*'),(15,49,'Title','','',1,'title',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_title',3,1,'',1,'*'),(16,50,'Login Form','','',7,'position-7',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_login',1,1,'{\"greeting\":\"1\",\"name\":\"0\"}',0,'*'),(17,51,'Breadcrumbs','','',1,'breadcrumbs',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_breadcrumbs',1,0,'{\"showHere\":\"1\",\"showHome\":\"1\",\"homeText\":\"\",\"showLast\":\"1\",\"separator\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(79,52,'Multilanguage status','','',1,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_multilangstatus',3,1,'{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(86,53,'Joomla Version','','',1,'footer',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_version',3,1,'{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(87,55,'AP Ajax Quick Contact','','',0,'',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_ap_ajax_quick_contact',1,1,'',0,'*'),(89,59,'testmodul','','<h1>Lorem ipsum dolor sit amet</h1>\r\n<h2>consetetur sadipscing elitr, sed diam nonumy eirmod tempor</h2>\r\n<h3>invidunt ut labore et dolore magna aliquyam erat</h3>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>sed diam voluptua.</li>\r\n<li>At vero eos et accusam et</li>\r\n<li>justo duo&nbsp;<a href=\"#\">dolores</a> et ea rebum.</li>\r\n<li>Stet clita kasd gubergren</li>\r\n<li>no sea<a href=\"#\">t akimata</a> sanctus est Lorem ipsum</li>\r\n</ul>\r\n<p>&nbsp;</p>',1,'footnav',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_custom',1,1,'{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*');
/*!40000 ALTER TABLE `w089_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_modules_menu`
--

DROP TABLE IF EXISTS `w089_modules_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_modules_menu`
--

LOCK TABLES `w089_modules_menu` WRITE;
/*!40000 ALTER TABLE `w089_modules_menu` DISABLE KEYS */;
INSERT INTO `w089_modules_menu` VALUES (1,0),(2,0),(3,0),(4,0),(6,0),(7,0),(8,0),(9,0),(10,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(79,0),(86,0),(89,0);
/*!40000 ALTER TABLE `w089_modules_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_newsfeeds`
--

DROP TABLE IF EXISTS `w089_newsfeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_newsfeeds`
--

LOCK TABLES `w089_newsfeeds` WRITE;
/*!40000 ALTER TABLE `w089_newsfeeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_newsfeeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_overrider`
--

DROP TABLE IF EXISTS `w089_overrider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_overrider`
--

LOCK TABLES `w089_overrider` WRITE;
/*!40000 ALTER TABLE `w089_overrider` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_overrider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_postinstall_messages`
--

DROP TABLE IF EXISTS `w089_postinstall_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_postinstall_messages`
--

LOCK TABLES `w089_postinstall_messages` WRITE;
/*!40000 ALTER TABLE `w089_postinstall_messages` DISABLE KEYS */;
INSERT INTO `w089_postinstall_messages` VALUES (1,700,'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE','PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY','PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION','plg_twofactorauth_totp',1,'action','site://plugins/twofactorauth/totp/postinstall/actions.php','twofactorauth_postinstall_action','site://plugins/twofactorauth/totp/postinstall/actions.php','twofactorauth_postinstall_condition','3.2.0',0),(2,700,'COM_CPANEL_WELCOME_BEGINNERS_TITLE','COM_CPANEL_WELCOME_BEGINNERS_MESSAGE','','com_cpanel',1,'message','','','','','3.2.0',0),(3,700,'COM_CPANEL_MSG_STATS_COLLECTION_TITLE','COM_CPANEL_MSG_STATS_COLLECTION_BODY','','com_cpanel',1,'message','','','admin://components/com_admin/postinstall/statscollection.php','admin_postinstall_statscollection_condition','3.5.0',0),(4,700,'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME','PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY','PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION','plg_system_updatenotification',1,'action','site://plugins/system/updatenotification/postinstall/updatecachetime.php','updatecachetime_postinstall_action','site://plugins/system/updatenotification/postinstall/updatecachetime.php','updatecachetime_postinstall_condition','3.6.3',1),(5,700,'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE','COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY','','com_cpanel',1,'message','','','admin://components/com_admin/postinstall/joomla40checks.php','admin_postinstall_joomla40checks_condition','3.7.0',1),(6,700,'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE','TPL_HATHOR_MESSAGE_POSTINSTALL_BODY','TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION','tpl_hathor',1,'action','admin://templates/hathor/postinstall/hathormessage.php','hathormessage_postinstall_action','admin://templates/hathor/postinstall/hathormessage.php','hathormessage_postinstall_condition','3.7.0',1);
/*!40000 ALTER TABLE `w089_postinstall_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_redirect_links`
--

DROP TABLE IF EXISTS `w089_redirect_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  KEY `idx_link_modifed` (`modified_date`),
  KEY `idx_old_url` (`old_url`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_redirect_links`
--

LOCK TABLES `w089_redirect_links` WRITE;
/*!40000 ALTER TABLE `w089_redirect_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_redirect_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_revslider_static_slides`
--

DROP TABLE IF EXISTS `w089_revslider_static_slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_revslider_static_slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int(9) NOT NULL,
  `params` text NOT NULL,
  `layers` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_revslider_static_slides`
--

LOCK TABLES `w089_revslider_static_slides` WRITE;
/*!40000 ALTER TABLE `w089_revslider_static_slides` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_revslider_static_slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_schemas`
--

DROP TABLE IF EXISTS `w089_schemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_schemas`
--

LOCK TABLES `w089_schemas` WRITE;
/*!40000 ALTER TABLE `w089_schemas` DISABLE KEYS */;
INSERT INTO `w089_schemas` VALUES (700,'3.7.0-2017-04-19'),(10010,'3.0-2015-03-02');
/*!40000 ALTER TABLE `w089_schemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_session`
--

DROP TABLE IF EXISTS `w089_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned DEFAULT NULL,
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_session`
--

LOCK TABLES `w089_session` WRITE;
/*!40000 ALTER TABLE `w089_session` DISABLE KEYS */;
INSERT INTO `w089_session` VALUES ('83329221b9e83128322ddfb4ca87b449',0,1,'1494266470','joomla|s:648:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjI6e3M6NzoiY291bnRlciI7aToxNTA7czo1OiJ0aW1lciI7Tzo4OiJzdGRDbGFzcyI6Mzp7czo1OiJzdGFydCI7aToxNDk0MjUxNzM1O3M6NDoibGFzdCI7aToxNDk0MjY2NDY5O3M6Mzoibm93IjtpOjE0OTQyNjY0NzA7fX1zOjg6InJlZ2lzdHJ5IjtPOjI0OiJKb29tbGFcUmVnaXN0cnlcUmVnaXN0cnkiOjM6e3M6NzoiACoAZGF0YSI7Tzo4OiJzdGRDbGFzcyI6MDp7fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fXM6NDoidXNlciI7Tzo1OiJKVXNlciI6MTp7czoyOiJpZCI7aTowO319fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fQ==\";',0,'');
/*!40000 ALTER TABLE `w089_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_tags`
--

DROP TABLE IF EXISTS `w089_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_language` (`language`),
  KEY `idx_path` (`path`(100)),
  KEY `idx_alias` (`alias`(100))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_tags`
--

LOCK TABLES `w089_tags` WRITE;
/*!40000 ALTER TABLE `w089_tags` DISABLE KEYS */;
INSERT INTO `w089_tags` VALUES (1,0,0,1,0,'','ROOT','root','','',1,0,'0000-00-00 00:00:00',1,'','','','',0,'2011-01-01 00:00:01','',0,'0000-00-00 00:00:00','','',0,'*',1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `w089_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_template_styles`
--

DROP TABLE IF EXISTS `w089_template_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_template_styles`
--

LOCK TABLES `w089_template_styles` WRITE;
/*!40000 ALTER TABLE `w089_template_styles` DISABLE KEYS */;
INSERT INTO `w089_template_styles` VALUES (8,'isis',1,'1','isis - Default','{\"templateColor\":\"\",\"logoFile\":\"\"}'),(9,'089-standard',0,'1','089-Standard - Standard','{\"logoFile\":\"\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"googleFont\":\"0\",\"googleFontName\":\"Open+Sans\"}');
/*!40000 ALTER TABLE `w089_template_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_ucm_base`
--

DROP TABLE IF EXISTS `w089_ucm_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_ucm_base`
--

LOCK TABLES `w089_ucm_base` WRITE;
/*!40000 ALTER TABLE `w089_ucm_base` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_ucm_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_ucm_content`
--

DROP TABLE IF EXISTS `w089_ucm_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_language` (`core_language`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`),
  KEY `idx_alias` (`core_alias`(100)),
  KEY `idx_title` (`core_title`(100)),
  KEY `idx_content_type` (`core_type_alias`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_ucm_content`
--

LOCK TABLES `w089_ucm_content` WRITE;
/*!40000 ALTER TABLE `w089_ucm_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_ucm_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_ucm_history`
--

DROP TABLE IF EXISTS `w089_ucm_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_ucm_history`
--

LOCK TABLES `w089_ucm_history` WRITE;
/*!40000 ALTER TABLE `w089_ucm_history` DISABLE KEYS */;
INSERT INTO `w089_ucm_history` VALUES (1,1,1,'','2015-09-09 12:28:58',332,2138,'bf5bf22cc982293d8645edded1b50c67c9b3eda0','{\"id\":1,\"asset_id\":60,\"title\":\"Demo Beitrag\",\"alias\":\"demo-beitrag\",\"introtext\":\"<h1>Lorem ipsum dolor sit amet<\\/h1>\\r\\n<h2>consetetur sadipscing elitr, sed diam nonumy eirmod tempor<\\/h2>\\r\\n<h3>invidunt ut labore et dolore magna aliquyam erat<\\/h3>\\r\\n<p>&nbsp;<\\/p>\\r\\n<ul>\\r\\n<li>sed diam voluptua.<\\/li>\\r\\n<li>At vero eos et accusam et<\\/li>\\r\\n<li>justo duo&nbsp;<a href=\\\"#\\\">dolores<\\/a> et ea rebum.<\\/li>\\r\\n<li>Stet clita kasd gubergren<\\/li>\\r\\n<li>no sea<a href=\\\"#\\\">t akimata<\\/a> sanctus est Lorem ipsum<\\/li>\\r\\n<\\/ul>\\r\\n<p>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-09-09 12:28:58\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-09-09 12:28:58\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-09-09 12:28:58\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(2,1,1,'','2015-09-09 12:29:54',332,2247,'602beeab0e3a827549b543d55f53c521e3ce396c','{\"id\":1,\"asset_id\":\"60\",\"title\":\"Demo Beitrag\",\"alias\":\"demo-beitrag\",\"introtext\":\"<h1>Lorem ipsum dolor sit amet<\\/h1>\\r\\n<h2>consetetur sadipscing elitr, sed diam nonumy eirmod tempor<\\/h2>\\r\\n<h3>invidunt ut labore et dolore magna aliquyam erat<\\/h3>\\r\\n<p>&nbsp;<\\/p>\\r\\n<ul>\\r\\n<li>sed diam voluptua.<\\/li>\\r\\n<li>At vero eos et accusam et<\\/li>\\r\\n<li>justo duo&nbsp;<a href=\\\"#\\\">dolores<\\/a> et ea rebum.<\\/li>\\r\\n<li>Stet clita kasd gubergren<\\/li>\\r\\n<li>no sea<a href=\\\"#\\\">t akimata<\\/a> sanctus est Lorem ipsum<\\/li>\\r\\n<\\/ul>\\r\\n<p>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-09-09 12:28:58\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-09-09 12:29:54\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-09-09 12:28:58\",\"publish_up\":\"2015-09-09 12:28:58\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/content\\\\\\/logo\\\\\\/lorem_logo_S.png\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/content\\\\\\/logo\\\\\\/lorem_logo_M.png\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(21,2,1,'','2015-10-25 18:22:09',332,47718,'608ac8a30947695bdd900ebd8d53fa7a374d1e05','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n\\thtml {\\r\\n\\t\\tcolor: #222;\\r\\n\\t\\toverflow: hidden;\\r\\n\\t\\t-webkit-user-select: none;\\r\\n\\t\\t-moz-user-select: none;\\r\\n\\t\\t-ms-user-select: none;\\r\\n\\t\\tuser-select: none;\\r\\n\\t\\tfont-size: medium;\\r\\n\\t}\\r\\n\\t\\r\\n\\t.error {\\r\\n\\t\\ttext-align: center;\\r\\n\\t\\tfont-family: \'Gilda Display\', serif;\\r\\n\\t\\ttext-align: center;\\r\\n\\t\\twidth: 100%;\\r\\n\\t\\theight: 240px;\\r\\n\\t\\tmargin: auto;\\r\\n\\t\\tposition: absolute;\\r\\n\\t\\ttop: 0;\\r\\n\\t\\tbottom: 0;\\r\\n\\t\\tleft: -60px;\\r\\n\\t\\tright: 0;\\r\\n\\t\\t-webkit-animation: noise-3 1s linear infinite;\\r\\n\\t\\tanimation: noise-3 1s linear infinite;\\r\\n\\t\\toverflow: default;\\r\\n\\t}\\r\\n\\t\\r\\n\\tbody:after {\\r\\n\\t\\tcontent: \'error 404\';\\r\\n\\t\\tfont-family: OCR-A;\\r\\n\\t\\tfont-size: 100px;\\r\\n\\t\\ttext-align: center;\\r\\n\\t\\twidth: 550px;\\r\\n\\t\\tmargin: auto;\\r\\n\\t\\tposition: absolute;\\r\\n\\t\\ttop: 45%;\\r\\n\\t\\tbottom: 0;\\r\\n\\t\\tleft: 0;\\r\\n\\t\\tright: 35%;\\r\\n\\t\\topacity: 0;\\r\\n\\t\\tcolor: #000;\\r\\n\\t\\t-webkit-animation: noise-1 .2s linear infinite;\\r\\n\\t\\tanimation: noise-1 .2s linear infinite;\\r\\n\\t}\\r\\n\\t\\r\\n\\t\\r\\n\\t.info {\\r\\n\\t\\ttext-align: center;\\r\\n\\t\\twidth: 200px;\\r\\n\\t\\theight: 60px;\\r\\n\\t\\tmargin: auto;\\r\\n\\t\\tposition: absolute;\\r\\n\\t\\ttop: 280px;\\r\\n\\t\\tbottom: 0;\\r\\n\\t\\tleft: 20px;\\r\\n\\t\\tright: 0;\\r\\n\\t\\t-webkit-animation: noise-3 1s linear infinite;\\r\\n\\t\\tanimation: noise-3 1s linear infinite;\\r\\n\\t}\\r\\n\\t\\r\\n\\t\\r\\n\\t.info:after {\\r\\n\\t\\tcontent: \'file not found\';\\r\\n\\t\\tfont-family: OCR-A;\\r\\n\\t\\tfont-size: 100px;\\r\\n\\t\\ttext-align: center;\\r\\n\\t\\twidth: 800px;\\r\\n\\t\\tmargin: auto;\\r\\n\\t\\tposition: absolute;\\r\\n\\t\\ttop: 20px;\\r\\n\\t\\tbottom: 0;\\r\\n\\t\\tleft: 40px;\\r\\n\\t\\tright: 0;\\r\\n\\t\\topacity: 0;\\r\\n\\t\\tcolor: #000;\\r\\n\\t\\t-webkit-animation: noise-1 .2s linear infinite;\\r\\n\\t\\tanimation: noise-1 .2s linear infinite;\\r\\n\\t}\\r\\n\\t\\r\\n\\t@-webkit-keyframes noise-1 {\\r\\n\\t\\t0%,\\r\\n\\t\\t20%,\\r\\n\\t\\t40%,\\r\\n\\t\\t60%,\\r\\n\\t\\t70%,\\r\\n\\t\\t90% {\\r\\n\\t\\t\\topacity: 0;\\r\\n\\t\\t}\\r\\n\\t\\t10% {\\r\\n\\t\\t\\topacity: .1;\\r\\n\\t\\t}\\r\\n\\t\\t50% {\\r\\n\\t\\t\\topacity: .5;\\r\\n\\t\\t\\tleft: -6px;\\r\\n\\t\\t}\\r\\n\\t\\t80% {\\r\\n\\t\\t\\topacity: .3;\\r\\n\\t\\t}\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: .6;\\r\\n\\t\\t\\tleft: 2px;\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@keyframes noise-1 {\\r\\n\\t\\t0%,\\r\\n\\t\\t20%,\\r\\n\\t\\t40%,\\r\\n\\t\\t60%,\\r\\n\\t\\t70%,\\r\\n\\t\\t90% {\\r\\n\\t\\t\\topacity: 0;\\r\\n\\t\\t}\\r\\n\\t\\t10% {\\r\\n\\t\\t\\topacity: .1;\\r\\n\\t\\t}\\r\\n\\t\\t50% {\\r\\n\\t\\t\\topacity: .5;\\r\\n\\t\\t\\tleft: -6px;\\r\\n\\t\\t}\\r\\n\\t\\t80% {\\r\\n\\t\\t\\topacity: .3;\\r\\n\\t\\t}\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: .6;\\r\\n\\t\\t\\tleft: 2px;\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@-webkit-keyframes noise-2 {\\r\\n\\t\\t0%,\\r\\n\\t\\t20%,\\r\\n\\t\\t40%,\\r\\n\\t\\t60%,\\r\\n\\t\\t70%,\\r\\n\\t\\t90% {\\r\\n\\t\\t\\topacity: 0;\\r\\n\\t\\t}\\r\\n\\t\\t10% {\\r\\n\\t\\t\\topacity: .1;\\r\\n\\t\\t}\\r\\n\\t\\t50% {\\r\\n\\t\\t\\topacity: .5;\\r\\n\\t\\t\\tleft: 6px;\\r\\n\\t\\t}\\r\\n\\t\\t80% {\\r\\n\\t\\t\\topacity: .3;\\r\\n\\t\\t}\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: .6;\\r\\n\\t\\t\\tleft: -2px;\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@keyframes noise-2 {\\r\\n\\t\\t0%,\\r\\n\\t\\t20%,\\r\\n\\t\\t40%,\\r\\n\\t\\t60%,\\r\\n\\t\\t70%,\\r\\n\\t\\t90% {\\r\\n\\t\\t\\topacity: 0;\\r\\n\\t\\t}\\r\\n\\t\\t10% {\\r\\n\\t\\t\\topacity: .1;\\r\\n\\t\\t}\\r\\n\\t\\t50% {\\r\\n\\t\\t\\topacity: .5;\\r\\n\\t\\t\\tleft: 6px;\\r\\n\\t\\t}\\r\\n\\t\\t80% {\\r\\n\\t\\t\\topacity: .3;\\r\\n\\t\\t}\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: .6;\\r\\n\\t\\t\\tleft: -2px;\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@-webkit-keyframes noise {\\r\\n\\t\\t0%,\\r\\n\\t\\t3%,\\r\\n\\t\\t5%,\\r\\n\\t\\t42%,\\r\\n\\t\\t44%,\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1);\\r\\n\\t\\t\\ttransform: scaleY(1);\\r\\n\\t\\t}\\r\\n\\t\\t4.3% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1.7);\\r\\n\\t\\t\\ttransform: scaleY(1.7);\\r\\n\\t\\t}\\r\\n\\t\\t43% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleX(1.5);\\r\\n\\t\\t\\ttransform: scaleX(1.5);\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@keyframes noise {\\r\\n\\t\\t0%,\\r\\n\\t\\t3%,\\r\\n\\t\\t5%,\\r\\n\\t\\t42%,\\r\\n\\t\\t44%,\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1);\\r\\n\\t\\t\\ttransform: scaleY(1);\\r\\n\\t\\t}\\r\\n\\t\\t4.3% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1.7);\\r\\n\\t\\t\\ttransform: scaleY(1.7);\\r\\n\\t\\t}\\r\\n\\t\\t43% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleX(1.5);\\r\\n\\t\\t\\ttransform: scaleX(1.5);\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@-webkit-keyframes noise-3 {\\r\\n\\t\\t0%,\\r\\n\\t\\t3%,\\r\\n\\t\\t5%,\\r\\n\\t\\t42%,\\r\\n\\t\\t44%,\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1);\\r\\n\\t\\t\\ttransform: scaleY(1);\\r\\n\\t\\t}\\r\\n\\t\\t4.3% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(4);\\r\\n\\t\\t\\ttransform: scaleY(4);\\r\\n\\t\\t}\\r\\n\\t\\t43% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleX(10) rotate(60deg);\\r\\n\\t\\t\\ttransform: scaleX(10) rotate(60deg);\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t@keyframes noise-3 {\\r\\n\\t\\t0%,\\r\\n\\t\\t3%,\\r\\n\\t\\t5%,\\r\\n\\t\\t42%,\\r\\n\\t\\t44%,\\r\\n\\t\\t100% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(1);\\r\\n\\t\\t\\ttransform: scaleY(1);\\r\\n\\t\\t}\\r\\n\\t\\t4.3% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleY(4);\\r\\n\\t\\t\\ttransform: scaleY(4);\\r\\n\\t\\t}\\r\\n\\t\\t43% {\\r\\n\\t\\t\\topacity: 1;\\r\\n\\t\\t\\t-webkit-transform: scaleX(10) rotate(60deg);\\r\\n\\t\\t\\ttransform: scaleX(10) rotate(60deg);\\r\\n\\t\\t}\\r\\n\\t}\\r\\n\\t\\r\\n\\t.wrap {\\r\\n\\t\\ttop: 30%;\\r\\n\\t\\tleft: 25%;\\r\\n\\t\\theight: 200px;\\r\\n\\t\\tmargin-top: -100px;\\r\\n\\t\\tposition: absolute;\\r\\n\\t}\\r\\n\\t\\r\\n\\tcode {\\r\\n\\t\\tcolor: white;\\r\\n\\t}\\r\\n\\t\\r\\n\\tspan.blue {\\r\\n\\t\\tcolor: #48beef;\\r\\n\\t}\\r\\n\\t\\r\\n\\tspan.comment {\\r\\n\\t\\tcolor: #7f8c8d;\\r\\n\\t}\\r\\n\\t\\r\\n\\tspan.orange {\\r\\n\\t\\tcolor: #f39c12;\\r\\n\\t}\\r\\n\\t\\r\\n\\tspan.green {\\r\\n\\t\\tcolor: #33cc33;\\r\\n\\t}\\r\\n\\t\\r\\n\\t.viewFull {\\r\\n\\t\\tfont-family: OCR-A;\\r\\n\\t\\tcolor: orange;\\r\\n\\t\\ttext-decoration: ;\\r\\n\\t}\\r\\n\\t\\r\\n\\t@font-face {\\r\\n\\t\\tfont-family: OCR-A;\\r\\n\\t\\tsrc: url(\'data:font\\/woff;base64,d09GRgABAAAAAHBsAA8AAAAAt8QAAQBQAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAABWAAAABwAAAAcKIgPf0dERUYAAAF0AAAAHgAAACABKAAET1MvMgAAAZQAAABMAAAAVm6xgqtjbWFwAAAB4AAAAfUAAAJiFS8wSGN2dCAAAAPYAAAAXwAAAaANACd4ZnBnbQAABDgAAAL4AAAFGNSI+xRnbHlmAAAHMAAAXPoAAJiE86qh7GhlYWQAAGQsAAAAMwAAADYGj4TnaGhlYQAAZGAAAAAgAAAAJA3WBmdobXR4AABkgAAAATIAAAH+fzRrz2xvY2EAAGW0AAAB+AAAAfjGPet6bWF4cAAAZ6wAAAAgAAAAIAL7AlZuYW1lAABnzAAABNEAAAsGXuXsS3Bvc3QAAGygAAABvQAAAm07tFf7cHJlcAAAbmAAAAIMAAACpT51oqgAAAABAAAAAMmJbzEAAAAAq8rFpAAAAACzM9qpeJxjYGRgYOADYgkGEGBiYATCX0DMAuYxAAAPCAEtAAB4nGNgZLnKOIGBlYGDdRarMQMDwyYIzfiYoZZJiIOfiZGVk4mRiZGZGSgHwnDg6+\\/nz+DAoPCBmdXw3wzGb2x3GbMUgBpBcgD1TQxDeJxjYGBgZoBgGQZGIMnAGAPkMYL5LIwOQNqHwYGBlYGHoY5hG8N\\/RkNGJ8ZgxkTGCsY6xklMx5lOMt1lPqAgoiClIKegpKCmYKBgpbBGUUlRTYlJiU2JX0lISVRJSklOSVvJQylBKVX11Afm\\/\\/+BpiowLGDYATTNkTGIMQFoWi3QtGNA024ATRNWkFCQUVAAm2aJYpog0DRJuGkpQNMY\\/v\\/\\/\\/\\/j\\/of8H\\/0\\/43\\/i\\/6H\\/a\\/4D\\/Pv8t\\/7H9Y\\/37\\/a\\/og8MPDjzY\\/2Dfg90PNj1Y9cDy\\/pH7B+89u\\/f43u17t+7duHf13qV7x+6tujf53oTbOgLfISFAPcDIxgA3kpEJSDChKwAGPQsrGzsHJxc3Dy8fv4CgkLCIqJi4hKSUtIysnLyCopKyiqqauoamlraOrp6+gaGRsYmpmbmFpZW1ja2dvYOjk7OLq5u7h6eXt4+vn39AYFBwSGhYeERkVHRMbFx8QiJDa1tH16TpcxctXLx0ybIVq1auXrNu7foNm7Zs3rp92+5de\\/beK0pJzXxQsaAg+1lZ1uf2mR+Kv3xNL3\\/x+tXVjznVDMt3NiTnvT\\/96WVuzcOkxpZpR45eu377zo2bO74dOMHw9PETBob7DLfuPmruburp7Ouf0DtlKsPk2XNmHTxzufD8hYuVVy6dBQAFSsIqAAAAeJxjYAABVkMQySLAgAaYjzKoguj\\/t1HFGTPQVZIPOKB4QMAqIF6NxF+OQ90KKth1muEMw1mGczAuYwjDOgZJRgZGJoZ9DKsZLRnCgDAPCKeAIKM5MzfDH4ZPAOQMEZwAeJyFVEtv00AQXid9Ji11kj7Sui1rlpSCE8KbqKogqrOuqgjUllSyKw52mkhpTj1z6q3SJj+CnzBGHCJO+Qn8CMQJJITUc5m1nfQhBJLt\\/Wa+mdnZbycpl16+eP7s6ZPHj4oPC3njwf31e2u5u+yOTm+vrixrS4vZhfm52Uw6pc7cmp5KJiYnxsdGR+IxheQVyJq2vzhuaLquO4XIXrpuQzyn\\/taBpK8FaTeSlm\\/YKzfs1aH9FsgsWMysyMI+sb4DyYAyC0TuomTe4E5REm+0GT+GRbPhuphRYSoF61cxaiWo7ScTJjObiUKe+IkkwiQijD3xFeuVEoCYxTf8GJmYLuQhbUAsx+XbhnLHRcAqWAmZzCXTu+h3r1IE0wYoEyIFxkwYD\\/alx1D2gHSon++Lbk8lddeYarCG9x6V87BHn8RzvFWTOnL5ui0KI1g8+GjoobxFBZNy8JaLX1bBrL\\/60T1p2md6X4M0rhxSBmxjxPaHb1pc8OwxlaYQZxQ+7tlXWV1+HcfJYsOCMyyIxXh7C4+SLRby4ZkiARpuW+7Z9mSfvE1Fpxn02g16CEJ5Cy\\/G+1+UELzBeMNrbIXVTSjXgoXUDu3ggChdxYlcUQAyIwHjVhw9FLu6b5uyMeZVtPDahx438qCDD0gqO9jBAkCPKJB9m2FoSX6aJSKOSsHw6I6CWbuXWTCaUxkV5wQUl\\/38cd3jRZ6xnHpOJLSY5QphMWoJV3i9i9M6oyoTfrUqTriLu+7amNW7+NLRwOo6oLotZQO1lxNg7duvNT3lDMzdgUlwpHCwksFxUAV8dqIFVSY1W6co1IHtaKiTLXENcbjKQcLBLeEdR7JJjZqloTxmBHVdTmenVyZ1NOB0zw5tSuraJ1IuGngfrmT6A2buQDKnA2aY7jLc5TNRCCFzMLE2fGbU+QxvbYAy\\/w+6GfKQMe24FnNCFNPiEiUM\\/KVvwoKBeN0QeAlfGagGjNp9bdOhagr\\/AeTtvWPVvUObcjGcgtATnvQPXgdheXictL0JfBzVmS96TlX1vtTS+75Ud6ulltRLSWq3JVvttrwgHDCOYxzHMgYMCEPAHscQwzCFJmEUh2RCKGMIkzC8hGE8Hi7JDVg2ueNgJ4aMcRyG8DK5uRkuWUySyYwSJtfjy4C7\\/L5zqrsl2YY77\\/d7z0vV6eqj7nO+8y3\\/bzlHiMEIIQsyI8TCzXvYzHCI\\/C+efuM0vZRLKTElZuGCode795vQe+SOoEH+MPgsYpg\\/mMrIjeKotx5wsBzHHw9KqOE9HsGqxWJtRCLJhCjVimf+UZgVmmfQaLNSHC2XMOvGcjrXxeaGhnHFzyoVfwx7zQWczi3BSzHzh262lMoWmW5GkdfLysG1slKGRhlXuJ82d2dKlTTz+XRl8L13KulxuVyWV8sKQhidYn7MRUwK4tFwPW5qOG2aE7EWlV1pXcXMiHbVvdK2ij8k7FiFVx3Bq\\/7ryvU7duwSzs6enRV0uKBRZbZ4plzypHysWU53sbJxO8WuXc+vWule\\/d0rxOVXuNablKv0kdWr8aS+H64vIwa9in7CxYAOy9EqdEd9RFnBa8PIZuPqXKUx1pC0nlSgGMCBWI6LsfLKhtqtNEZGqv0vVnF15orkCqyGhYbF4vQ5D60Wzo0I\\/yicGWmOjAj6RKcNYxsR\\/ueEMDLSPHOmCE3SGhkBSvq8MEigGlwGB4Cag53GwBAlq9JpVPw+QmNfp9H6UdanDDKpsiyXNxBirl9PrhvKcrpyoJKWlQ3pylB8\\/fqUUsxuUMjDA1h\\/VVYU+VXS5VhZzpRelcuV9KvGFV5Xvl1JpxSjD775298ma3MaMVwKaKSgLfVaQCuVvEWt68U4jtcdrmo8Ltdt9qrMejVTI+nGbrdN8qmFBj6aM6vOkhpuiEeTTufgAOGlkbOwYM2Rs2fIihUnzsyOFpRiYWKkKdaKs0AREaaVWkgWJTWYgoY4ny4pSgXoCQ0uocgXUCWdqejpSllm1jW\\/QWhwvphPlzFD5tF8lgEmZB7X30hXTpP3XiX02lPK4LRcPk1+6DSh1ScrRJpOIQR8WAY+TKAi2livhARRnAqGvMFgSAhFIrbjiA2KHk\\/yeLd0l7AnyAiheKP3eHamLAb9DdNx56GScG72bKVC+LJSmUCjs7WiohjXaXd\\/Ydp93wmY6QKxGRiyYTOdbhUP0SW++H2cOCgrlfSz6VKFtTQ3+tKpqp854MvKygV0kMzpGblYMpUV+YukmyYr+k\\/PPxEqp5MxdluoP5NuiqWMJpfhkikhMs\\/XYJ5p7gnUi6poFL1d\\/1CPs390fNE9o1wWM0wmunRJLLZkyRST8cKryOiiReGq09HXF160aMrR53U4+qqRaDaXG3KOLsVoyIXQVG7IC69NQZur6jly4Tf1xdBwDfWFHYvMSyqaJxNLLNFMKMewhUbCY5KYJX0uxyJrnxoIxDIoZ0o38jPLhhapgvVQHSSpCTJ0JqAUZ4FtamKtBvyinA1UhFkgY7EpBmqFAqUmd98Jt3DixMiJabiNnBiZ5vqDhen7TljcIyMWN4jatAkeuO8ToMMJ40m5hCayXZY2bUdxFyUx0WZAch4HqsYaQIuyWZodHMhBZ3MMB1if1z+Mq13sF7dNOeVnb30k9aW\\/SOyfPDi5v3DyyIaYnC6GN24MAn\\/9D7Ik5UceqaT3bp0JTd4p7N++uzv6qYPp3lsn9++f3FnMPvOdVSHgu8CHboyk0yV27TrCq+uf1L+6Ti6vfwnkDr\\/KrGTTlBdDdbvNhlhupUV1rgT9t6M4SyXIYCSf18JS9gEhYbqelivsAe\\/SbLkI\\/JB22JvfHh3IMUp+CJHPPH2hye4CPRtBW+tDoXB4SpS8oiiB+p9yOGFdnVJIDHNe1sE5Wc4mwV0Kh8wBp+gOmLFqdrtjUSLIoG5HYFVgHdBoQIF\\/ZDEMKrfITaU54DEoCNYCBlf1tAlrZnZ3\\/T0\\/IGe9r6xZ84pPlgf5v+9ia\\/rRZZULSEzk+8OY2XIHvmX79ZgJ93fHJV2BsX8Kxt7dHjvHslNOh9fpdIRDoSlJ9EqSyDlYJ2djQ1JY5LxSGAYfksjYeXMAqQGe\\/38z9oEipuqnQuYgtikM3DLEfKr7pHswLftOj4+f9mXSA+6T3fgcXjE6oHuiXeWwrk\\/coX9px2ZdD5fyMPQK0aG\\/Rhq3CORNQV99AVkvPPOc6KmiIxeeqW+GRng9XNLkYkImqy1sS3NKL5mVArNSbIIsT\\/XYvD09NqXXJnCORrnMyT0hiQs0slw2HpLgobvhPWqJqKkeuVfh+hv5oziVautckCOqdAMKItJDxIpe2+qIztvQS2iiAH86irYlCRYXNtax2DE8lpa+6mtTiVlEDMyJaCbTH9L\\/gJ2hkpyKniB6+XgkJZdDmNf\\/V7iSTkfYde+kK5X0+QMBJZXx\\/\\/pXflmuBA6cJ8\\/eedqvyBnfr34dyKaVIKK4AL3Npbgx0MaVepg7mvAeDdkSGh9yHhWjRwOWhqjiQCCVpBNtnhHgLyoS5QENuop0AmLbYIheQ05ShmlhUpVMumRYiQtIAaug62QczNuYSZdKaV0v02eZSiWDGWIyqQzdjRh2F7MVSShWd9s0nuckrDqPWixeDwVMVDTb5mze18Eo2F3kEy8gYm4xGGvGrq8h6htvJdQjHw9zfhXrXIz5A8i9t25zaiY8I1oPCcKvhN8h+rF0KoZdIJ\\/JxcpycxP5FObp5ADzxDiZDmCrIhnrSfQTNsZIVIc40TGb6VjJiZ1OUSAjnZhFxealxohJHCRWEfRXhZHK8heISfmiXKZj++WFC1zRVIHPW1JPftSKbfAdU1ab12q1IZuDxVZWsiH3jIitZhjyOWIHYVqgvEcXmL+Jiw0eF25uEORUzcscFDPJGvfke3vCfXJC5v4sUpBTFLMCLzBciGJED0rVJUnjWI\\/mkgTVNuPzqOiQFywvmF0wuaNApqpokMcnGuTKAiA+tbXBOJddd93y5rmxLcwBHT3Fzja2bm2c9yy\\/7ror2DfPp8gcT8L3BEDvxlCpHmY0GysIDqQFJafqPepQsbkROYpxIk4oePaM0CTwszkC4Bi+FBB3G7aISpv7jO+HhziGGZ0u99MHCB+01u0g+yZ59d4evIXQXb9KBkOeLlF6fw\\/GQvBpHJXrkYBmMmGr5maDGpbMqscTnUkGVUcDH0oI587MCudHBGP2EoVTMBg6kDn2M8bSEgf2ZJO7cQXWmwcypVKG2bhy27aVOsOsBz5kmLGV2\\/CXyKhuXKHfsupGfRtpt2iTgPEUUKPOs34tGrWntTwraHaJPXLhZ3WvLVFlzSmQS9Qwz4RC2YbrUC8MDoz2meYZqVbojK9GBcQynz4w1oFcp10Zmi85XAyGtLL5LOXztUAl2sIw2uYB+mzjyhu4vfpLd4xdtVk\\/suJGMl4MzkUpQ\\/utoHMx5OtdkN+3gY8SqFqPhRqM5rHbY2aeN7MxadSP\\/U6kSv6wXQ0zxFYooE6KgONApltaBigrpjqqpd0wVIwh8sxZ3UwEGq8lX0sGAJa+vAFvYbspIp3C\\/0ruupfqlyehAxnXz4G2YLRRGg3Uo6zmcvmdwOLxONL8oGCs0kzGpbJs5JAMclUETh+ZNZyyueVOzWm6Oc5vG7IE9jMMfkDnmLWUzTbhdWPbYHnvHRhWlf5UKHWn9yD+MvMvZKj6TdvG9G0rbgwN+oblMl90IuorGbwYAAsM3BjUrFbRomE2pIlSWHXNxDjO2gip3kNR4VzzbKXYlkVjeC0nRp43MopYPDBqnF1vrGu6AtTZAgrnLnzeVCYjaf5IoRJRSXPTcvn8p\\/Rz2E7tAtEFRRRG6+r9Xo9nymb12mzWe63Ya\\/XYOBcXDPqBeC7Jr6KjgtWrmo\\/aPDZVEKIRkXKfcLYJ\\/2GAhISiFKhNEPXEUevvMXjRa+locNBTYoo0T8Eo303ocJOVOB7SOakhlzltNQz0fHdEf0ffQ7QlPlAKYzOurS4TrH2SjtWgWwLV65mo5uJCIR8QkI1pPkI7V1wVZ1Ih9Rsc5rgY6LKk0JwlClrQm\\/OEemSkrdg6ym2OmBg0DoxSPLltJfg24A+ObYNFZFbdiNc9qx\\/E+jP48RU34t8TQuvi9avI8k5yCf0m\\/BfNo\\/o2\\/GWqY19D8P0gG2Ssg\\/U4WWMU1gQW1lkqweBCDc6i+mZiEdVJlpmMr3KGiMec1mkpQLG1vm29w8LQ2JPrCes1v0Gt7FPQ3q0zHBECpgBUe548fm+PXGbvw1b9HRKnOA32K8FtoP5Yss6j47bQcYA7vuOuaLRt9kcQkdPLGDLPxYYtdSBTUVLgK5XlA2SdSIvbMBT\\/YrpSysCl+Xdtc0fdJPj+naArPs59Fb4\\/TeIk8P22kJZIeCVRDRyNgr\\/XiEYzMjUFs5SdwPIrlxvLZcAAPkldN7hU8MsEKV1A6RLRaSmF+2pZfoi8+RAManI+RlBaGAE9BLjoa8iP7qqv+hyLHXN42Od0uaa8Pq\\/X67uW2+i9hZv07ubu8Zq9Lh8rAURmnSbJ5NztxC4v5+Bs\\/EzQxzpZdCggnAMHSx8JgNcaAqAYLIJUFFuCcRFOxBOeOdttsbXgYRfOvaow+yORpJJtPsLcllWSoVhzu8LsWVaJ42mpkin69Yd1LVjMVCQ8HSU2HT\\/PRdjvIRfwW7Iu8JrPhzUL7wiBNpEIh4EfOELVHFEh8ywZMNTCV99rbqRcRa1Buw0cxiYAdVaoon2EQDzi6xs0fAtoONSm4QM+oIfPN+V0AQ1drNPhmOJYL8exq7lV3o3ctd5J7havmZCPEtIpmSXnPU7M+oB0M0EHB8TkbYSGI4SI\\/zkaApKztHGQrQ2ksZ8bKje3x8JJJcdsb+7PARHjzP4y861lSkzfI5WyJT++E98ehIak74kZ2Ogt0DGKqYSS4NG\\/UN\\/NOfyOjGMScZFodMrl9rpc7oCb56f8Aa\\/fH3AS0Ga4e+6621Od9mPkdEVBSfl5XkyIo+LVoio+I5rFQMQtciHWgU2mlNYluSMB1utCUdapJmb6Mw3swGpPw+rnraz3UB\\/hH0C\\/NQDfejNgOFiwcArxsYoTikK1bGG6n\\/jnHTrMc7oQoMK2fwHMNdg1HyOC9QJxwoYNoQvPRfUIsRBh\\/LP1+rJ4MjHgB9yWSnifiRkwAf6zU\\/qWScIKD\\/YF8FeWrtGnwqVctwcf9oj7\\/d0P5ZpBheIwYIk5+1ZAQ2hTPRRK2sRqnwYuC2+zmTSeDXUPaIGI5D1y4e162CZU4wJcQjm1tzeOVO\\/MorLqbMQPVSkU0w2MUwC8AwqqZhgZeFIg4IGCHHcnwNVS4emuthM\\/jCvwxK9UhrIAJ1tNdvd4UddrA9VyV\\/Frd+LEnc\\/u23br8sbkbdojtz61iTgn+jfWbCHcrbNS\\/5U9t5SS9b\\/5+ifP\\/3bLvkZj\\/2a8fHL\\/Jp1OFn9hi\\/4qxRxkzqhl08Po6\\/XxsNvlCoZ4QQiagqBFrCYA9iYcFIQpPwbOwbwWCnnY4P1ubzVYd7ngYrNXg9gUZFnW4RdcVs4jWQXJU\\/UTIsWhsdGP7VaTP2z1+TmTyypgP1hm65Go\\/3AEPJpfLRFrxaBUq8G\\/iQlCrYAyQtgFvFBKr8vIjcUkjIxY4D\\/li0DHNU1ZWiTFbOr\\/xuu99aUCvporJRrSmD41zvaGsyX8tw\\/hyG7uvtHB93a6nHJ5yZmzQqyS4dlVegKhdvwvRuPiQdD42+oBV1x70Yd9ZJ4+n8mrmdmEZpLAdX\\/7Oau9am\\/dXUcu\\/FPdBtN1CWHVDu\\/+a90peavI20ioyEQRW\\/NMsUBYoVDoYODCBEXBHcAGDG7xk2gTfTkkdlpc7KUHm4DNH3xp165nALIdxBsodHsGrx8Dyz3+4PdM5Zc\\/t+qRW2\\/Dt61YuU2\\/9cZmcyXcJ28keu51mFMW5pRFn66PpbPZKbvLa7e7YsnkFC95eV6KSd6o5mOzdqRZJXsQprrXhcclLAVtjmrNNc4zUsye5VLBBpdOplQ3WVu71VblXDzrPpwjcl9oVkD2qYDDOpKoZ210VqzVLhNbybYxKs0igHh7lmKl88zCmn0kbsR8GRcpRn555P7Qd7qyJ6J3VTG1g\\/qxfOhb\\/uA3Q9SHWlPEd1\\/zkS0r8K6BqwnzF\\/WnuhcN9+kHB1s+oxHPjaJu9EA9m4fZm1mv2czKMHuHFVSglc3HAhpiHUl3vOHV3MSP+c1zDlfVQaKYCWjk2aSDi8vZjGo12y1hC2OJqz6LL9wQD\\/UQ1wZMPlFvQAEalwSlB44OiPzIZaePU3FM42IWkklJtUL+LbfRJCqVqmdocAAoI+MUXrPs3tjTxcpfJz85rr9IJr+RxPlxanhR9OlE6JnIElMZr1usf+NjN26+Xn++\\/iFCAfYNghMOnk9V8fiy3moZb+hgBkR9tzD4FzG7xhCeZhgP1rysQ\\/NIZnOQxw6V9xyKEHYdKY7Uih1OpdZ3oXex0JV8FiNiZS8g4kACiDcz78LycdP6LTQ2T\\/1HEqA2YigdvRNEffWgpO2wY7vdbwNnwi+53SabRzWhwyFjGGQUdBATFPxeOgZqEF5l3k0BcINxUFClbyBfuJ57gyrEd8iL5n0tP4vwBMln9KLr6oM2p3MqX\\/Dm8wVn3hrQYjG5YLMCO8hWm7MqarJkK7jDYbdP5Rpg8px5t5ojlg74W9DPkLwFDU7PFkabRIkVYZydRSckm8\\/WRI+DMM9lNua8ICIDzAwOyOXBhH6uK\\/avcVDTG9KVZ4lrjjfsm9QP3vrI8nci15tgbtlyVn8tU9cBRVXSMxTZfHVy375J\\/Np9G\\/BwptKeI+H7BFped4Y03pqwMlZrzMVR45XoqQK4iQlwj0n2APE9LI0wdTvONc\\/Af4BchRH416H6xd5uG8wajVPE\\/DCIDEVJZUo6Q9Z5PbeXWF5dxL+He+a9PYQ\\/8B90N9yRkVcyeCCB1tedljAQ3R4AV5JzkTH2wNhcR21qMOYlw\\/QeRcH7g8eCbD24NvhQ8Elo\\/kPw7aAlGDS8gImdZwsjQhMuJBgD\\/zrEL4Bf1FE4g\\/MCIjjFHCAso+sEFDY3UtPIkFdl6noq51NGkAb8kTcJE5N4WgqMxPdobChX93FcxMNG4jbQFVpE4tWAVQ1QX2SWcgQMi1r7FsX8YNaHBucISDwDTL+ikjkoK6V0euDxbbDMOrZTDj5LnAH2Tbk8lD6f3b5vf4dmKVMFcN5d9THebrVOibxXFPmwCD5wMOwNBsNh6xD4gWKctzsaHOsJBuLSvfaDQSbo4e0kTizWrZ6qKFrDqhcdSlEZqxQLJCAMbtwIoDXQaCFCUhqxC9Icy3TH+nZY4VLwb\\/DCaerOhaOyXIo0n2BuCJUz6UjzN2XKEdMZEnnYhU8FBtJyoPlu8998qdRQAP9Av5MI55yO4lGk7uZYkETJOSNagTUF4m3+jiikckm5KLD7KoH4DM2gcG9QJ\\/JUK17Vtucx9Hp9DXLwLsEBQuACoQ\\/4vQGnI+B3QBszXux0YSaAI05nJGJ2+BsMAcYM43KwgPRd\\/oCDcWL1ocCTASYQYPwqf+TCsee68lVyr9sy2WqJv59n6CshGKreyf8l\\/3We5WcSjGojXX3+Kr0LIr0fcrmrtkNx4RygAQCLE4WdhQJRJgAIxBrwMywELEWxcIYmLIpEqVj7CyZiSaywFCZDu0wARm5LohLBSrttMTAyk3qWUOEnr\\/7oR9QiELDMdhXeOW+n4ZQ39Ndx33vUI2L\\/8HaRhJ\\/n9LIH\\/Wnd6WQZZsrlBO\\/BCTJ5Vz0FAKfuWuu638W6iPFw3W+1VtnPwtOr2evYLwAGJFOEN+B+fz0Kb4pOcDdZl4thLJKTYV0qOuJjVfthI1TbpLpzRFSK4DYLI4UCyT6Ah2CiLgKJD8odt1lpBfO52C\\/0ETKtN\\/FLBLtzP30vy1nPU2TAvfHeu+w3wH83fKK2fvGiPPrjF1Dswr8+B5gsRrSLBwZptTqc1S4vPOoS4KXnyIV36iJ5pHnYGAChtMB7qkHSOwKNz7DTbiatLmIxK9AZ9gTVqhsTLHQIXrsPd8NiTszSy0QL4hXoH5iHp5OjayEemEkb6Mwz+Gl2yQtdXS8Mf375bZFvZbPfGv3s5048Gww+584mxedDoW+ImQw3o09\\/aNPGq\\/QH7105iHdfMTExjvfc+6r+xKBSreiPp1L4Zvj8Mr6hy6BBW54EsPpK3Slqdrs7DGbWLZkJSHWD7vKp5iPRsENFgMhB88PKFFoGt2NvxTk766FxWQJMX2XW3\\/IIWKYNpGzgID5Ilf0GMLn7J\\/WXJvfpRKEyu0CnDtPcgbEeCSqLi9CJ+m1lu8NudRTzXk8RnG1Pj9fj6VkEqCzGeGMxxg7uhq2Yz1UqudyU30Zf9fT3s4FYsmLjsouYqsqy\\/kzA0ciqnoCUZBb5KzbzYEwOOzzFnkJBHlT5hvnIYlkNH65RbqPWWlFqRL6obzpi4LRZmgukgjYvFXjCyCh3gFsnj0xyylbhhBssjA23dDmPA+3KjC5L24uzYTeG95QKyTC3IrBLMbwjp\\/F6fPPqNevvvW9nYSdh3F8t2bXmQxvvHmJy+uPFRF7gD1adlfSwp3DF4pyPPY4Hh+9Z2dQf3n79dsL5T1w3umcD\\/tFGFSP9VPf4mD4+4HpYi\\/kS5dLO2pUK\\/n0PasXyEJekeC8DSCfiEcUpp93rdNojmt3jCWaADZwiuHqS6ciF39UDgHc8dp\\/odCVVV8N0JJdp+IAnshQR6GdmRXDgayR+D\\/KqGNFI6p0Zuqja8l+AAh17IM9xDDj0wDGvMesIx9yGNwoDqUpY372HSPCBNmKjrLP11kea3wyH0+U+5kBTJS78mrJ8A9Xlpzu47fa6k+8GvpB4ryTxiAD1MjjrgSBcpFw3z6UtFifgiWg0rQWknOSxAZdwnpl+vrvHDS5ZTyNMQNyZ5hmR4DiDEYxgOnHUO9UFRpymZWTS\\/QSug8ziOTRBV52gOhBgGsDBn8eH1n5BSRSSsYp+fdzF3Nn38eX68KYHNqZL2Sj+asTVfLh3h6k8vTVT6drucQrnf7m4ceUf7V23pZyKZHx2gY3Vll+1ax4+IjHuUj1sbgSDdi8sGS\\/ZVPaoU5VgdY5KkhHPJmU1RjAbbDkphBqcK6Npa1DZeHKaXUvAzUGCaA4SduLW\\/PItYgSWvEerYzhluIWTT3dsZ67uj0iksIOTWkhnJkFscnwu4zZSmIcNDKA7uDABdpoY6UoabzQw1SO36Acm950HgAMY7HwiVS6l2TdJpvW8PLl\\/H\\/n+11rrHUD76\\/L38N+bGZHBzJQgegVB5M0W85TAQ5O38CLmOJ+DqGonYEUHOybgtwQsHLnwD\\/W8y1MVzJyfy3MsJzGimYefEFUbDmEGHwnxqtniB4\\/ucFA4R6LtE7OtiASxwRNKYWInQcGB2uXjdyI2gE9rnra2jUrpB8OLhkP6MzQLBVOhdP6pfur8l0ZH2VvwIJmn\\/rvmJ5jPYm+mnfdt68c+tLLe482KJLwUIJdCIWBNsmxWEwOSU+W+jb1Y7fp2Rt0RxdFD\\/TDwJvgjsBQ01jRKQHuhINL8WkCZWxMeE8jZRS4kJdR+3unCnCqvVAhQ8F0VLD9xpS+wYl89utqryKlyefUghcHdzdfWrW+tGOdJTxXGxop7UkGWuHry+dja9Uxfi3d+CnMpAU6V0d\\/WJz8RwZLkicQi4Zhos1nhIZYdokOWp2Ii6HoxEonCj8g4JEnwvt3i8YQwnrJ4vBaLxyF6QpyvwbMWLMe4ZIOVYjI8CTZqcRyfyVqwQ+ScjSEzNh\\/KACVIrYNOrmgULqPzqhws1hHriMUqwLWziK2XoM6tJ\\/AEUegXR2Ntl8Bc5tP6vwdKGYXHNf20VEmVA5jH9lC\\/PODSX8I1Z1kuB8HLqeYHEsEQ42GERDhZzuuL8MvdpXTA3\\/xN8y1vIFPqNtb8tdaaR9CW+pAngtCU5AGd5hFtAO2Nqhab6IlwLmSVJC7gAh72qJGjCDj4qIRUcOmiRj6rlX6gq68ohGONOKtRDjWvGKFT3OHptJjT1EcnaFE5jxGpAGTua\\/6YpuS68TPfzIBDv0v\\/b3j5blkpdn1jvm7woGX1jIshyQcaOB8CCXWB5nWymkNiXU4AfjNgU0hunkTyRapnZ2vgLU\\/MwbyUr+MoD7Zi\\/DjB6M1HGR91zThmsvnbdKlkKt\\/+3i+JI3EnF6SRhHbM1shX9dWDgbDGsygC6M3DzcSQGgxGIqrDyFT9T2HWyCUY7uG8MFQnTUVesqIsMnfjdUZ6ntYUPk3zpc8e2Ej1JclEEs+bOIhGyQDJJVy4wJVpXcTSenqVdYPtMcxaMSynkZfEVjNrQ263DTkc2MqyRVKDo6Bi8ZLCiOxcHsAoPnuqucG3KJUVmAP+RalMiJvOxeVC5L17MnG5r41z5\\/Ky\\/fVQEGsWNuTXBCmghkLhsOqciXENDyECGHJKh3k+8kW1Aa2XHlERmV36N8jKM08ReTcqTPDagxu5vWQJyjJJ1JE6BUID9k2gwZvon7giJyAfqtbjo27VzbhZ1iHWXEM8Y8JuN+9SrUcCvOo97DfgP1W1CridRfB1FJA\\/z\\/yIbldbGn+P14QrlTC+UiynSXGR\\/r\\/C\\/eky3sbcWy43H4iHE+UM1vSPZ0qJCFmLC+cuIO7NCwcRi8QZrAFvmpgi+BhG8UxqMMW9+V6A++cL6CD489Psu+wW6m8uqjs+gjZZGWvdKM06VvdDw+J08FWXa4cJm0x5Wwo8Qjv4Y6iAwD0G7x7g\\/c72cLOthWOLxf6MR9JfpTfuy8GB1KD18\\/SKOjHJGPsSSqEiuvEFJIA7aCVuB\\/3SotVWRShi1dx8WiuFcKhf84ciUjxod1bZdIgLqZ4XOVWaKTsaBTVOqk2bvyK1ps1p98h9JwooOKqMzIIf2QnboQIIV6BrfmGFP+AxlIGlVUaEuyi2MQOA8w9jfBJfEVEYYPnjNGQzOqmxPqIh\\/lw\\/s33fI4zLhEuyfohIwiyzxXk3zamVH97efJDWIO\\/bfsOtj+gvrMqUyvKWdMsXIzFoGegso2G09fkU261hUi4SBX8LZzXM8b2aPwkv\\/CXN6\\/WHpcNO7CRxWPAtyb3OW+3VIeDkJajgV2OHRoinAgoFXKwJWmA9C3MlReAFEh7AYpXkUBI4TgC4mQcgTiyfr53MJ1oPy2kwiDDhBCaBmVFM4ArYRXzv+MorH\\/9IY22qkA9fEZ3MPHTrHxtSD5Z88hM9\\/kdjqelYcm\\/MtzwSWRIFL3r38OLFK2K3D+jP9N8cXBEp7E5RGLUXrnrMMxUe+HZP9I8i0og3i2\\/o8SuBjt5PAQ9k0eJ66MeB\\/x79VYD1ak4+oWFJaOyQsfx0GIfJzG1OVzWsyubDOeFsZbYwUZw406pwmuimlnwJzsHo5yLonWm1M6On9C9dt\\/xrV3GbtVXX4D0Uaz+aTkynwvui\\/XgjLHHtz5fd+dPbrlYXk4LwyK5o8XQq8CchGkEha3eyFUtMo8Xow89zbJVw6s\\/qfbBcKKOhBD+g8WTt+KLm4CUpGIRFCwZhwfCgW7XOjPSowUPDhsSPjBDXmHDoguWaH1Rcgg2E0k9cpSGYGaxOwEJXC3p0VgvYFubO7iqliXbKlO697QsbNqwMFkrJFePb9tx4zbd2Z+ND0VQ1HNsbyT6QkvdG2LX482Ty791Nrtk948OB24t4Q+22yHB95bZIMdSlf1mOFgOJO2KFmSWx3aFODAHWKYcG0EQ9YtLydViPfB2kNJ\\/3lLQUT0Lnn+nGmKDPJMzcB0olj8MkCRYmROhWeXxkKN8wgyM6aDhTI4RtJwqdqolCoVPW55u3cERgF9IB3jP5UoNk5kAjoAgXIyHvbDmfwd9IV5SUvqL3+sCVclHpWrli\\/b9+9OrRseZmrMfKufjrXclyitl7Cpa7EO9OEBWe9Tai23rwVGpHorpofGTJ7X+6uxgqJCLdoXadFGLPwrp3obEXkBemlwEXMap5WWTVkgKPSeGjM5MJJhtYFRtB56gFWyx5keZ6wbUCPUki9xMEk5K8ZweBzNvmMIxbbjPRPcoQnTVzFdE1G+RyX9fG\\/\\/rpl4jovTRy9QHiO7CxZpWikpMEoyybOljJpCura8twxcgrwHrpj9F6giJahT5eDyzT3IQz3SOa6BbF1WUt14iPaWnCvj3wPJ2O8xaN1DBacSQgSF5wIWFl3Y3lDVntPnIFx1XVwOHVJNxfFM43z5wh3LtlYgvwLrCucGZW+EdRobpnvimluuRSJk7grjko7sbs+7A086a+j5ra21OxWuyuHVPb1o4m+voSBlsvZui+jrHwdXt3ZtMDccrg8cwDCXlvTH+slJFLobI\\/g9HdKxaFt5Xgk7eHgbvLKaLIz6+7bc1wANBLrBAq6L9Jx8v++M5Il9of2x1pY7oI+JsZNP4CcoEt6gGzFCeMzLERzRHXrHUrDZvxnrTkV4UXAyo\\/k7MkwBHM0siRsRVnxCDO7GiBGJ+O\\/0FVMlgevxEG68eUg+fFjKc3r568ZR\\/rg8WUQRrgtiWZ+72RxtlLYuH6xo2LtVuJlVFSzN3pSujLzd9SdwMxOIUY9qTpXrDjBVSrJ0idTlhL8jmSUOg+lnUeN5myajDW8B4LBvt626WbYDeEMxQMXVq0M0rs4kXpOQMnGaEeJkEd2U2ZMmbKeI+O6DYMGsoAX6ZM6otM99L9TQPJcbn87jTNkewiy6fIzRrVrgxO6I+xp013w7iLgBwz4PO5qmTwaS3PB6JaQIzA0JOqqdGXDanZvpIXe4l4zRaLwHzGvic69sLFFUdLCYMZiCTOGJmHhViPTWxKKTB+sjWr+M2bH6tkcolIpveAUZ5M0k9yeZ\\/pbiU1TngHLv+R0tbHK+nVubLMfraUIVm+85lb9s3lFkmsKYU+VV8jilI4HIoIQiQybdQGi5IoSNOhsBcAadW6G1BWJMyKDo4VbGAyQoFAQhJsbrcUYr1H5Fp4T5gRw9aIig6nCWNVKhOkpIYgZiP6BP86JXtWEpzj2lE6cPI+OEnRio3TPUD4RAwsR1h\\/En8zXJLliF7faLAbwfeZ5jo8BW5G1q\\/\\/SP+UL50aDOAHxnXDv8Uy6MaT1G\\/rrvvtmsD6SYVc6JhPtboa3LG2X0aYrMVg5RKY6\\/flJna3zhGqH2gxULpCarCJk2MUB1fS7502Kpcw+hbQukh13JfrNkdQ8FTv7MYuEnq3GtmF56w8qUw49hy8ZSagsksIV+3maHcop1mdDWuD4z3JRh+J47woqP7vxFT5O+ZGj4paSQ1yn0mkqiX4MtL0+qv3G01S64CKBSNfceZc8xyV+DMjo2eINFUqtVqBSn1hArckP9D638acNM7NzxVWzr34VlleJq7ccNOytVuv2sUFYbYV7GUSorSNGiuJ30Ns\\/Xo2JpfrjQ0b7tzQXEeLvLQhfn3zcaIgmC8XXDc3DxqF9K1cBtBJRtccwkSN+Qgt8qDUsCYQVSakNafg4SMmSUQAFtVgQg2+GDiSFbBqPZwxVBoRM8NrHWnhFjo\\/bBHno0myVWKBeiP2mdn8llF6lq586UPXX7XtYS4ASi1VwSH2o+tyLPNI89eGUSMaYdFE+c8nm59KV\\/q6mF0b5\\/w6GD8PFrh+yETABMXCTmjIgBX9mknC5IEEc7MS1BVV8ZFuEQafNwJznTyEYX2JZjDqi4b8JvFi5NtGFD\\/a+dKuH57+5Es7f9TclBr15b5bkJVEYjief7aWqCWY\\/fpXdh777h34xju+yxy7B0BvZEhK9AULeH0lWAxFymEj76Dvh7H\\/DiXRIrTpcL\\/maEgVjSd2N0iQIi8B7WODGko6BHB3Yh4uEiRBaMklVBd34S41OLOYH8Cq5VCtjfRJVUnbsrSsL8muLlyFFkiai3ZVlYsnaGBjvIcEOwh8uOfuro9e+bnNN+4dm8zeMPZMISF\\/KpX9fDBajvbdl8ru6duqf45oA+5ZslZDn2nsWXuv\\/u4ff+SupWM3J3aF+zCfT+zx9f9xqvvHXdDbmPtjdN1SqIaunSlqQbFH87YRs9crgmNn6tIwASYYm6RExEGY00HW15tHanRmeFB1HF5MlvB8CzEvmPgI3VtimYcwWih5cD6OxsocpjBmbmANdnf9wxM7bxy7Su4bX\\/LhW+\\/d\\/iftLLf+6O5e\\/6MRWU1k90Z9owm5GtV\\/hx8cHKs1YreU8JY99cE1d9O+790LJME\\/k6aCQ8fTiZ0JTzWS1r+STpQD8+Uug65+7mCcaqZ6weqqft2DPQREiN6qlaQPrRHN6nGAMiJoYoeABYASAs04kLKPCRLNOlupzBJ4XJwgUjdRXShtxNoZ1YK5eWCCqva9H7nm3sl9xsw+Lhd+T9w3ojpgyW\\/dvXrfZB9JRBflcvZVvWBgiFb8FbB+L\\/rMCyh+4b\\/XvUSu8nDhu3K5aSPYTCq\\/6glerOa6NnVt77qni4M3BS5psTg0xMdiSS0g0f2PUZhxTxA0chcP7\\/dkVK7hOdLvIrmHw33CObJz+cys1CoY6iQeJuYnHgqFTu4hZ6QeTHE8r5KhYoTZB7qy\\/QRM42\\/hqU2fVRL5Rkb\\/9KIsfnHs2i9drz+z8dFyV\\/KKFbiWrSzSlyxbeR\\/70m1Kpn91MSW+HH\\/gmtv3b6+klNW1bC10KnZHbdOnO7SIcL9GPWhVvRsHYS5izKz5RCefzabA0tlCoYya9zXiqruRtyFuLcdwXGEO+wNgLhrzovsnQAGlLsb+C7bytsojWsYwdmByP93QDPY4pWCq5\\/VnFPmp5b2j2eIX9k0yKbKv4wfELv6A7hJ4M135Yjm75tF2vVuKyt+aut1XB31vakR44qAdgrUE3U8YMkT0fwr0v1dieNU2IyfAGIIJOJQm+3epr9p2VAujZwj39V3spxpbSMlOeGre\\/AqzBUcMY52uPLxh41W37DMFSSlMGaRu28HnGQ8Nk\\/+SzKZxXfWhSWYPLVnSNs3F\\/4mvGUQz4Elihpk2NmcKVotlWhSgKVgFCXOi38mLZA7AaVXRwoisSRJ41Sp51AsY\\/oh0Iw68t038hPig+Lx4QTSJSZe7St54HgRPtMJPH4YOyIpp+7lYvGqllQyhSHWr9X7AZ8WJQkGZKISKJCOvBIsTYAwJSUht5uj7ZUR8htLtrC72DLaS9vob0f7bhyP6m0ZGRK\\/A8rE7n2xuH17CPPxVstRTzWNMQ23L4gtAi36gRQ+u1FexDqdzmmG9DMNiSRSnGcAFDM5IHs+0nPHKciYdDoWm5TQ00yGP6OT84XQGsw5zXrbEeVXCkpRnGjKZaDheJff6IEAjWXC4qjKfl\\/2Sk3WE0mFOtb\\/oUFmRuY7F7HewhFWPfF0GZ76TTqmRF9NqmHzC8o1Veq+OGveCYtwzeeNOXDL6TVHjDuCJ3t2icQe+CxcL7T+E1c5N1GqzNeFXcCNZPJKeluiG5xHSj\\/jREwB7geSXUJ27pNFahwAtqqz4LY5WNKXFqw7cYtZA1VgZ5pUdiyWmnAKsJW3yDG8qRjZvThQ2xormjR48ICfL2FPdTcwks\\/bksWP4t3T7xr\\/rP15SfOLL5XQADzAcOV1A9\\/7die+2147kfvpg7WT09\\/UH2rmdsJHbsXo8YYynrR7wCjwkKRSNhKPtpJBol+XpqOiNRkXR7glzPp63kqxPqsFKt0Q\\/Gd0b3RflNlonrXdb\\/8z6F1bTnvDe8Avhk2HuLvuf2WfsL9s5OQo\\/FmzsiOP4kawV20luyEygXCcvNAF\\/jNzQyFxuiKSBTlyUG6KuBE0NjVwuMeS5NDH0gD4byWdKAu7Tf+spZnvCOuaCmeSAQ38Ld4tKqiuGt+svyJVM0oNvxVf6o6nBhP4qVmJ9ctqlH9S\\/5InmlHRLFwCGSIAOS6Mf1NeFnQ5HJCjw\\/HQoCB5UMEIC146IF54+gh4R\\/gaxAjzjQgkvz4dDmtMpY4cGShlbuUhURQFVsEw69jgY7EBBXghFwjTS7KOR5ifDOPyR0E2h3SE2HLKEhJY2oDrSQbSB8KTwDYEVqD4gUXoS+C7QIiWyL3+iKFA\\/lARFasJLAql1PyFdfsPDtEk4ccKGDRhKjICH7AzvpKOUlrLwcQl9OFD7Z6IUnt\\/VXQudJ7j\\/jeEzxBtiXj2PqiTMgR3DzDhVHPfpCOtTRJO268eAbjG0pb74JHtCYjwsx00bSTWJ5TxamOc0p9QjjUtMhh1kV7Cszcxiq4dFDetMgpMCDZ7ktc8owDAAP2BiEzRQR4qVOikrZOzdmIvT0R0L83YrJmLNZ5gIzSFtzlT6mNW6ldgFRiVqbywfbb5oYJPVmV6mXMmkFKWdD2zvr\\/xY3XbEjd0Jq6eaOXLh54AnPFWy4ZIgp2jUjbQMa9bcUt5+FJlJyXLdZYtXWbMaOpoSRSPEMXJmtlYQRs6OzJXxAtyY6Gy8XJABy3WWpZW5b73LnBobw3TD8gW04saBNN1fcyA9sI0+JYFu\\/YXJ+tLt+hHi6NzcwA2ZLFB5KIOXL7vJCCJjBoAhcxzm5UL8jJ1lJZdqIVWSowDh5yN4H3NcyaTLROFsoNV8e+ViUV5Al48CXbI4S+jiInQpELpENT+hi9+ftWgutqBlpdBRMcUmyW7UlIrs+aNmhFpEOTsiULKcuZgqWXGgmrsoTOKfF6GcnyFjssuXt+IkaGybIhtUyVRuaj09iFffsmzJJB43iKIflQdJmdSgrP9d\\/eZWbeI5fI67itHJ3vZDd4U+HWJALM\\/WAeVVI1bNU7c7qh6PI6ix8XjK3UpgKcQBUMol8K+pRu\\/KypbOwQTEBTAprT37XPaxqDho9VxAQTUQTFe6H\\/WEh9xObOY\\/FQjIFWbrZrd5w1XuoXRvaMLJbVzrHpJ7yblA+n7mD6C7eRRHgy8gx4Xz9XCmUPVGIhHuuIMXGui4FVBSMNCwWi9zPhBxNOf2vFHagUdMXrcMUoB5W5HXZsv0TCCGnBGUqTDdbDGTLjFsV7oylFxtOBipQSXJ\\/Hm2qBj51df0N7kU9xYaRJsPyXG3UO0\\/cuEX9Qw0KhW5H+iFeI8WkRL9KNPHRVSZk\\/M7AjjwNK3QM5IobjWQPzwEzgVNooD6Ks4GaqhoMEGNbDmZX7B9cRZl7sydXCfn0rKhPua0IQoXpVVknZQ31fI09VIeIkzNvnRJiuUU4fPX+XQrB5N3\\/IiyfGfvtwL+yJ\\/UF8eyuRwYSms0l5uySmA9pajkzdq0WEMUcVjzsjkrwHIp6fBUzVJQYqSoNcdx\\/kaqh2hyF+j6HleD6+HWWrGVgPQi2Y5ibNgw4lNkKZsjkqHkjPL19kabbGpOYbfywzBvQyosbLu6mm2lm75LNt+VMtvv\\/SUVD+\\/OkR9+vCwXlvtu+gVMNaX\\/yx78OWgwNKP2BKEK\\/uTJ9emBVaQAIV3RbzAOWiB5p09x6zgnqqKr0V0ACC\\/87DneU1UIunVDI5CES5RcYIr0rSx5qwQNG3nqJpdxLZrtUZDNHbB2OZ2jWhdfUztPwoODY2qYbNgEOpAqJqNBj8BoVW4G2ruLqLR1ZI3q+05VH5W8zqkxxC3tGrw44GBEWtifbQrC9EJb65t3XTt2fRDWP7Spr28T4YTgdY2P7frY8i0hoEtwU9\\/BRC02PF5LLI7HFydq4yT2gj\\/+MfoTW4t9W+hPfKx+485tSzfRn9hSLG6hn71pyU07b\\/zk6vFwORIpR1dDoxIOV6KodZ4Gx2VNA6BH\\/6peknsKham07E2n5RQ5RSWdgmYq3JOSuUTaKmgoGCoE2XTamW0kWLCZxPWtAHAOC3CBn\\/A2ovARXCOZvz\\/\\/szyL8lvzx\\/JsPp+2FVQ+KoTeDjGhkKF1J4gbWKsUJ3aSUrsiudC4BjRILQS8KbZqVghm4Oag7Psc3ZEavKRCBzMnAaWRLUPvljtHeWBG\\/7ZUkcs+zGOnv5zvs+rfZIxdck9R9txMZLSLkZORRCWjD+ETXUOyh2++jph5NiuC0nUpASiLYVk7GwyKUsRHLRg9mIhUL09QxTfPkInvY9bad\\/ZmMlS8hwj7e3cbbXLmSCuHRereFqOVaG890VOXV475PzSwpt5YeSfeGlfxrvj9\\/i8MOOtJ0VNdSXh+EBrDWmyZhlwxlHFJmpnt7i5qGUG125NoGbt0ODa2NFRRk8nVvWpX11jVq7KHV9FdLOSIJYVMRKoZmA5eEEND0gpkpYqzpGyCBHS75u22vagYstCSk8vXRCqg+Elbpv0f2zd459XziiRvizmZTUO3LU88Cpe5SsnHws7m06Rn3\\/6wU3\\/9izEne9XTjdE1O2jl5O0S725uHh4b33FweOzKOz\\/7YVo+6XW5mb9cvvRDO5o\\/hiZe63G7ab0F+y49N4SsY77uE4Os3b4DPDyWFHRYVJcrovrIjGdbZR0w7fZq0jNWYD6mBa+4ENHiRE01N7WDZ9xV5P4FEir8Am2Ri7Gn8CBg0gKtMZHRFehIPdAjZaOL8CLzoG3QbX6BecE0Y59xcWlS7wCCFSMGyw0NDwntB8nFceTCb+sO8Bw5P1yyrywnOp1szltefKVah3t1Obz1soNNvMytAgV57HmbUFV6iImM+4LVnpNL6vBgibLEIrz0JGgBtCLy0kOWJy2MpThBln+CWEAS6SgWFFj80dZL8sQI58P678TzA8hABVYcoEk8WuUcx\\/BsaTup2VJ4LeCUoydQjOKluPWMK3xv6t+mvkcvGyZGJv75prEPtW5HDvDeGyyOLR7h6Vx\\/f+7x4m3BG0KRrb6by1+F17i4\\/aWXtjOvk2uzzJxau7VveON4c\\/Dq6+HOPJedscfCnufSvYlkYUn\\/t1b0X1F+rjBaSCYKYDwuoFP4dlInJVrQGKjC25HtOYx4Fo0ODrZr5b\\/FcYB9fMAHf1YfT5Njlwrenp5CPBab8he8fn9hbmu91WKZQj4vQj4L8kouE\\/N9p9Wn2mP+nrxsCn4\\/nrazBTUZyZpO8zzdDlnCgDeUZqBCUBzcLtkaeGkDuHDu\\/KzO8VkW9vJP2fPWwdSQ\\/\\/PWVrHRfbp60QNclqRMsYgrXsEr4kqxmJEk\\/d\\/0P1zuKXDJcXSWA7yGJOSr21jWovEWVXIUW\\/phTuHNA8wcOdSp0vxruVSSmf8iK3g1rSBCxikssAats40EMxKA5twC2RhGy9Hv6\\/l\\/XvYfS99bxip14NwKufwnxCW4QFyUSuVUocdbKPRQwQm1BacnM\\/KKopQyi4jILFr+SoZWeUE7k1nUFp9Cz3Bf3+jJPHkrafNW8yMDJ4frNnd1OD8M0vPQ+0sP4N\\/CLNUeC0SIAk2J5sXa5fJtgSJa9T8hVIYNtATaq94q5B70ELUKIOMDxKn5OI5P7nt+OtAV60p9O1tIJbu1B28d7n\\/wAyWJXb1\\/8uXCf0tkYl192D6b7OlJ4qnJ0jCprTNNc2+ay7S2jtHw+9TWmaYPgjydY2q0foTkr5Yd4shOGwfZdGIjdRZeXqymySsfaWjkbBEuoDlYUeX5qGo19DFBZBOzLSh2kV9heT+IdfZHO3ft+uE\\/7N616\\/UDJG21mWAoksjaSRJZ7PYdx797x\\/6dx7696wvT5VAxGK6EeqaLkUFPoi9o6IHjwJMBeuZYHxqqR6NHrSioOY8KfVqXzB1FVgGpwR7v0WCw2E9zvBWB7AY9WwEiAISu1c58AGxp4QG8YEci821QnMYZZIAO9Ga6XE5jFlBC0Xje\\/DB5wrx98Wlkxill52faNghh\\/CAzw94HNE+iYj3IveLxRE2vuFz+V6KsLeQ9aYOZnQzxRQXoSmoIKIHb5\\/PMO2AUBmbQeXCebLP3XTvwup7K51P6kfGNAz\\/Uz5E29gxcyw0OXLu1kEgUxr85cO31PdC4dgDG8iaM5WW677xSD7vIWOyvgEsCYxEES0r2sc6TCIUs5Ew68MSk2sXD6RQszh2MOjcculfex7587YA+S8dh\\/6M0IM8\\/mhtVKMQ2BjaSYZGx9bTGtTMbjeYoFl5Y+7nJ8hGesZDaT75d+2kltZ8MQ\\/cmE6hgMrkuqf1sKV9TKzzI\\/FryZPqL+qv0ZupuKWByNfD3DwHbpdnvgUSU6tFPJ\\/\\/S8nULmwnycY11SGLSrKKZbgJHDuWFc80KOVwHHFRldhRc\\/ons\\/LRDZ99fzkCbwPztnaZcupV\\/uLm3vyxni6ySgcuNqpGbWN\\/KPgj6P2VKtQj3QEqpRZraplZ2ol1fYAe6hEg1segUWasWZFletdm8ZIARb9zhqnqLBcCPZK1Q0VB2c6EceX6lFw0lcXYS8D9ILhsm9z1yC2OcVMspxp4TfGDfJB6e3G+sy936YyzZAeMidseucU6HihCwCfkqWqUznzUUcqSe\\/oxhZg7ovzMqs8m5tfgN4L8X6P4nuS56ra9gbHtFYIMuLug9yVmLCjmbxOC5ZqVTv9WpzqCHcLTyUXfqR1Ld3alNm8j1qXFyY2YIZ+H1hKvG9RvJC4IxTuM7iM8OGGMdxRjSNzEiSfvnSfIesEZh0DiX4CeANdwtrPFA\\/QqX2z3lQ16fD5G6f4y8GCM5k5kq+MGK+RPxON2uGvAj1WVzfN\\/tw6zV5PGrciL1\\/UwhEIp7zN25qHDa3IEaAXIcwaxxa\\/7noAY7d95l57jL6uWfspvvWwgtdPWiB8wo5kUPwAr9B17RI+o\\/oLACuy\\/3lNANgJTJZioLXege\\/SMICTk0ixBQ8V28uDmOLN9kShIANYRafbl\\/uUzfT7T7LlrQt\\/W5u\\/UPX+5zF7f6wpoc0fdzZlgTB+pFR+v9hfRQdkX2xsA9zoOMWZSkqUzWm8lkXdlcrrXZQQ4Egy3\\/OUuUxkq3pxqQMxLHR525PMoL+VK+nt+R\\/1reLIqubJ7jI+lgNGrWEJ8VXWwhIGVYWfXZGsF0UGUbCWcuwRYK\\/WBPwSWmB+kQxXjROTqicZDOB56jM\\/+YHJq6KOLWmTqtYDt4y75AS3mx9tZhOdjWPG+cnxPAb9GTdfTkevwiOVmHxq0qRHzP\\/4U+7pUeCXR\\/IefF\\/9etrbN19BuWrMFqqJjrIRiP5DHNXwFaZyitl19ogiRYn2OSphwqDNJ1ubjPXe0+uffv8\\/0L56GPfYZBqQI2ZQuDrT495ic6fV6\\/8E\\/447RPd63dh9Tbh81\\/SfqcJ322g264HQXqLoZFpbzfmmXYOP3WQWyIZ+e73+h87mvI1PkZuecyPwP88yPEcHnQN93AP5+vr+oxWyy9fC\\/4CW7e63bzbksa86HeXhEXyDmOYlhLp3FIE1lZA80k8bLF3ev0xYtduCvckNXervsJJnLSnKGt6gTXjEZRzhQnRkaM7AsaHRFa566J7YuRmZkrIqCn5WSNnYDtQ1vxXN2tqCxsYEVkfwgQ49m14RG5YsFPB5a\\/Q+PZB1vljYqih+8E9HH6zUYpI0rcnlGKSm4iHrFOK7XwKVyktu51sHVZem74Y\\/VrHg3gCKgx1uxlWbM\\/FJqyOrxWq8Pv9GkulumKs6TyvaL1AihRu1h6Bo0Zjzuwg55BYx63Mg4\\/G+8yhcWGHAmFVUv7EBrZbGUthweEsxcfQkMgJEXiAbrH93JHsUyk5A88hKYT+52\\/N4Y5tfh9j6SpGYfWbJwzSe9\\/Pg2lW8tedXguQfk935Kbd6hM4IvkZn6fu2gfT92BUT7pDWaxIT+4MI+P5\\/f\\/\\/oX\\/3e6fKpD+\\/Qv7n6L9\\/7LTfzu1YoTvwYnLww8w+BJZOd2R095546bf0ZOOcVmcXfgdF\\/e\\/qz3Pnrl5Xtxn3rhzJfKZ8qXjjtFx914y7sE++AGm9RPz5LV9RkoWPM+\\/rS+xO+yivdtejDgiYrQ7WrTmC4XWAUmlvr4pl93rctmHlZiWzfrspQqLNAsbzUtsTvNJ9taZQRJxRrPQqFbtUr5kGu4r9Kq8i54a5Fayw2qMi6U4NxckO1gC4GsDY86dHVSktUCEXSkcfj+m7Zwf8wEHCQ1jz9zBAvPEHN9N62ouc7AQPTFv44HOyULV9fRAcm6aHmR68SFD+Aka\\/joyd8aQfit1QVo810V1cpkxdPJPQCfTtUtXHCiLA+\\/HD5WF\\/MMgpeBms0z\\/B\\/e\\/a67\\/UD\\/pr3xw\\/w4vMahYI\\/17L+2fpeOvtGzKj1s2pboKundsisFvlYv4jUVLusmHsr0L+Q1jAefZf+dcSEZ\\/UvdVffsR4yERXIlcRHKRSR7D2H1G73SLVAYaj+LH7Psjj8l\\/bf0b8a89B6JWK+luIxc7uURbPxBp3cOk0Id8tC0EKpC6KtRbad87WY1OQqOTy2gf+c38xy\\/CSloOnfl5SM4Ug7\\/4RUCR5cDPfxEiWwyYL\\/8imEkPhH\\/5yyD4GsEzPw+mMkrgzC9ClVQmRGOeb4It7DcpYAsH0Ci+u36vRRBFc6lUHBwdHBotZ6ORSK5YLA1Vhwar\\/cLgUDQW4y2lYjaTsY4MD0+RCLLfR9ITCAOEw+XR0SnR7IWP6K9WpyI5L\\/y4lY\\/FpjJWwGTWYf+IjyubR0Uu1x+pcqiAe7gQZ+UzMS5hrx0xDv6otQ\\/+CJGDPxQSjlDIUR4cOfhjOTyyKxrH1rREIyRNo8+In\\/FN57jLfbInz1vhk9OupaqHfIDLc2RZuk\\/Nw+fPwMt0\\/nBdOFcpzBYFnVwlcrhEoGb8JbUnYJJGjJUQF7y3oGTi\\/4jX6e8yaP2Sg5HWHmbjyIpO8wRuJV0X\\/EYDS+s3uCxYe0wtHjTltAd6tXQHtvxd+OP1RbvjtUgxU\\/Js3yynvhu\\/tbryrlAtVEoOibet68GfOOwdxn1VKZF6U\\/+x4s3EPUe87Gp812B9xcotfUo8mnz+hVWbr12mq0uWXtm4oacsR6OHDqyf2Ixv1fcVCuNbGbQjqz+vo+35HN7e1Xv1RTK76GKbkk9i\\/yX6\\/+L+8+xiQSb98x\\/cf559yfST\\/uX3sy+LLrEv5QT8AINTF9vF1zq2d8QYE9rV\\/o7ulD280FbPO6slQCqH625Oi8c0jydu5VmrRI8U5G2ualBFR+SYh1MdZG8ECTwZ57XQMGMncOJbmItKGfGy9mmCifapLQTQ44O3PHKAnNxCTnBhBif3vdXZvijrv9k\\/+fDD6Yr+cmXenu\\/l6P76VtOgqeYf9Ne6lywJLFmyCq9Zsg6tx7cFb0vvCd6T\\/jN8f+RzSx7Bj7gfC+6LPzy4b\\/HX8FPuvw3+Vfxrg19bHB9OelnOnNOqrMslaOai3b5MDYcryWGvyrIretXKoTGYVjsvZQA5IycFwkIdHFoH26pYgSnSopUBWg1NEw2DC6qeW7Qg+Qe6xYymKALsY9cMPL518KOPVnLJK1ZEsv3Jl9YWbvrqcKpT6w66beDuXR97aeePPnTztoaM37nizqWtctlFeSX2\\/Hit75rB9eCdL2rFEc9vIpvw2Gdv2nMNc2zXpg8PKGnenOjwD\\/ihOZJ7EMxo+Tvz+Gre87su91w0o+\\/\\/b0Qt0FA9IWVbGOmUca5jp8\\/rPzb6LL1qYZ+YSen0Ab5FzsNgpMZKtFPHlpl2dvq81u5TbfUBfb4G+uwF33gRWoHWoQP1O8elLRIj9qYKi32D4VJh3LcivLz3Y7714Q2FW303hT\\/B3ufbHf40t3dkb+PTKx\\/iHh95vPHQyie5vxn5m8aTK6PjGorgCNL4FXjFiojUy3lHtSIvaz28WXNfo3njvdIKDqmrcrlVdjawWB0g7rXT7qoODKy\\/Sl0bwAEj9F4gDFE0CjlmCdanHALv0WPrJnbiS1NRFrkTRA4Mzq96CrQ2D9CqciOB39ojzA62fv8LKQdiZsh2S3rK2K\\/7tgbGT1+xfd8jt94dArR\\/O4H8t9919a1bgx+9ur68LK+LlnPxfLKceiZY3kNKqMJ4+7ztl\\/U9dK90ewf1ddeNjyzZcypdOdjeh5n\\/U\\/3du\\/uMbdSkBuYxumdYQWvr2R8H\\/sf8PcNdbdepC5vfZ\\/\\/wQGf\\/MNUV1DkqtnYSp+T\\/zE7ii\\/ygV2\\/74G3F8vo5N+j9txjPuUAdvUx5ejXlRSInhKdHqxGUne8bzOtz1y6jT2Px+\\/dpy8\\/wirk+c7Kxep5ssAwab9BOIBvYiRj2Nfo5x61Ubh+g46T7DVvPDbndZYz\\/i\\/P7k+\\/9GvleAL69gUj7e7fRvYqK8bPke79E5Q0MSbcQofLGdH7nVj9aif65rtkYm8lsN6fDTNjkt\\/vTD9YeWPrACofo9LvGXKsCY0HTV1JPdO\\/v\\/S+pr3c\\/1Wvu7nG5eUHIp1NJsPiynBeEKZ\\/s9flkVyAYCjl7Cz2pZCLuBDc83ueNx\\/s4J9kelZfZOC8kBEYQhrQ+p5OLs8s0TpLzzlLcJ+S5vlBJ7TuyOjSsWmn1gCLoFN6QI0dIMR3cayRjQ8NTrVwXgSHwp4VT5k5ecY9Q2EKfnqBuzU40sSDH08Xji9ApKP0WkgEEg98\\/BcSlWnuY1n+lZ0UYeCy4c2eQ4I5P\\/FGInG+zm8GPjx3oqPnOJieskJ1NL9zx3a8UVoZJfc3OXaTexrcb7zJ+XL9lbPK5g619Tj0HW1ufOj4PXfdrGKqLf2Kse6VhC8zxJNPiyXULeLvSY0Pv16fN24N979+nzdv9ixb2yc7r07YNQytt8\\/ifafH\\/uoW2Yckg7UT0Ps0Bc2PIgULgJxfrIbOGjoY0T1hLHvXwR9mYzKr+ozZbV8741TeAR86g1nFUsKjz4pALU17z32G2E\\/2XqpRBi3SyXWT7KX3IjdFfjUMqdp4xEl2AR55tPyO+xukLV3Ix7h6UAWlZhL5TH2IsjhAftqVjYZPdDULj9qTuCk\\/l9w59JblP+SvlBZ\\/Dzfdnw6GkzVMNTSmWVCVtTptJ8WUEnpineD5k5tMhzs9lK\\/0KN8CRrVl+okxJKjmn+fkBEAveHEpzSOrPKhWuF7X34UlkH14RQNriXlUyTp47PwtI3wD6tJC48wpPC83CtHAM\\/jQL5HqMyEFbDOYAurGLfGGRmS\\/b3j8eyIoDXcZuL7p5L5vfSMB5RRnt+ofdL1XiGyh2X3eL\\/uxThNu9+Lc+\\/fjzbX63P9pTykSu\\/cjodUV8447jjod7KwDZP7f+k3sJi+vPZCJL8PpUovkGMDrwzOvGmVzAMxsNHPOuofcufn7X7ss8J7z6H4Zc5CtB63w9nKB8uFGfx4cYDaVpJ4JRaD08\\/ZzN8+SCxKSkNP2c9p5Dmvskew4dbL\\/2\\/\\/meQ3JcxcI9hz7x\\/789h6Z\\/o1Z1+v+06ZDQkNJH6dCnQ8OqLKapLGNjjzz7PWPPsqAFeMRrVok7hlTB23AcE4TL7FnOLjg7d+HWZeYU\\/SUaF+98P9iW0vZmdxILDsEaL6Lnv+ys1z4S1sJMWrbZHY5UJCyJQjSacjimhKhXEKJCKuqAv1Y\\/S04jhzcEjsumwrIqCbaiESlTikWlbXEA\\/c2Ld4NZ4UZG3NTa0Ab9ZX+dZHxHouYOJWjVDuKHjONjm+Y6LbGcITnU3xNrEdIfpweN4646+cV+wRnmJFE9zTffJr++D95F8\\/eDF9GD9VJ\\/URAlqa8rFwoGAvm+fCAwJfV54UlfQMrDXz6spVlK\\/DyJ3gRoNRc04vk+7lgxFyqqQkCKs2RJyiW6JIF2jUHFiBFW6OHqF01duGTul6zeHAHmb0HvaBXmFP3NX62t6B8nlAie6exIv4OS462DxqqmK7h4O61CPYO\\/Y+x60\\/fdQWny1tz5qwTDZFAZ3VMf7e7paZ3dV+zvb\\/2ukkx7P4M1ktM8LCqTJc+oDqcgctlsIMm6OU7pXXjQaoWesSoqrV+t0axQejQrrfOOLgmZziNCp2IqdXH6gwWCdKL+bOogrSM92DmH1Ti0sJUEOcBlvMaxrAcIm887h3VeKuQG9uddnVNZjTpAw4f6HupDdXQFerR+Q9VbC40xH2E4G2\\/7f2q79ugorvN+79zZ3dmHdmYf2qf2pd3V7uqxo90RWklIaHhLGBudFGOMERhMAOM6QAivcDhrpaE0xWlJxiZGf9jUclrsqNQnMWC3B4zcUhs3QHxc16dJE9c9rs\\/pySFxOYRSQEvvvTOzL+TgnrT\\/zNzZleZ89+59fI\\/f9\\/saDX42bGjxN3r93paWlsPMeG68+3DfYXl8\\/vjiCWYyN9k90TchT86fXPyG+5Tfj22nYTjMD4eRbaGSwAcSzWuZo7j48DACxSyX9c4ttupmU2vrfT21ZpMOf6+2mqY1k6nKYBJV7qQyQMnbLRWq7SSDQ7WTKkQm3ZA4Goi1xLy\\/PneZrJV\\/Xt877\\/6HBoJPvvvO1qC8cphhFi1c5F9CTaTp6yEx0NW3Lpx3Z\\/xdj8DXV2Va12ErKf04GduCvKTtncfhY0\\/8fXYxS0lqiH2Ug8vbQmJwQ3tpsgP\\/3xPgzs7WzQ+3f0PT+2PU59JNfS4XK36dAamVTcLC3X4g9VxZQffNi\\/+lniv0j6vPJ+r76b\\/b95OxBpIMjHx+TGSgXo5EwAGT2n\\/UxmmoHKM1cmQTXlVfw3vMbHy+PYfXUwtYKqd5r89nB5Gw3ecbC0Osw0MXFw\\/H8dXQRMqGebFZCPhAkFD5EpJbigkQRVFfHjq2H2schTIwk55W5SWB9wqyWrSdgpnNh\\/5s93nye57f2sRPEgDspZ96RobYn24lXD3o\\/NynXyCH29hc8svd+vmGQYebjROiHrz\\/\\/wrdRP+NZW8EC360FpDV\\/HfEUc3huxwmLu\\/7yCVqsxeAy2pF21zQ5SKYG0EwljE3BFun+rK3Qx1wkyTYBIOegbejdJXCC2qwjGw9NgHUyLNQto9ggfqJNP2q8\\/2k7oSX3VggBKwu1zYrtFrrBVKRwqpAySokUJkKjtkxE96BnawXCMIQ3hkuq\\/lRJxsUE0KUZk6s6NCa3oxUFZmW0DTkdGIyrZ4R7tNnlJOxWXYZI7gPG4VdAgNsNkExImRW30hy+NpqVXMsKfH4dqPxuHT1GeWqFM\\/4n3xq7Em\\/IdcWmj85OT\\/U5tu500fzuNBN5qwmp8WC1DQusYIG1SHSzFmSpNeJd0INGE3h0AycRNdQm2E7raeVlj2N71it4Xe2kUB3Ox5cx9s873ubq4Cxr0hlaFot9E93yKh7Dha97Yne3t7HN83u7duyNWdJurI2n9ne5gzZ2G\\/3rHu0d0nvuvU9AzlXImWX3GYXF2HVmMz60nNojY41gorJxrINRZvuccx1Gqpz5CTGombClSbjueNnqQ2SgE4y9tfZDPsArWUwUJXLFarP5aKw87A5UCjndKm9\\/L9K62IVLa0r3u5fXU7rwuM+G91A79LamRHwyBvArMaJOC1e5NfuPj3gRMNRQtThKcAoXZi4FXQhvDLtdmPYGPYQ0B6dT1WLgObNVOSm8hrqntEPn6FaBb2Wnql6YF96hupXd13p3CZn5\\/PoPJY+Jbt5H1BMvKvJU7QWBSHmL7LFiEgYqkntVVHCukA+1+mq0nB011VVyYpZ11dQ8pXjNMpJa7+uIFcUUtkt47npXvIV87Z6x3NlFjOEjjGf4bniOIXNYIR4SwX7WrtKaQ3W0qBaTFbXh0k\\/ADPEfvCF3sEWyTtufXDXO+B65n2KW3QCn2zFI2HiEcs6tVmrlZ6tIzclK7Jq7lIy3\\/20nFwcvqZmKkC4G793h\\/5eivdGpqLTaRV1KO6MkO\\/6krYoVVXQNvG\\/qZebvPfLV9S+mwH\\/zrzPzqIyJ7FV4wYmlxII8M3qgHht3iSeGWR5aWNDIwrlWpS6YkMRuXd\\/hA5RH8QrGjsKU3lKdNaOXulY7Vgy4Bgey880uTpkL\\/J4TFTrpwMaKPJJmLQmw2QVjV7Rxpfk4kiVIS6jELFsd3+EPiNS6BKVGCKh\\/lT7C9QUGKZjdozUP6W\\/RxK0y6pgzcjzuaKJ+rAlf2fRVvw2yQDNOQC09m4EtMkeDgiK70wTsmmoa4K5Vsu96I6mwWvq73kPuDUzhef+TADrPPo5XeoxutQ\\/oimJjJ5\\/qcuSA1+Su3IZv8LHIrFi7KMYGzuTRAQUDgRB2Cb8RGCFM7YOLKTAxTIsIDVrsKRSXkeHe1V0+LXRaayJXRu9h8h1speztCudoGzNWieqO6N\\/D29rCXCdYhzS8Ye9la6pVa3xuvTDJWhKzfWVnSxr5aP8CP8U\\/yp\\/jjfwPEd4J7Qi83U0yWjqy\\/NLE\\/PWbVgAf2\\/xRnh90UbcSi3ctHERtXP2AoZdA67Rs0bEsz\\/girc3sbbLyMlfWkbSTy\\/JaZjm0pFLPro2abQGENALnWI6fx2dTIZ7PLNr1EzifPNIXCod6owvT+B9NL4MPzxNv8EP+Bs674aIt2LJDC3AaligvCbzINgo91dBcSHgzCyyItncUEBottU9FURtidlTXU7ODAFrtM8Tp9LpwameHuO8yJTvpCxcv\\/JJv0CQxZ9Q+CZVwSVioYpVJB5VvBL6cVmn6dRrPvXfs+z0ysbmWMHDHG9M4j6z+eYTzZ1kqUnwJinhLXYmyCJkaZvWGmWPTbP+XHM8wJT8YrxlGxkAhR41e+O5zoRCKk4p5RbZvy+Cv0ZqXtQiOXnPDCgt4YmkO82c5\\/R5WU1Mff7SzOlKqt72IZbJrss0E3q6BimtIaN1XPTdgOjPgz8z9UDnmXHN6t61Es8hp6EAFoP\\/kLsEoPg2ybvlg\\/L35L+QDWbZLzNmuT\\/RpTRx\\/b7+nv6h\\/pX9Bhvo72dzSgaximxbQIoJtnUWeghQZQNuRAV8cZPLzvyB\\/LP57+fZZ1MQpVM9qaEUsqQCKYZ51o4\\/sEOLPWBnmJ3MAeZZ5vsMi3xCE1Z1jQET8AKG6eoHcp4xdizoWdAzp6ODC0Tdc4qB00PCtSv\\/0i\\/8I4WTioR+ai2J3awdXTs6ql1IRZ424BMD918hLidx7RrC5yr8Ys0oueNhCzNVbLUppJ8DZPDKSLQ5VX9jNOlN4mzR2vD8Y\\/72AvQlc\\/hUeH\\/NnvS3H0yLYtrDsh5yf\\/hp74J1wzBOv\\/63pbHfhy9kstlMyWk2wbfS2Wwann43625vTXfcvwwGs+l0NuYKJsgd+sXhRHsmnZU+JI+RqCdM7qp\\/ajXzY\\/Zf8f7uAd3g0TdA7s6kPKfRVwiTnLMEufxBTsm9HH45MZE7Ez6TOJnjsl25cCwRSHOCwUCTCUIXvN7khTZ3gxteaGtQ\\/ZOiqCUvTeepjyrXSQYRxmvpb7UR0c9PE4VjlT3LOhKrkZlzoDUcbj0QS6VicBxfmTHyvMrt9TSuwq1MXzD04ovhYB\\/7Yumr0XQ6Cv8kE45kMpFwRnues4DnF5Teou2LI569ez0jVViOHvC0LCtZRZrITkhvZA09AGiVYVszmbFwyB0Oh2xWOxcKB2PJGIfNNHuxpxt2E9xT9+t9wWKhHbbrwKr2072NSicyAXOUzSjNTqBibzthpTQS8TORyUYcl9nawjrUVatrYXRjqHahk0CHyxOC+qRCukuKcnyb0P6lRAG7Iezzh\\/S4nm+f4waJLS\\/dl7y86\\/za\\/VtS1JF73JOBXXP7sd7hVVEbpeflgdKPwzHVzSs9vB5u2Ta1esf60pGVj9Xy64vgwdObbXts37IhwrMpu0hmIQkOOVFEsfGtCuukgSCvTsho8+GDlEB2cskid7pTZWV0ku5r1IyEMaMuCzBVKQCAHPXsfVqAUydpXP4zmvIPP\\/z8cCYhavwZNbNKmQWbSxNlnsbWMnejViuI+Go5bJmsk3tNVotljDO5Oc5EyLfGVPIt6\\/IGV4GxWS2cAZqQATUo0IlMVsRYitzrvK3InLZrPVQjXbQORdvBu5mAMtChMv2bZplhIxu7\\/Spc7p\\/d64crjt8BWH85hDYNDt4++vE55o+nv0ZyeQBWyNi3qV6YkT3U\\/kOa\\/XdKtf9ORoVrn1whtXe\\/sAV4UbUAb1YsQJZWFPsW0ZXIXLh1kXLQSOq9zCkaoblR98kpfAyOWS14sVj8pNKDxW02WyxWARsfQfPJgNWt4GkCFA6I1D+Nl8GnJLVjholfjlDg4ZiltyENTIwzObWeJvxS6QStqRmffg96UtpchlfhlVeIAl3a1BUN9ao6NLYxvV\\/MTp2qTy\\/U7VQ3sLC\\/NKSwPvQN+csNEJqZnXCf+S\\/Bi9zfgJPcJfAW9wv4nrkE\\/9NsW8Ud4o6Ao3Cc+3PwMvwB9yb3puXNhnPgMvcT8BH3Mfg1d4e7Y77T4LbwDPMPJovbZLKYcJsoVIg2jHa7wyiqHn2s09doSHB0+1frtST2n26d9YbC2UZ2Hrmxx2\\/t84uhJi97wJ8NN1H94AJzCIWobhuQG9hzVp4\\/14lVT84hUDYKom7e7Vcitv8kLcfYnGc+JorRd4ndQt4nAsj+gPL9LpfbnQ6nyzFmMuKeGJ0uFy0fCBmTkYGs1e8PWZ2sSbHbWag0IlbNjiNZjVj7ULtYo31wqvYRV7N0ymZUBJaTNNgVUul5f3cuvKODpMRv6PGIsMvVv7VFFNmDt\\/52eM7VXCKRE9ejE72kocYaXgNjbJzZgfWjMEiBR+X5jwa3RrYFi5Gngn8aORx8IXIscCx4rOmvIieiJ1L2ZQZZWGsYEfYZtgvGgCOqCAaT3d6EUoopGLF4i42NmXjRojJFqPnNxFh1Eto6crxBdQNLVaBV3oKk+unaoMkr5ZOpeHMIumdDNr6n78j2bUd6d+\\/ufe4rW5\\/r2\\/MKt3TR0BKuVBxaajQugVJh3\\/79e\\/oO939t797dvYcPGx8YGXnAeJj6bAmvqp\\/6bLFtiBCnuJHd7lQsxiJwFi3AXLS4aFLflQqDRZVfMq4XLw1ByRFTKRyOl4zqKrqJ3qN0YzddcA1tGH6t6gjX4UH2AXQD7zuDYLVs2eXf1ftN\\/zd72ejrd679yO0tUDdfC27MTisScfNJUluPEq8lcJqTtev+PnX0VH9fFZ3TvR1\\/6HdzDGZ+67doVYUNanUDu3IZ3x1vLxVX88YV9\\/NVvsTmdpVD5rtskL0CJPAVub9NwraRubU54HcI8Xir2TwmxN2CELcmrRxwKH6\\/1BZQks28YI63sva0vRFf2LB4alb4ZJfwqfArkvZA1\\/2ghCdTXqQXUQ3j0bCmgaKcKci5bDOXx0BfLwXdaC6HMnVFgjkOIzSmObHC2R+XDHO\\/Rzb9DxaWPlWLka1s7I0WDPPGya\\/+ocoM88tCLsHbfkjOgikaBi7kknb7a+V6GJcp7jKP9cYEeEoeGeKG+D+yIw+PrRDgwfaQhxQUGFOJz1dyD\\/GbuU3817mdvNHOe5ADmjkEjA4jSCjhnQAGOMi7PUZ0qsVuBjHWSqj0vXnhtjc\\/iieKH2uUPi3QOROlHhytMJurGX66NVWLtvkO80RSivpD01skiTkSDEalZAfd+mk6MMqUFK+YzDngwZA0LxeBBx35RNZzWygnlpfrgES0fv+h\\/OBOZpd1l+sAOuA22FzkJKRqI0M0B8S4EWKG0GLXSvSQazPa5DIiC4MMLrcNWR0GhxX3PfB1K3RbmQALTrXEkMVmdxm5qs6LX6j3JJGxnpmR6E21ntRIbnpLKBCVWpgt00da8DiEmSNqIvT0coKzQJwUKu11iMlOD9wKn\\/R2toiO0t6QNL2magAY8KpWw8aKT5Ut8gBv4+02mp9gawCwwdZAnqz497fydt5mHzNiY93eYDQ02BvIkwk\\/8UaTGSES4FMP5EE8zzmNwp7TDoOD+l3\\/gJ5+KUpz5HVZoYuNl44mO8RE6SjclBCziZeu\\/+Y3zPj0xMAA8wizemBg+iV4tPR4aSM9B\\/9\\/fDyqPhaitRhnyaG4EkJcWlAEH\\/TxyGYqtp\\/KutM+rugWSJnFPElyIuXpRHFwlHpCqny99yoKQllFQtTjxc5UF4QZieeWqzVBZq4NouJPKvLOlZOGlsYWxqSkkdvWpAgJmOCxvSw0mYrgVDZqS7iL0aZquQkzQZXooxU\\/dbloSdtMxU2kKtm\\/o1UweaW6D\\/B8c17Lg9eLmNT3oVnjdLhMsTM52QdDje+a\\/WY\\/tkAFFGsMmS\\/EoOsCbCB8DmQHpREAlbiJuhnjOm1drLmMWJL0NGAtvf2Am6Svl24sIbez5OJuTqfR8DBhVBheApeTe+k6ZVa4TjPdAfgfKCPUbQAAeJxjYGRgYGA8czYnNqY0nt\\/mK4M8BwMIXDDadhVG\\/\\/\\/2bxo7A9tdIJeDgQkkCgCb1A6TAHicY2BkYGC7+28aAwMHw\\/9v\\/8+wMzAARVAACwCgpQZNeJxFULFKA0EQfS9YWFhYBEIKEbkPsLLyF\\/IBFlbBH7A6rC0sUvkH1iGFhYWmu0qOZVnWEEREgkiwSyVX2MS3c5t4w+zOzr03M292QaRvZ946f+AQ4HVGBnpe4xuOJQJrfClb41n+i0+h0isKc2nxUhYsmlmNFDl4HqlayFWTR7nP97udQUyPxbrhjbGj6nr18InNQ\\/UuMM38YLcXyuNJ\\/JSLnZ44jU0fbT6nTNIwtr6VfLxuhKg44kJdXjBlyQ9h3tA1e8y120nnqr95tdO6rCC\\/uI\\/F9k\\/MG\\/u3wVal4542cIuLrH7DqUzDzHZl+xGqL51e3BahLE+xwooHWHZ6nHCIRpkBT6R0qPlL7e1OVklXH1eq4fmKM5zn\\/Ramvcuax3hQ3JiGGvdCFyg4+gNpnrBAAAAAAAAgACAAIAAgAGgApAE2AawCIALyAxwDegPYBHQEvgTuBRQFPAV6Ba4F9AY8Bp4G6gc6B4AH1ggsCHQItAkACWQJogoICqoLJAusDBYMlA0aDWANng4QDmIOvA78D2wPlBAsEJIRFBFaEhISehLyEzITchPqFFAU5BVAFYQVyBYGFkoWjhakFt4XWBfgGDYYtBkyGYwaKhqIGuQbSBvAG\\/4ckhzyHVAdzh5OHqofMB+OH+ogcCFOIfAijCLsI1gjeCPiJCwkLCR4JPIlciYcJrIm7ieWJ9YomiiqKTIpXClkKjwqUiquKwwrUiuiK9wsLixuLJIszCzgLWgtfi2ULaouTi5gLnIuhC6WLrAuyi9SL+ov\\/DAUMCwwRjBeMHAwiDCiMU4xZjF+MZYxrjHAMdoyUDNwM4gzoDO4M9Iz6jQ6NNg05DTwNQI1FDUmNTg1+jZsNn42kDaiNrQ2wDbMNt428DfIN9o37Df+OBA4Ijg0OIY5PDlIOVQ5Zjl4OYo6CDoaOlg6yDtSO9o8jDykPLY80DzoPPo9WD2gPeg+Cj48Plw+pD7IPxg\\/eD+6P9w\\/\\/kAqQFZAhEDQQR5BbEGyQiJCTkKoQzZDgEPKRKJFIEW2RihGeEa8Rw5HMEeQR7hIEEh6SLRJPkm+SjpKuksUS0BLmEv2TEIAAQAAAPsAUwAEAFoABAACABAALwA4AAABrAF3AAMAAXiczZXPbxtFFMff2k5s8ksVhIqKACPBIUX1OiGq1ARxsNIUCSgtaUUQl2qyHnsn2h\\/WztiO+wcg4A\\/ggDihSkgIVYIDEgjEnRNHLhzgBP8AXDjw5rvjNHETftyItbufmXnz3ps335kQ0VrlGgVU\\/r1B9zwHtBQse65QPdjwXKULQe65xjafeJ6hheAHz7PMf3hu0HIl9DwXUG3H8zxtzNz3vEjbsweel2ijfp6jBLUqx12ovwKeYT5Xfws8i\\/4DcB39h+AG+H3wI+zpY2TrOKAV+t1zhXN+1nOVXghe9FyjleA9zzP0RPCl51nmnzw3aLVS8zxXocq253nqzlzzvEjvzPzieYm6s\\/fBc8j5I\\/A88vwUvID+b8BL4O\\/B51ye9R\\/BjzE\\/Wv8VvAybP8GPOz+NOfB5199YAV9wcxvPg5+EzRb4Kdi8Bn4G\\/Db4OdjH4CYY9Wwg58a74NL\\/B44Xyv57YOTf+IKukqYeP5afu6SoQ4IfyW3JFFFOfRpTAauYewXd5q\\/i73Uey\\/ixPN5Hzza3Cmb3lvDoLASt0yb\\/1qnp6TKF3NumhH\\/imG+DluKv4u8Q2YREV3VPW31XdURHWimivD8udC+24nasxPU8y+24r8R2XvTzQlqdZ2J9c3O9ya\\/LoWgniYC1EYUyqhiqDru8wbnuIgdBO3TIsRVn2kFEurG9K9pi59CqrKO4vcvdPRpwspLTol3VGySS4eECNPn5W9dH2TbFdJT\\/lNKbqJA5VuGQrtAaD6jCoALhlbWJz\\/ap\\/toPvP3bnaTTik20B\\/nEPM16aUiOMUQ8J5icunj34Sji0QTSilFPyWSxnHLzneR6PFMfE9CqX8lFZsNJGsxI+XsJ0QRbKS9e5zOHhATHzeF3srAuoikIbcTxJ3YP52K8+N2uO6\\/7sFNs4YqesD93TDQy0Rzd6cO1Yp\\/FPixSeIxhUUZ19XeVSVCP0tYyl2uXvq2wX+GRmppQhYYHw5EiVFtiheWKytpL1DtGlmXOCvVTU\\/s\\/iTnZr+KEwgV2NIMWM6z39N1ytXNZ0p62sbB8GGVnqDIr8q7I+1ZHMhFRLAsZWVXw6YvyXqZxPldZfxeFGRurUnNJSJEpPt2yyI0S3bwQTmNdGSkjRrHrO\\/Ji+PQPko7YV0JJo5Ox6GhjddYbaBOzi\\/2xSGUU64yndtRQJXmfe23O0SV\\/1aEN3cFrtoU2wgyimGO7QJy9tCKWhj2rTCh\\/NNxMt66iPPJiT2einRl9bFlGWa6Au70sb+gWtfg3wi\\/k7T95rkJIKmULZ59yuVv8tmwj+avQMnSH5zqBlLZu1sSaYmv7W63WaDQKU38UwyhPW7FNk1ZqM5mqVmrujFTCvSp03UQ3eWYpXOVFneF2jyCWiQSGXh6TA+PkUc4rIIUUI+Xh3sfBsLDQkF\\/mvZe3t8WdHh0dYONEcjPhHeOtzDPLlUNZWSwdt9u54LEiE2lesIb284EViY5UZlzljS105FRj\\/ud1fh0VSnG9ETNfAcEi+z3guL9xT3Zi\\/Bb2IMP\\/2sJdydUPq59Xv61+x89X1a+rn9G0xwctibqfNf7zlLXbxZPxfMQz\\/SdsN54erz1dW6+9Wnu59hK\\/N6fiZYhxtj\\/Xkqwxd8G5OhBfLQX\\/Bl6L\\/zT3zNZfVLsh6gAAAHicbdBVkJUFAAXg718Wlu6WDgkV715acokVCeky6O5a6e6WkKGHZoChQ0JgBlGQZuhSUklRwhcfiOU+cmbOfHNejyiJebXeeu\\/L\\/28aiJJEtKSSiZFcCimlkloaaaWTXgYZZZJZFlllk10OOX0gl9zyyCuf\\/AooqJDCPlREUcUU95GPfaKET4XECiuplNLKKKuc8j5TQUWVVFZFVXGqqa6GmuJ9rpYv1FZHXfV8qb4GGmqksSaaaqa5Flr6yte+8a1WWmujbRBlpXHG22+e+yaYYaol1lkVJDEliDbWHM88Nz1IapLf\\/Wvpmz9eeuE\\/K2z0myM2aae973VwXEdHHXPaCSed8kAn55xx1mad\\/WOWi867oItHnpism66666mHXpbpra8++ukvwQDfGeihQYYYbKjhhtltuZFGGGW0x\\/621yW33bHFVnfds8+f\\/rLGZbdcdc11N9x0xR8W2Wa7XX502A47\\/WKMQyba4FcHHAySmRbEmB8kt9ACT\\/3kZ6vNtthaM831gz1BiiBlkCpIHZPQq2soFBeKWCNi\\/DurlY6umdCv99sRjg2XSTQcSjQ+FApFjI0YjljyNSE1jL0AAAB4nH3QXU\\/TUBgH8HO6Oc+ga3XsdC9dOaL4Wl87RdSbZVzuhgBC5yABhYXJwqbFlxtSFrIMQmZGFkO420c4i5nrlSHLbvBTeOln8GY+dcZ4YXxyfn3+T0\\/Tk9bpn3z6SiMTTv8k6T+FsNP71hNgaveGhieqtuP5\\/pk1bIOV3ZicZHW7aQusi4tduyvYXzDqwFru1DvNjhe16+1m2zP6XtHeKfG3ivpGiW0pUUspKQ5ByYhWKCrheKEYVgvF6Mamom5s2q9jL0PuZl\\/L5UM0nstTNZePrq2H1LX1yqvY8dSPsSPQAIegBg7APqiCCtgFZWCDbWAcLxJ2tETYR9CAfAhqC4QdgH1QNQmrgF1QhtkG2+DFCmHPgbGUJWwRmPOELYCVp4QtAyMLl3mgPoRfRekDGrxP5QQVDeq\\/R313qecORbfpzVvyDV26dl2+clUavyxfvCRdGJNHmaTGtUAkGgtQJRwIjoQC8rnzohiQRP\\/QsJgc950losd7RkRYEJM+XJKx+oQw+TFhnkeEoUnCphOYB9MoPZfiIxj6bIon9DT8vRlu6GlOprNmC+MPGbjLhT0Hoznu3XMEaMGpZ1nTwVF3u6LCaLa82ME7lVpN\\/ZMyGV3jq+lZk5e0DDfcUNcySIeyLP0\\/hXU0aIPuht+F\\/vn0X8Uj7kGpwdDyu5+xOpOyrC13\\/ToZ3vETRom3cg==\') format(\'woff\');\\r\\n\\t}\\r\\n\\t\\r\\n\\t@media only screen and (min-height: 500px) {\\r\\n\\t\\t.viewFull {\\r\\n\\t\\t\\tdisplay: none;\\r\\n\\t\\t}\\r\\n\\t}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">\\r\\n<div class=\\\"wrap\\\">\\r\\n<div class=\\\"404\\\">\\r\\n<pre><code>\\r\\n\\t <span class=\\\"green\\\">&lt;!<\\/span><span>DOCTYPE html<\\/span><span class=\\\"green\\\">&gt;<\\/span>\\r\\n<span class=\\\"orange\\\">&lt;html&gt;<\\/span>\\r\\n    <span class=\\\"orange\\\">&lt;style&gt;<\\/span>\\r\\n   * {\\r\\n\\t\\t        <span class=\\\"green\\\">everything<\\/span>:<span class=\\\"blue\\\">awesome<\\/span>;\\r\\n}\\r\\n     <span class=\\\"orange\\\">&lt;\\/style&gt;<\\/span>\\r\\n <span class=\\\"orange\\\">&lt;body&gt;<\\/span> \\r\\n              ERROR 404!\\r\\n\\t\\t\\t\\tFILE NOT FOUND!\\r\\n\\t\\t\\t\\t<span class=\\\"comment\\\">&lt;!--The file you are looking for, \\r\\n\\t\\t\\t\\t\\tis not where you think it is.--&gt;\\r\\n\\t\\t<\\/span>\\r\\n <span class=\\\"orange\\\"><\\/span> \\r\\n\\t\\t\\t  \\r\\n\\r\\n\\r\\n<\\/code><\\/pre>\\r\\n<\\/div>\\r\\n<br \\/> <span class=\\\"info\\\"> <br \\/> <span class=\\\"orange\\\">&nbsp;&lt;\\/body&gt;<\\/span> <br \\/> <span class=\\\"orange\\\">&lt;\\/html&gt;<\\/span> <\\/span><\\/div>\\r\\n<\\/div>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:22:09\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:21:57\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"0\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":22,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"34\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(22,2,1,'','2015-10-25 18:24:02',332,1784,'4c6cf77b196b5b17c2ed196a9f0628bb0d88c7aa','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<h1 style=\\\"text-align: center;\\\">404<\\/h1>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:24:02\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:22:09\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":23,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"36\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(23,2,1,'','2015-10-25 18:26:47',332,4653,'00d716d5e3f54134987de111c192959d6e804276','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"\\r\\n<style>\\r\\n\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n\\r\\n\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 140px;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n\\r\\n<\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<br \\/><br \\/>\\r\\n<span class=\\\"info\\\">File not found<\\/span>\\r\\n<img src=\\\"http:\\/\\/images2.layoutsparks.com\\/1\\/160030\\/too-much-tv-static.gif\\\" class=\\\"static\\\" \\/>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:26:47\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:24:02\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":24,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"37\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(24,2,1,'','2015-10-25 18:27:40',332,4700,'118aee3466c23daf0e3232373e69a5a062efb6e6','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style type=\\\"text\\/css\\\" scoped=\\\"scoped\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n\\r\\n\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 140px;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<img src=\\\"http:\\/\\/images2.layoutsparks.com\\/1\\/160030\\/too-much-tv-static.gif\\\" class=\\\"static\\\" \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:27:40\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:26:47\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":25,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"39\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(25,2,1,'','2015-10-25 18:28:12',332,4700,'439e9c2f27f1b103ac5e1b400fd3d699c831730f','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n\\r\\n\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 140px;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<img src=\\\"http:\\/\\/images2.layoutsparks.com\\/1\\/160030\\/too-much-tv-static.gif\\\" class=\\\"static\\\" \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:28:12\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:27:40\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":26,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"40\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(26,2,1,'','2015-10-25 18:30:59',332,4697,'cf00d743dfcebb517fefd7608e7cfdb2ab924db0','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: 30px auto;\\r\\n  position: relative;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: relative;\\r\\n  top: 140px;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<img src=\\\"http:\\/\\/images2.layoutsparks.com\\/1\\/160030\\/too-much-tv-static.gif\\\" class=\\\"static\\\" \\/><\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:30:59\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:28:12\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":27,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"41\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(27,2,1,'','2015-10-25 18:34:20',332,4595,'a6eee03bb259f8a4d9a2b7da51f4e08ae816f940','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: 30px auto;\\r\\n  position: relative;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: relative;\\r\\n  top: 140px;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:34:20\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:30:59\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":28,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"42\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(28,2,1,'','2015-10-25 18:35:28',332,4562,'fe663d1e95f714f2e9ebef58433999b02193c5f5','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: 30px auto;\\r\\n  position: relative;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: relative;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:35:28\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:34:20\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":29,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"43\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(29,2,1,'','2015-10-25 18:35:43',332,4562,'3f0fc3e4ee36540b2e2f710daef2265306a0f74e','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: 30px auto;\\r\\n  position: relative;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:35:43\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:35:28\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":30,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"44\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(30,2,1,'','2015-10-25 18:36:47',332,4568,'6a91bb5d20a535827aeac47922730e5c9cf8f7fc','{\"id\":2,\"asset_id\":\"61\",\"title\":\"Beitrag konnte leider nicht gefunden werden\",\"alias\":\"404-artikel\",\"introtext\":\"<style scoped=\\\"scoped\\\" type=\\\"text\\/css\\\"><!--\\r\\n@import url(http:\\/\\/fonts.googleapis.com\\/css?family=Gilda+Display);\\r\\n.static {\\r\\n  width: 100%;\\r\\n  height: 100%;\\r\\n  position: relative;\\r\\n  margin: 0;\\r\\n  padding: 0;\\r\\n  top: -100px;\\r\\n  opacity: 0.05;\\r\\n  z-index: 230;\\r\\n  user-select: none;\\r\\n  user-drag: none;\\r\\n}\\r\\n\\r\\n.error {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 95px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: 30px auto;\\r\\n  position: relative;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: -60px;\\r\\n  right: 0;\\r\\n  animation: noise 2s linear infinite;\\r\\n  overflow: default;\\r\\n}\\r\\n\\r\\n.error:after {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 150px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: blue;\\r\\n  animation: noise-1 .2s linear infinite;\\r\\n}\\r\\n\\r\\n.info {\\r\\n  text-align: center;\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 15px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 200px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: -40px auto;\\r\\n  position: absolute;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  animation: noise-3 1s linear infinite;\\r\\n}\\r\\n\\r\\n.error:before {\\r\\n  content: \'404\';\\r\\n  font-family: \'Gilda Display\', serif;\\r\\n  font-size: 100px;\\r\\n  font-style: italic;\\r\\n  text-align: center;\\r\\n  width: 100px;\\r\\n  height: 60px;\\r\\n  line-height: 60px;\\r\\n  margin: auto;\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  right: 0;\\r\\n  opacity: 0;\\r\\n  color: red;\\r\\n  animation: noise-2 .2s linear infinite;\\r\\n}\\r\\n\\r\\n@keyframes noise-1 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: -6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: 2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise-2 {\\r\\n  0%, 20%, 40%, 60%, 70%, 90% {opacity: 0;}\\r\\n  10% {opacity: .1;}\\r\\n  50% {opacity: .5; left: 6px;}\\r\\n  80% {opacity: .3;}\\r\\n  100% {opacity: .6; left: -2px;}\\r\\n}\\r\\n\\r\\n@keyframes noise {\\r\\n  0%, 3%, 5%, 42%, 44%, 100% {opacity: 1; transform: scaleY(1);}  \\r\\n  4.3% {opacity: 1; transform: scaleY(1.7);}\\r\\n  43% {opacity: 1; transform: scaleX(1.5);}\\r\\n}\\r\\n\\r\\n@keyframes noise-3 {\\r\\n  0%,3%,5%,42%,44%,100% {opacity: 1; transform: scaleY(1);}\\r\\n  4.3% {opacity: 1; transform: scaleY(4);}\\r\\n  43% {opacity: 1; transform: scaleX(10) rotate(60deg);}\\r\\n}\\r\\n--><\\/style>\\r\\n<div class=\\\"error\\\">404<\\/div>\\r\\n<p><br \\/><br \\/> <span class=\\\"info\\\">Beitrag nicht gefunden<\\/span>&nbsp;<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-10-25 17:40:28\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-10-25 18:36:47\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-10-25 18:35:43\",\"publish_up\":\"2015-10-25 17:40:28\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"0\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"0\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"0\\\",\\\"show_modify_date\\\":\\\"0\\\",\\\"show_publish_date\\\":\\\"0\\\",\\\"show_item_navigation\\\":\\\"0\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"0\\\",\\\"show_email_icon\\\":\\\"0\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"0\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":31,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"45\",\"metadata\":\"{\\\"robots\\\":\\\"noindex, nofollow\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(56,3,1,'','2015-11-01 19:05:38',332,2913,'7b4b41d5a04048645e5e588bf2d462497b820bdf','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n<div class=\\\"row\\\">\\r\\n<div class=\\\"span8\\\">\\r\\n<div id=\\\"myCarousel\\\" class=\\\"carousel slide\\\"><ol class=\\\"carousel-indicators\\\">\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n<\\/ol><!-- Carousel items -->\\r\\n<div class=\\\"carousel-inner\\\">\\r\\n<div class=\\\"active item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">jQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 2000\\r\\n\\t\\t})\\r\\n\\t});<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-01 19:05:38\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-01 19:00:37\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":27,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"49\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(57,3,1,'','2015-11-02 18:41:21',332,3071,'a83d702c492c4d57da0a3808ba9a370d37a8bb9b','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-fade carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 2000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:41:21\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:40:54\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":28,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"58\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(58,3,1,'','2015-11-02 18:44:48',332,3071,'5f067607fb6f6ab2beb7bfc0282c1f141764e32d','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 2000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:44:48\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:41:21\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":29,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"60\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(59,3,1,'','2015-11-02 18:52:26',332,3071,'a1edb1f9f9ea3c9677e8da8e863c28628e100ea1','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:52:26\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:44:48\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":30,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"64\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(60,3,1,'','2015-11-02 18:56:41',332,3072,'8b4826b9747799c3d791a17387b3ebccf59abdce','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/650\\/400\\/nature\\/grey\\\" \\/> alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:56:41\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:52:26\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":31,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"65\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(61,3,1,'','2015-11-02 18:57:40',332,3072,'307742188bc3eae0707d36a55c1637e4310cd06d','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" \\/> alt=\\\"dummy slide\\\" \\/&gt;<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"images\\/content\\/bootstrap_slider\\/dummy_slide.jpg\\\" alt=\\\"Dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:57:40\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:56:41\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":32,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"66\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(62,3,1,'','2015-11-02 18:58:36',332,3050,'c717dbd5c9eea7e05f75b4edb708b40271b57d7d','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\"\\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\"\\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\"\\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:58:36\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:57:40\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":33,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"68\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(63,3,1,'','2015-11-02 18:59:18',332,3051,'f919c73208e8f569c586602b9ada4b8078c6a7ac','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/sports\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/city\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 18:59:18\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:58:36\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":34,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"69\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(64,3,1,'','2015-11-02 19:00:16',332,3054,'2834d0b4fe49d85c21f843ee9c9b176d3dfa352f','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"<div class=\\\"container\\\">\\r\\n\\t<div class=\\\"row\\\">\\r\\n\\t\\t<div class=\\\"span8\\\">\\r\\n\\t\\t\\t<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\">\\r\\n\\t\\t\\t\\t<ol class=\\\"carousel-indicators\\\">\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n\\t\\t\\t\\t\\t<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<!-- Carousel items -->\\r\\n\\t\\t\\t\\t<div class=\\\"carousel-inner\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/sports\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/fashion\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<script type=\\\"text\\/javascript\\\">\\r\\n\\tjQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});\\r\\n<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 19:00:16\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2015-11-02 18:59:18\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":35,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"71\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(65,3,1,'','2016-09-28 21:30:36',332,2914,'038067c191e46f8e87c137a83b7c7f6725138893','{\"id\":3,\"asset_id\":\"62\",\"title\":\"Beitrag mit Slideshow\",\"alias\":\"beitrag-mit-slideshow\",\"introtext\":\"\\r\\n<div class=\\\"row-fluid\\\">\\r\\n<div class=\\\"span8\\\">\\r\\n<div id=\\\"myCarousel\\\" class=\\\"carousel-fade carousel slide\\\"><ol class=\\\"carousel-indicators\\\">\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"0\\\" class=\\\"active\\\"><\\/li>\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"1\\\"><\\/li>\\r\\n<li data-target=\\\"#myCarousel\\\" data-slide-to=\\\"2\\\"><\\/li>\\r\\n<\\/ol><!-- Carousel items -->\\r\\n<div class=\\\"carousel-inner\\\">\\r\\n<div class=\\\"active item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/nature\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/sports\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n<div class=\\\"item\\\"><img src=\\\"http:\\/\\/lorempixel.com\\/g\\/650\\/400\\/fashion\\\" alt=\\\"dummy slide\\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n<!-- Carousel nav --><a class=\\\"carousel-control left\\\" href=\\\"#myCarousel\\\" data-slide=\\\"prev\\\">&lsaquo;<\\/a> <a class=\\\"carousel-control right\\\" href=\\\"#myCarousel\\\" data-slide=\\\"next\\\">&rsaquo;<\\/a><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<script type=\\\"text\\/javascript\\\">jQuery(document).ready(function() {\\r\\n\\t\\tjQuery(\'.carousel\').carousel({\\r\\n\\t\\t\\tinterval: 4000\\r\\n\\t\\t})\\r\\n\\t});<\\/script>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2015-11-01 18:08:57\",\"created_by\":\"332\",\"created_by_alias\":\"\",\"modified\":\"2016-09-28 21:30:36\",\"modified_by\":\"332\",\"checked_out\":\"332\",\"checked_out_time\":\"2016-09-28 21:30:11\",\"publish_up\":\"2015-11-01 18:08:57\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":36,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"8\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0);
/*!40000 ALTER TABLE `w089_ucm_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_update_sites`
--

DROP TABLE IF EXISTS `w089_update_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_update_sites`
--

LOCK TABLES `w089_update_sites` WRITE;
/*!40000 ALTER TABLE `w089_update_sites` DISABLE KEYS */;
INSERT INTO `w089_update_sites` VALUES (1,'Joomla! Core','collection','https://update.joomla.org/core/list.xml',1,1494252213,''),(3,'Accredited Joomla! Translations','collection','https://update.joomla.org/language/translationlist_3.xml',1,0,''),(4,'Joomla! Update Component Update Site','extension','https://update.joomla.org/core/extensions/com_joomlaupdate.xml',1,0,''),(6,'z-index development','collection','http://www.z-index.net/en/extensions.xml',1,0,''),(8,'JCE Editor Package','collection','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&file=pkg_jce.xml',1,0,'');
/*!40000 ALTER TABLE `w089_update_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_update_sites_extensions`
--

DROP TABLE IF EXISTS `w089_update_sites_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_update_sites_extensions`
--

LOCK TABLES `w089_update_sites_extensions` WRITE;
/*!40000 ALTER TABLE `w089_update_sites_extensions` DISABLE KEYS */;
INSERT INTO `w089_update_sites_extensions` VALUES (1,700),(3,802),(3,10002),(4,28),(6,10010),(6,10012),(8,10018);
/*!40000 ALTER TABLE `w089_update_sites_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_updates`
--

DROP TABLE IF EXISTS `w089_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_updates`
--

LOCK TABLES `w089_updates` WRITE;
/*!40000 ALTER TABLE `w089_updates` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_user_keys`
--

DROP TABLE IF EXISTS `w089_user_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_user_keys`
--

LOCK TABLES `w089_user_keys` WRITE;
/*!40000 ALTER TABLE `w089_user_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_user_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_user_notes`
--

DROP TABLE IF EXISTS `w089_user_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_user_notes`
--

LOCK TABLES `w089_user_notes` WRITE;
/*!40000 ALTER TABLE `w089_user_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_user_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_user_profiles`
--

DROP TABLE IF EXISTS `w089_user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_user_profiles`
--

LOCK TABLES `w089_user_profiles` WRITE;
/*!40000 ALTER TABLE `w089_user_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_user_usergroup_map`
--

DROP TABLE IF EXISTS `w089_user_usergroup_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_user_usergroup_map`
--

LOCK TABLES `w089_user_usergroup_map` WRITE;
/*!40000 ALTER TABLE `w089_user_usergroup_map` DISABLE KEYS */;
INSERT INTO `w089_user_usergroup_map` VALUES (332,8),(333,8);
/*!40000 ALTER TABLE `w089_user_usergroup_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_usergroups`
--

DROP TABLE IF EXISTS `w089_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_usergroups`
--

LOCK TABLES `w089_usergroups` WRITE;
/*!40000 ALTER TABLE `w089_usergroups` DISABLE KEYS */;
INSERT INTO `w089_usergroups` VALUES (1,0,1,18,'Public'),(2,1,8,15,'Registered'),(3,2,9,14,'Author'),(4,3,10,13,'Editor'),(5,4,11,12,'Publisher'),(6,1,4,7,'Manager'),(7,6,5,6,'Administrator'),(8,1,16,17,'Super Users'),(9,1,2,3,'Guest');
/*!40000 ALTER TABLE `w089_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_users`
--

DROP TABLE IF EXISTS `w089_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `idx_name` (`name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_users`
--

LOCK TABLES `w089_users` WRITE;
/*!40000 ALTER TABLE `w089_users` DISABLE KEYS */;
INSERT INTO `w089_users` VALUES (332,'Chris Gabler','cg','cg@089webdesign.de','$2y$10$mZm.PyY95HbXahW.GWP8teKbjKWcHkdt5088Pdz17Ue3ocPQdupdy',0,1,'2015-09-08 10:47:01','2017-05-08 14:03:33','0','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"jce\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0,'','',0),(333,'Mic Schneider','adminmic','info@089webdesign.de','$2y$10$9YNLAvKpYZg4NapsNVv/YuDRk5tkEG8AXybQ2a0JYj2vaHG7TFZd6',0,1,'2015-10-25 17:16:17','0000-00-00 00:00:00','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"jce\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0,'','',0);
/*!40000 ALTER TABLE `w089_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_utf8_conversion`
--

DROP TABLE IF EXISTS `w089_utf8_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_utf8_conversion`
--

LOCK TABLES `w089_utf8_conversion` WRITE;
/*!40000 ALTER TABLE `w089_utf8_conversion` DISABLE KEYS */;
INSERT INTO `w089_utf8_conversion` VALUES (2);
/*!40000 ALTER TABLE `w089_utf8_conversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_viewlevels`
--

DROP TABLE IF EXISTS `w089_viewlevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_viewlevels`
--

LOCK TABLES `w089_viewlevels` WRITE;
/*!40000 ALTER TABLE `w089_viewlevels` DISABLE KEYS */;
INSERT INTO `w089_viewlevels` VALUES (1,'Public',0,'[1]'),(2,'Registered',2,'[6,2,8]'),(3,'Special',3,'[6,3,8]'),(5,'Guest',1,'[9]'),(6,'Super Users',4,'[8]');
/*!40000 ALTER TABLE `w089_viewlevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_wf_profiles`
--

DROP TABLE IF EXISTS `w089_wf_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_wf_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `users` text NOT NULL,
  `types` text NOT NULL,
  `components` text NOT NULL,
  `area` tinyint(3) NOT NULL,
  `device` varchar(255) NOT NULL,
  `rows` text NOT NULL,
  `plugins` text NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_wf_profiles`
--

LOCK TABLES `w089_wf_profiles` WRITE;
/*!40000 ALTER TABLE `w089_wf_profiles` DISABLE KEYS */;
INSERT INTO `w089_wf_profiles` VALUES (1,'Default','Default Profile for all users','','6,7,3,4,5,8','',0,'desktop,tablet,phone','help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,blockquote,formatselect,styleselect,removeformat,cleanup;fontselect,fontsizeselect,forecolor,backcolor,spacer,clipboard,indent,outdent,lists,sub,sup,textcase,charmap,hr;directionality,fullscreen,preview,source,print,searchreplace,spacer,table;visualaid,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article','charmap,contextmenu,browser,inlinepopups,media,help,clipboard,searchreplace,directionality,fullscreen,preview,source,table,textcase,print,style,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists,formatselect,styleselect,fontselect,fontsizeselect,fontcolor,hr',1,1,0,'0000-00-00 00:00:00','{\"editor\":{\"allow_javascript\":\"1\",\"allow_css\":\"1\",\"verify_html\":\"0\",\"schema\":\"html5\"},\"media\":{\"iframes\":\"1\"},\"visualblocks\":{\"state\":\"1\"}}'),(2,'Front End','Sample Front-end Profile','','3,4,5','',1,'desktop,tablet,phone','help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,formatselect,styleselect;clipboard,searchreplace,indent,outdent,lists,cleanup,charmap,removeformat,hr,sub,sup,textcase,nonbreaking,visualchars,visualblocks;fullscreen,preview,print,visualaid,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article','charmap,contextmenu,inlinepopups,help,clipboard,searchreplace,fullscreen,preview,print,style,textcase,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists,formatselect,styleselect,hr',0,2,0,'0000-00-00 00:00:00',''),(3,'Blogger','Simple Blogging Profile','','3,4,5,6,8,7','',0,'desktop,tablet,phone','bold,italic,strikethrough,lists,blockquote,spacer,justifyleft,justifycenter,justifyright,spacer,link,unlink,imgmanager,article,spellchecker,fullscreen,kitchensink;formatselect,styleselect,underline,justifyfull,clipboard,removeformat,charmap,indent,outdent,undo,redo,help','link,imgmanager,article,spellchecker,fullscreen,kitchensink,clipboard,contextmenu,inlinepopups,lists,formatselect,styleselect,hr',0,3,0,'0000-00-00 00:00:00','{\"editor\":{\"toggle\":\"0\"}}'),(4,'Mobile','Sample Mobile Profile','','3,4,5,6,8,7','',0,'tablet,phone','undo,redo,spacer,bold,italic,underline,formatselect,spacer,justifyleft,justifycenter,justifyfull,justifyright,spacer,fullscreen,kitchensink;styleselect,lists,spellchecker,article,link,unlink','fullscreen,kitchensink,spellchecker,article,link,inlinepopups,lists,formatselect,styleselect',0,4,0,'0000-00-00 00:00:00','{\"editor\":{\"toolbar_theme\":\"mobile\",\"resizing\":\"0\",\"resize_horizontal\":\"0\",\"resizing_use_cookie\":\"0\",\"toggle\":\"0\",\"links\":{\"popups\":{\"default\":\"\",\"jcemediabox\":{\"enable\":\"0\"},\"window\":{\"enable\":\"0\"}}}}}'),(5,'Copy of Default','Default Profile for all users','','6,2,3,4,5,10,12,8','',0,'desktop,tablet,phone','help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,blockquote,formatselect,styleselect,removeformat,cleanup;fontselect,fontsizeselect,forecolor,backcolor,spacer,clipboard,indent,outdent,lists,sub,sup,textcase,charmap,hr;directionality,fullscreen,preview,source,print,searchreplace,spacer,table;visualaid,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article,imgmanager_ext,mediamanager','contextmenu,browser,inlinepopups,media,help,clipboard,searchreplace,directionality,fullscreen,preview,source,table,textcase,print,style,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists,charmap,imgmanager_ext,mediamanager,formatselect,styleselect,fontselect,fontsizeselect,fontcolor,hr',0,1,0,'0000-00-00 00:00:00','{\"editor\":{\"width\":\"\",\"height\":\"\",\"toolbar_theme\":\"default\",\"toolbar_align\":\"left\",\"toolbar_location\":\"top\",\"statusbar_location\":\"bottom\",\"path\":\"1\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"resizing_use_cookie\":\"1\",\"toggle\":\"1\",\"toggle_label\":\"[Toggle Editor]\",\"toggle_state\":\"1\",\"relative_urls\":\"1\",\"verify_html\":\"\",\"schema\":\"\",\"forced_root_block\":\"\",\"profile_content_css\":\"2\",\"profile_content_css_custom\":\"\",\"theme_advanced_styles\":\"\",\"theme_advanced_blockformats\":[\"p\",\"div\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\",\"address\",\"code\",\"pre\",\"samp\",\"span\",\"section\",\"article\",\"hgroup\",\"aside\",\"figure\",\"dt\",\"dd\"],\"theme_advanced_fonts_add\":\"\",\"theme_advanced_fonts_remove\":\"\",\"theme_advanced_font_sizes\":\"\",\"custom_colors\":\"\",\"dir\":\"\",\"filesystem\":{\"name\":\"joomla\",\"joomla\":{\"allow_root\":\"0\",\"restrict_dir\":\"administrator,cache,components,includes,language,libraries,logs,media,modules,plugins,templates,xmlrpc\"}},\"max_size\":\"\",\"upload_conflict\":\"overwrite\",\"upload_runtimes\":[\"html5\",\"flash\",\"silverlight\",\"html4\"],\"browser_position\":\"bottom\",\"folder_tree\":\"1\",\"list_limit\":\"all\",\"validate_mimetype\":\"1\",\"websafe_mode\":\"utf-8\",\"websafe_allow_spaces\":\"0\",\"upload_add_random\":\"0\",\"invalid_elements\":\"\",\"invalid_attributes\":\"dynsrc,lowsrc\",\"invalid_attribute_values\":\"\",\"extended_elements\":\"\",\"allow_javascript\":\"1\",\"allow_css\":\"1\",\"allow_php\":\"0\",\"cdata\":\"1\"},\"browser\":{\"dir\":\"\",\"max_size\":\"\",\"extensions\":\"office=doc,docx,ppt,xls;image=gif,jpeg,jpg,png;acrobat=pdf;archive=zip,tar,gz,rar;flash=swf;quicktime=mov,mp4,qt;windowsmedia=wmv,asx,asf,avi;audio=wav,mp3,aiff;openoffice=odt,odg,odp,ods,odf\",\"filesystem\":{\"name\":\"\"},\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\"},\"media\":{\"strict\":\"1\",\"iframes\":\"1\",\"audio\":\"1\",\"video\":\"1\",\"object\":\"1\",\"embed\":\"1\",\"version_flash\":\"10,1,53,64\",\"version_windowsmedia\":\"10,00,00,3646\",\"version_quicktime\":\"7,3,0,0\",\"version_java\":\"1,5,0,0\",\"version_shockwave\":\"10,2,0,023\"},\"clipboard\":{\"paste_use_dialog\":\"0\",\"paste_dialog_width\":\"450\",\"paste_dialog_height\":\"400\",\"paste_force_cleanup\":\"0\",\"paste_strip_class_attributes\":\"0\",\"paste_remove_spans\":\"0\",\"paste_remove_styles\":\"0\",\"paste_remove_attributes\":\"\",\"paste_retain_style_properties\":\"\",\"paste_remove_empty_paragraphs\":\"1\",\"paste_remove_styles_if_webkit\":\"0\",\"paste_process_footnotes\":\"convert\",\"paste_html\":\"1\",\"paste_text\":\"1\",\"buttons\":[\"cut\",\"copy\",\"paste\"]},\"source\":{\"highlight\":\"1\",\"numbers\":\"1\",\"wrap\":\"1\",\"format\":\"1\",\"tag_closing\":\"1\",\"selection_match\":\"1\",\"theme\":\"textmate\"},\"table\":{\"width\":\"\",\"height\":\"\",\"border\":\"0\",\"cols\":\"2\",\"rows\":\"2\",\"cellpadding\":\"\",\"cellspacing\":\"\",\"align\":\"\",\"classes\":\"\",\"pad_empty_cells\":\"1\",\"buttons\":[\"table_insert\",\"delete_table\",\"row_props\",\"cell_props\",\"row_before\",\"row_after\",\"delete_row\",\"col_before\",\"col_after\",\"delete_col\",\"split_cells\",\"merge_cells\"]},\"visualblocks\":{\"state\":\"1\"},\"xhtmlxtras\":{\"buttons\":[\"cite\",\"abbr\",\"acronym\",\"del\",\"ins\",\"attribs\"]},\"imgmanager\":{\"dir\":\"\",\"max_size\":\"\",\"extensions\":\"image=jpeg,jpg,png,gif\",\"filesystem\":{\"name\":\"joomla\"},\"alt\":\"\",\"margin_top\":\"\",\"margin_right\":\"\",\"margin_bottom\":\"\",\"margin_left\":\"\",\"border\":\"0\",\"border_width\":\"1\",\"border_style\":\"solid\",\"border_color\":\"#000000\",\"align\":\"\",\"always_include_dimensions\":\"1\",\"style\":\"\",\"classes\":\"\",\"title\":\"\",\"id\":\"\",\"direction\":\"\",\"usemap\":\"\",\"longdesc\":\"\",\"tabs_rollover\":\"1\",\"tabs_advanced\":\"1\",\"attributes_dimensions\":\"1\",\"attributes_align\":\"1\",\"attributes_margin\":\"1\",\"attributes_border\":\"1\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\",\"dragdrop_upload\":\"1\"},\"link\":{\"target\":\"\",\"id\":\"\",\"style\":\"\",\"classes\":\"\",\"dir\":\"\",\"hreflang\":\"\",\"lang\":\"\",\"charset\":\"\",\"type\":\"\",\"rel\":\"\",\"rev\":\"\",\"tabindex\":\"\",\"accesskey\":\"\",\"file_browser\":\"1\",\"tabs_advanced\":\"1\",\"attributes_anchor\":\"1\",\"attributes_target\":\"1\",\"links\":{\"joomlalinks\":{\"enable\":\"1\",\"content\":\"1\",\"article_alias\":\"1\",\"static\":\"1\",\"contacts\":\"1\",\"weblinks\":\"1\",\"weblinks_alias\":\"1\",\"menu\":\"1\"}},\"popups\":{\"default\":\"\",\"jcemediabox\":{\"enable\":\"1\"},\"window\":{\"enable\":\"1\"}},\"search\":{\"link\":{\"enable\":\"1\",\"plugins\":[\"categories\",\"content\"]}}},\"spellchecker\":{\"engine\":\"browser\",\"browser_state\":\"0\",\"googlespell_languages\":[\"English=en\"],\"languages\":\"English=en\",\"pspell_mode\":\"PSPELL_FAST\",\"pspell_spelling\":\"\",\"pspell_jargon\":\"\",\"pspell_encoding\":\"\",\"pspell_dictionary\":\"components\\/com_jce\\/editor\\/tiny_mce\\/plugins\\/spellchecker\\/dictionary.pws\",\"pspellshell_aspell\":\"\\/usr\\/bin\\/aspell\",\"pspellshell_tmp\":\"\\/tmp\"},\"article\":{\"buttons\":[\"readmore\",\"pagebreak\"]},\"lists\":{\"number_styles\":[\"default\",\"lower-alpha\",\"lower-greek\",\"lower-roman\",\"upper-alpha\",\"upper-roman\"],\"bullet_styles\":[\"default\",\"circle\",\"disc\",\"square\"],\"buttons\":[\"numlist\",\"bullist\"]}}');
/*!40000 ALTER TABLE `w089_wf_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_xmap_items`
--

DROP TABLE IF EXISTS `w089_xmap_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_xmap_items` (
  `uid` varchar(250) NOT NULL DEFAULT '',
  `itemid` int(11) unsigned NOT NULL,
  `view` varchar(10) NOT NULL DEFAULT '',
  `sitemap_id` int(11) unsigned NOT NULL,
  `properties` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`,`itemid`,`view`,`sitemap_id`),
  KEY `uid` (`uid`,`itemid`),
  KEY `view` (`view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_xmap_items`
--

LOCK TABLES `w089_xmap_items` WRITE;
/*!40000 ALTER TABLE `w089_xmap_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_xmap_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w089_xmap_sitemap`
--

DROP TABLE IF EXISTS `w089_xmap_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w089_xmap_sitemap` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `alias` varchar(250) DEFAULT NULL,
  `introtext` text,
  `params` text,
  `selections` text,
  `excluded_items` text,
  `published` tinyint(1) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `count_xml` int(11) unsigned NOT NULL DEFAULT '0',
  `count_html` int(11) unsigned NOT NULL DEFAULT '0',
  `views_xml` int(11) unsigned NOT NULL DEFAULT '0',
  `views_html` int(11) unsigned NOT NULL DEFAULT '0',
  `lastvisit_xml` int(11) unsigned DEFAULT NULL,
  `lastvisit_html` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_state` (`published`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w089_xmap_sitemap`
--

LOCK TABLES `w089_xmap_sitemap` WRITE;
/*!40000 ALTER TABLE `w089_xmap_sitemap` DISABLE KEYS */;
/*!40000 ALTER TABLE `w089_xmap_sitemap` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-08 20:01:44
