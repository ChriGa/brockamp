<?php 

/**
 * @package 	mod_ap_ajax_quick_contact.php - AP Ajax Quick Contact Module
 * @version		3.3
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

//no direct access
defined('_JEXEC') or die;
ini_set('display_errors',0);

// Global variables  
$doc = JFactory::getDocument(); 

$layout = $params->get('js', 'jqscript');
// Path assignments
$path = str_replace("&", "",$path);
$ibase = JURI::base();
if(substr($ibase, -1)=="/") { $ibase = substr($ibase, 0, -1); }
$modURL = 'modules/mod_ap_ajax_089multi_contact';

// Load css
$doc->addStyleSheet($modURL.'/assets/css/style.css');
$doc->addStyleSheet('//netdna.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css');// font awesome url

// Parameters
$uniqid = $module->id;
$form_width = $params->get('form_width');
//CG
$subs = array(
	'subOne' => $params->get('subOne'),
	'subTwo' => $params->get('subTwo'),
	'subThree' => $params->get('subThree'),
);
/*joomla Model aufrufen um item detials verfügbar zu machen für Produkt-Items*/
	$model = JModelList::getInstance('Articles', 'ContentModel', array('ignore_request'=>true));
	$appParams = JFactory::getApplication()->getParams();
	$model->setState('params', $appParams);
	$model->setState('filter.category_id', $child->id);
	$items = $model->getItems();
/*<-- CG END*/

$name = $params->get('name','Name');
$email = $params->get('email','Email');
$message = $params->get('message','Message');
$captcha_label = $params->get('captcha_label','0');
$captcha = $params->get('captcha','Captcha');
$ap_captchaError = $params->get('ap_captchaError','Please enter the correct Captcha!'); 
$submit = $params->get('submit','Send');
$ap_error_email_not_set = $params->get('ap_error_email_not_set');
// $ap_send_message params with decoded hmtl characters from editor in admin (Ajax message when email has been sent)
$ap_send_message = $params->get('ap_send_message');

// remove comments from sent message (if any) */
$ap_send_message = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '',$ap_send_message);
// remove tabs, spaces, new lines, etc. */ 
$ap_send_message = str_replace(array("\r\n","\r","\n","\t",'  ','    ','    '),'',$ap_send_message);
// replace apostrophe to avoid problems */ 
$ap_send_message = str_replace("'","&#39;", $ap_send_message);

$ap_error_name = $params->get('ap_error_name','Please enter your Name!');
$ap_error_email = $params->get('ap_error_email','Please enter your Email address!');
$ap_error_field = $params->get('ap_error_field');
$ap_script_required = $params->get('ap_script_required');
$ap_script_email = $params->get('ap_script_email');
$subject = $params->get('subject');
$recipient = $params->get('recipient','');
$moduleclass_sfx = $params->get('moduleclass_sfx');
//CG Override DSGVO u.a.
$recipientClean = str_replace("@", " (at) ", $recipient);
$dsgvo_text = 'Ich stimme zu, dass meine Angaben aus dem Kontaktformular zur Beantwortung meiner Anfrage erhoben und verarbeitet werden. Die Daten werden nach abgeschlossener Bearbeitung Ihrer Anfrage gelöscht.
Hinweis: Sie können Ihre Einwilligung jederzeit für die Zukunft per E-Mail an <span style="font-weight: bold; color: #000000;">' .$recipientClean. '</span> widerrufen.';


if(isset($_POST['submitted'])) {
	// require a name from user
	if(trim($_POST['ap_name']) === '') {
		$_POST['ap_name'] = "Kein Name angegeben";
/*		$nameError =  ''; 
		$hasError = true;*/
	} else {
		$name = trim($_POST['ap_name']);
	}
	// need valid email
	if(trim($_POST['ap_email']) === '')  {
		$emailError = 'Please enter your Email!';
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['ap_email']))) {
		$emailError = 'Invalid email address.';
		$hasError = true;
	} 
		$clientMailAddress = trim($_POST['ap_email']);

	// CG der Betreff muss mit! 
/*	if(trim($_POST['ap_subject']) === '') {
		$subjectError =  ''; 
		$hasError = true;
	} else {
		$subJect = trim($_POST['ap_subject']);
	}*/

	// CG Produkte sowieso
/*	if(trim($_POST['ap_bestellen']) === '') {
		$subjectError =  ''; 
		$hasError = true;
	} else {
		$prod1 = trim($_POST['ap_bestellen']);
	}*/	


// CG POST Varibale dynamisch entsprechend Anzahl items in Kat Produkte mit Produkt Namen als value
$orderProdukte = array();
foreach ($items as $item => $value) {
	$prodCountController += count($value->catid == 9);
		if(isset($_POST['ap_bestellen-'.$prodCountController])) {
			$orderProdukte[] = $_POST['ap_bestellen-'.$prodCountController] ."<strong> - St&uuml;ckzahl: ". $_POST['prod'.$prodCountController.'Menge']. "</strong>";
		}
	}
	
	// we need at least some content
	if(trim($_POST['ap_message']) === '') {
		$messageError = 'Please enter your Message!';
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$message = stripslashes(trim($_POST['ap_message']));
		} else {
			$message = trim($_POST['ap_message']);
		}
	}
	// require a valid captcha
	if ($captcha_label == "1") {
	if(trim($_POST['ap_captcha']) != $_SESSION['expect']) {
		$captchaError = $params->get('ap_captchaError','Please enter correct Captcha!');
		$hasError = true;
	} else {
		unset ($_SESSION['n1']);
		unset ($_SESSION['n2']);
		unset ($_SESSION['expect']);
		$captcha = trim($_POST['ap_captcha']);
	}}
			
	// let's email:
//CG Email Kopie an Sender	
$mailer = JFactory::getMailer();

$config = JFactory::getConfig();
$senderCopy = array( 
    $config->get( 'mailfrom' ),
    $config->get( 'fromname' ) 
);
$mailer->setSender($senderCopy);
//<---END CG Email Kopie an Sender

	if(!isset($hasError)) {
		$mail =& JFactory::getMailer();		
		$config =& JFactory::getConfig();
		$sender = array($_POST['ap_email'],$_POST['ap_name'] );
		$mail->setSender($sender);
		$mail->setSubject($subject);
		$mail->addRecipient($recipient);
	
		$body = "<div style=\"line-height:22px;font-size:110%;\">Subject: ".$subject."<br/>";
		$body.= "Name: ".$_POST['ap_name']."<br/>";
		$body.= "Email: ".$_POST['ap_email']."<br/><br/>";

		$body.= "Betreff: <strong>".$_POST['ap_subject']."<br/><br/></strong>";

	if(!empty($orderProdukte) && ($_POST['ap_subject'] === $subs['subTwo'] || ($subs['subOne'] == "Produktanfrage"))) {
		$body.= "Produktanfrage: <br />";

		foreach ($orderProdukte as $key => $value) {
			$body.= $value."<br />";
		}
	}
		$body.= "</div>";
		$body.= "<div style=\"padding:15px 0;line-height:22px;width:100%;display:block;font-size:110%;border-top:1px solid #ccc;margin-top: 30px;\"><strong>Message:</strong> ".$_POST['ap_message']."<br/><div/>";

		$mail->setBody($body);
		$mail->IsHTML(true);
			//CG Kopie Email an Sender		
			if(isset($_POST['emailCopy']) || !empty($orderProdukte)) {
				$mailer->addRecipient($clientMailAddress);
					$copySub = $_POST['ap_subject'] ." ".$senderCopy[1]." - Kopie";
				$mailer->setSubject($copySub);
				if(!empty($orderProdukte)) {
					$body.="<div style=\"border: 1px solid #e3e3e3;padding: 30px;margin-top: 30px;\">
							<h4>Kopie Ihrer Email an ".$senderCopy[1]."</h4>
							<h4><i>WICHTIG: </i></h4>
							<p><i>Ihre Email wurde versendet. Bzgl Ihrer Produktanfrage erhalten Sie hierzu zuerst noch eine Bestätigung bzw Angebot per E-Mail von uns. Vielen Dank!</i></p></div>";
				} else {
					$body.= "<div style=\"border: 1px solid #e3e3e3;padding: 30px;margin-top: 30px;\">
							<h4>Kopie Ihrer Email an ".$senderCopy[1]."</h4></div>";
				}

				$mailer->setBody($body);
				$mailer->isHtml(true);
				$mailer->Encoding = 'base64';
				$send =& $mailer->Send();			
			}
			//<--- ENDE CG Kopie Email an Sender	
		$send =& $mail->Send();
		$emailSent = true;
	}
}
	if ($captcha_label == "1") {
		$_SESSION['n1'] = rand(1,10);
		$_SESSION['n2'] = rand(1,10);
		$_SESSION['expect'] = $_SESSION['n1']+$_SESSION['n2'];
	}

require JModuleHelper::getLayoutPath('mod_ap_ajax_089multi_contact', $params->get('layout', 'default'));
require __DIR__ . '/assets/js/jqscript.php'; 
?>