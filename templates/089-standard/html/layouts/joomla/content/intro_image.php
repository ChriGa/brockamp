<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
//CG Override wg lazyLoad!
defined('JPATH_BASE') or die;
$params = $displayData->params;
$ownClass = "lazy ";
if($images->image_intro_caption) $ownClass.="caption ";
?>
<?php $images = json_decode($displayData->images); ?>
<?php if (isset($images->image_intro) && !empty($images->image_intro)) : ?>
	<?php $imgfloat = empty($images->float_intro) ? $params->get('float_intro') : $images->float_intro; ?>
	<div class="pull-<?php echo htmlspecialchars($imgfloat, ENT_COMPAT, 'UTF-8'); ?> item-image">
	<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
		<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($displayData->slug, $displayData->catid, $displayData->language)); ?>"><img
		<?php if ($images->image_intro_caption) : ?>		
			<?php print ' title="' . htmlspecialchars($images->image_intro_caption) . '"'; ?>
		<?php endif; ?>
<?php // CG Overrider ?>
		class="<?php print $ownClass ?>" data-src="<?php echo htmlspecialchars($images->image_intro, ENT_COMPAT, 'UTF-8'); ?>" 
		alt="<?php echo ($images->image_intro_alt) ? htmlspecialchars($images->image_intro_alt, ENT_COMPAT, 'UTF-8') : 'Vera Schulze Brockamp ' . $displayData->title . '-Beitragsbild' ?>" itemprop="thumbnailUrl"/></a>
<?php //<--ENDE  CG Overrider ?>
	<?php else : ?><img
		<?php if ($images->image_intro_caption) : ?>
			<?php $ownClass.= 'caption' . ' title="' . htmlspecialchars($images->image_intro_caption, ENT_COMPAT, 'UTF-8') . '"'; ?>
		<?php endif; ?>
		class="<?php print $ownClass?>" data-src="<?php echo htmlspecialchars($images->image_intro, ENT_COMPAT, 'UTF-8'); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt, ENT_COMPAT, 'UTF-8'); ?>" itemprop="thumbnailUrl"/>
	<?php endif; ?>
	</div>
<?php endif; ?>
