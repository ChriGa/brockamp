<?php
/**
 * @author   	cg@089webdesign.de
 */
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de" <?php //amp ?>>
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-standard/fonts/raleway-regular-webfont.ttf">
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-standard/fonts/playfairdisplay-regular-webfont.ttf">		
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<script type="text/javascript">
	<?php //CG neuladen oder refresh immer von oben beginen: ?>
		jQuery(function(){
			jQuery(this).scrollTop(0);
		});
	</script>
	<link href="/templates/089-standard/css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="/templates/089-standard/css/overrides.css" rel="stylesheet" type="text/css" />	
</head>
<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : ''); ?>">
	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php						
			// including header und menu
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');
			// including breadcrumb
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');																
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');
		if($this->countModules('googleMaps')) : ?>
				<jdoc:include type="modules" name="googleMaps" style="custom" />
			<?php endif; ?>
		<?php
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');				
			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');							
			?>								
		</div>
	<jdoc:include type="modules" name="debug" style="none" />
	<script src="/templates/089-standard/js/jquery.lazy.min.js" type="text/javascript" defer></script>
<?php if($currentMenuID == 112 || $currentMenuID == 157 ) : ?>
	<script src="/templates/089-standard/js/jquery.lazy.iframe.min.js" type="text/javascript" defer></script>
<?php endif; ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			<?php //CG lazy-load ?>
			        jQuery('.lazy').lazy({
			          threshold: 10,
			          visibleOnly: true,		        
			         afterLoad: function(element) {
		            	jQuery('.navStartWrapper').addClass('loaded');
		        	}
		        });
			<?php //<----END CG lazy-load ?> 

			<?php // scroll detection sticky menu und scrollVisible: ?>
				jQuery(window).scroll(function() {
					<?php if(!$clientMobile) : ?>
						(jQuery(this).scrollTop() > 50 ) ? jQuery('#header').addClass('sticky') && jQuery('.innerwidth.content').css('margin-top', '125px') 
								: jQuery('#header').removeClass('sticky') && jQuery('.innerwidth.content').css('margin-top', '0px');
					<?php endif; ?>
			<?php // scrollVisible Variablen: ?>
				    var top_of_element = jQuery(".scrollVisible").offset().top;
				    var bottom_of_element = jQuery(".scrollVisible").offset().top + jQuery(".scrollVisible").outerHeight();
				    var bottom_of_screen = jQuery(window).scrollTop() + jQuery(window).height();
				    var top_of_screen = jQuery(window).scrollTop();
					    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
					    	setTimeout(function() {
					        	jQuery('.scrollVisible').addClass('loaded');
					    	}, 750);
					    }					    
				});
			<?php //<--- END scroll detection sticky menu und scrollVisible: ?>	

			<?php if ($clientMobile) : ?> //mobile
				<?php //CG toggle für resp Menu hintergrund overlay: ?>
					jQuery('.btn-navbar').click("on", function(){ 
						jQuery('.site_wrapper').toggleClass('openMenu');
					});
					<?php //CG submenu klick f. slideToggle: ?>
						var acc = document.getElementsByClassName("mobSubMenuOpen");
								var i;
								for (i = 0; i < acc.length; i++) {
								    jQuery(acc[i]).on("click", function(e){
								    	jQuery(this).toggleClass('show');
						        		jQuery(this).next('.nav-child').slideToggle();
						        			e.preventDefault();
						        		(jQuery(this).hasClass('show')) ? jQuery(this).html(' - ') : jQuery(this).html(' + ');
								    });
								}
						jQuery('ul.nav-child').css('display', 'none');

					<?php else : ?> //desktop						    

			<?php endif;  ?>
	});
	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
