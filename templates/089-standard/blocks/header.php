<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>  			
<header id="header" class="fullwidth headMenu">  
	<div class="brandWrapper">
		<h2>
			<a class="brand" href="<?php echo $this->baseurl; ?>">
				<?php echo $logo; ?>
			</a>
		</h2>
			<?php if ($this->params->get('sitedescription')) : ?>
				<?php if($currentMenuID == 163) :  ?>
					<?php echo '<div class="site-description"><h3>Bio-energetische Produkte</h3></div>'; ?>
				<?php else :?>
						<?php echo '<div class="site-description"><h3>' . htmlspecialchars($this->params->get('sitedescription')) . '</h3></div>'; ?>
				<?php endif;?>
			<?php endif; ?>
	</div>
	<nav class="navbar-wrapper">
	    <div class="navbar">
	      <div class="navbar-inner">
	        <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>                                
			  <?php if ($this->countModules('menu')) : ?>
					<div class="nav-collapse collapse "  role="navigation">
						<jdoc:include type="modules" name="menu" style="custom" />
					</div>
				<?php endif; ?>
	      </div>
	    </div>
	</nav>			
	<?php /*
		<div class="vcard clr">	
			<h3 class="vcardHeader">vcard Header</h3>			  
			  <p class="tel "><a class="" href="tel:+555111">Tel: null achtneun perfect</a></p>
			  <p class="adr"><span class="street-address">Strasse 0</span><br />
			    <span class="postal-code">80000 </span><span class="region">Dorf bei der Stadt</span>
			  </p>
		</div>
	*/ ?>	  
</header>      
