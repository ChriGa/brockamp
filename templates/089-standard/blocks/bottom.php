<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<div class="bottom fullwidth">
	<div class="bottom-wrap">
		<?php if ($this->countModules('bottom1')) : ?>
		<div class="module_bottom position_bottom1">
			<jdoc:include type="modules" name="bottom1" style="custom" />
		</div>
		<?php endif ?>
		<?php if ($this->countModules('bottom2')) : ?>
		<div class="module_bottom position_bottom2 scrollVisible">
			<jdoc:include type="modules" name="bottom2" style="custom" />
		</div>
		<?php endif ?>		
	</div>
</div>  	

