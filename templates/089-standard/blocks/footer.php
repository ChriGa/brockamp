<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="bottomLinks innerwidth bottomFlex">
		<div>
			<a class="bottomLink" href="/impressum.html" title="Impressum Vera Schulze-Brockamp">Impressum</a>
		</div>
		<div>
			<a class="bottomLink" href="/datenschutz.html" title="Datenschutzerklärung Vera Schulze-Brockamp">Datenschutz</a>
		</div>		
	</div>
</footer>
<div id="copyright" class="fullwidth">
	<div class="copyWrapper innerwidth">
		<p>&copy; <?php print date("Y");?> Vera Schulze-Brockamp</p>
	</div>
</div>	
		